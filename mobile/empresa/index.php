<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>

  <!-- ---- LAYER SLIDER ---- -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $("#carousel-gallery").touchCarousel({
        itemsPerPage: 1,
        scrollbar: true,
        scrollbarAutoHide: true,
        scrollbarTheme: "dark",
        pagingNav: false,
        snapToItems: true,
        scrollToLast: false,
        useWebkit3d: true,
        loopItems: true
      });
    });
  </script>
  <!-- XXXX LAYER SLIDER XXXX -->

</head>

<body>


  <?php require_once('../includes/topo.php'); ?>

  <!-- bg-empresa -->
  <div class="container bg-empresa">
    <div class="row"></div>
  </div>
  <!-- bg-empresa -->


  <!-- empresa descricao  -->
  <div class="container top25">
    <div class="row">
      <div class="col-xs-12">
        <h2>A EMPRESA</h2>
      </div>

      <div class="col-xs-12 top20">
        <div class="jumbotron empresa-descricao">

          <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
          <p class="top20 bottom40"><?php Util::imprime($dados[descricao]) ?></p>

        </div>
      </div>

      


    </div>
  </div>
  <!-- empresa descricao  -->



  

  <!-- portifolio empresa -->
  <div class="container  fundo-azul pb25 top30">
    <div class="row">
      <div class="col-xs-12 portifolio-home">
        <h2 class="bottom20 top25">NOSSO PORTFÓLIO</h2>
      </div>

       <!-- portifolio01 -->
       <?php
       $result = $obj_site->select("tb_portifolios", "ORDER BY rand() LIMIT 4");
       if (mysql_num_rows($result) > 0) {
           while($row = mysql_fetch_array($result)){
            ?>
              <div class="col-xs-6">
                  <a href="<?php echo Util::caminho_projeto() ?>/mobile/portfolio/<?php Util::imprime($row[url_amigavel]) ?>"  data-toggle="tooltip" data-placement="top" title="<?php Util::imprime($row[titulo]) ?>">
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">
                  </a>
              </div>
          <?php
          }
      }
      ?>



    </div>
  </div>
  <!-- portifolio empresa -->


  


  <?php require_once('../includes/rodape.php'); ?>


</body>

</html>