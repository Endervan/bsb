<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>

  <!-- FlexSlider -->
  <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
  <!-- Syntax Highlighter -->
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
  <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


  <!-- Syntax Highlighter -->
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


  <!-- Demo CSS -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />


  <script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 130,
        itemMargin: 5,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        customDirectionNav: $(".custom-navigation a"),
        controlNav: false,
        itemWidth: 141,
        itemMargin: 7,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>


</head>

<body>


  <?php require_once('./includes/topo.php'); ?>



  <!-- slider -->
  <div class="container">
    <div class="row slider-index">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          <li data-target="#carousel-example-generic" data-slide-to="3"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/banner1.jpg" alt="">
            <div class="carousel-caption">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">
                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/saiba-mais.jpg" alt="">
              </a>
            </div>
          </div>


          <div class="item">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/banner2.jpg" alt="">
            <div class="carousel-caption">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">
                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/saiba-mais.jpg" alt="">
              </a>
            </div>
          </div>


          <div class="item">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/banner3.jpg" alt="">
            <div class="carousel-caption">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">
                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/saiba-mais.jpg" alt="">
              </a>
            </div>
          </div>

          <div class="item">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/banner4.jpg" alt="">
            <div class="carousel-caption">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">
                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/saiba-mais.jpg" alt="">
              </a>
            </div>
          </div>


        </div>


        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a> 


      </div>
    </div>
  </div>
  <!-- slider -->





  <!-- flex slider -->
  <div class="container">
    <div class="row slider-prod">
      <div class="col-xs-12">
        <h4>NOSSAS CATEGORIAS:</h4>
        <!-- Place somewhere in the <body> of your page -->
        <div id="slider" class="flexslider text-center">
          <ul class="slides lista-categorias">
            
            <?php
            $result = $obj_site->select("tb_categorias_produtos", "ORDER BY ordem LIMIT 9");
            if (mysql_num_rows($result) > 0) {
                while($row = mysql_fetch_array($result)){
                ?>
                <li>
                    <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/?cat=<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
                        <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" alt="" class="img-rounded">
                        <h5><?php Util::imprime($row[titulo]) ?></h5>
                    </a>
                </li>
                <?php
                }
            }
            ?>

            
     <!-- items mirrored twice, total of 12 -->
   </ul>

   <!-- seta personalizadas -->
   <div class="custom-navigation text-right">
    <a href="#" class="flex-prev"><i class="fa fa-chevron-left"></i>
    </a>
    
    
    <a href="#" class="flex-next"><i class="fa fa-chevron-right"></i>
    </a>

  </div>
  <!-- seta personalizadas -->
</div>




</div>
</div>
</div>
<!-- flex slider -->


<!-- produtos home  -->
<div class="container top25">
  <div class="row">
    <div class="col-xs-12">
      <h2>DESTAQUES</h2>
    </div>
  


    <?php
    $result = $obj_site->select("tb_produtos", "ORDER BY rand() LIMIT 4");
    if (mysql_num_rows($result) > 0) {
        while($row = mysql_fetch_array($result)){
        ?>
        <!-- produto 01 -->
        <div class="col-xs-6 top20">
          <div class="thumbnail produtos-home ">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
              <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">
            </a>
            <div class="caption">
              <h1><?php Util::imprime($row[titulo]) ?></h1>
              <h3 class="top10"><i class="fa fa-star"></i><?php Util::imprime( Util::troca_value_nome($row[id_categoriaproduto], "tb_categorias_produtos", "idcategoriaproduto", "titulo") ); ?></h3>
              <h3 class="top5"><i class="fa fa-star"></i><?php Util::imprime($row[marca]) ?></h3>
            </div>
          </div>
        </div>
        <?php 
        }
      }
      ?>


  
    


    

    <!-- botao produto -->
    <div class="col-xs-12 text-center top25">
      <a href="#" class="btn btn-primary" role="button">VER PRODUTOS</a>
    </div>
    <!-- botao produto -->


  </div>
</div>
<!-- produtos home  -->


<!-- conheca mais bsb -->
<div class="container top45 conheca-mais">
  <div class="row">
    <div class="col-xs-12 conheca">
      <h4 class="top20">CONHEÇA UM POUCO MAIS A BSB PISCINAS</h4>
      <div class="conheca-index">
        <h5 class="top20">
          <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
          <?php Util::imprime($dados[descricao]) ?>
        </h5>
      </div>
    </div>
  </div>
</div>
<!-- conheca mais bs  -->


<!-- nossas dicas home -->
<div class="container top30 bottom30">
  <div class="row">
    <div class="col-xs-12 dicas-home">
      <h4 class="top20">NOSSAS DICAS</h4>
    </div>

    <?php
    $result = $obj_site->select("tb_dicas", "order by rand() limit 2");
    if(mysql_num_rows($result) > 0)
    {
        while($row = mysql_fetch_array($result)){
        ?>
        <!-- dica 01 -->
        <div class=" col-xs-12 dicas-home1 top30 pbottom20">
          <div class="borda-azul">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas/<?php Util::imprime($row[url_amigavel]) ?>">
              <div class="col-xs-4">
               <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="<?php Util::imprime($row[titulo]) ?>" class="img-circle input100">
             </div>

             <div class=" col-xs-8">
              <h4><?php Util::imprime($row[titulo]) ?></h4>
              <h5><?php Util::imprime($row[descricao], 400) ?></h5>
            </div>
          </a>
        </div>  
      </div>
      <?php 
      }
    }
    ?>



</div>
</div>
<!-- nossas dicas home -->


<?php require_once('./includes/rodape.php'); ?>


</body>

</html>