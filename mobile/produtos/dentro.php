<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// INTERNA DE PRODUTOS
$url = $_GET[get1];


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}


$result = $obj_site->select("tb_produtos",$complemento);
if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/mobile/produtos/");
}

$dados_dentro = mysql_fetch_array($result);

// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>


<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>

  <!-- FlexSlider -->
  <script defer src="http://masmidia.com.br/clientes/san-remo/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
  <!-- Syntax Highlighter -->
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
  <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


  <!-- Syntax Highlighter -->
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


  <!-- Demo CSS -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />


  <script>
    $(window).load(function() {
          // The slider being synced must be initialized first
          $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 51,
            itemMargin:7,
            asNavFor: '#slider'
          });

          $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel"
          });
        });
  </script>

  <!-- star rating -->
  <script>
    jQuery(document).ready(function () {

      $('.avaliacao').rating({
        min: 0,
        max: 5,
        step: 1,
        size: 'xs',
        showClear: false,
        disabled: true,
        clearCaption: 'Seja o primeiro a avaliar.',
        starCaptions: {
          0.5: 'Half Star',
          1: 'Ruim',
          1.5: 'One & Half Star',
          2: 'Regular',
          2.5: 'Two & Half Stars',
          3: 'Bom',
          3.5: 'Three & Half Stars',
          4: 'Ótimo',
          4.5: 'Four & Half Stars',
          5: 'Excelente'
        }
      });


    });
  </script>
  <!-- star rating -->


  <!-- adicionando ou diminuindo quantidade de produto -->
  <script>
    $(document).ready(function() {
      $("#mais").click(function(){

        if($("#mudeValor").val() < 10){
          $('#mudeValor').val(parseInt($('#mudeValor').val())+1); return false;
        } else {
          $('#limite').html('limite maximo 10');
          return false;
        }

      });
      $("#menos").click(function(){
        $('#limite').html('');
        if($("#mudeValor").val()!=0){$('#mudeValor').val(parseInt($('#mudeValor').val())-1);} return false;
      });
    });
  </script>


</head>

<body>


  <?php require_once('../includes/topo.php'); ?>

  <!-- bg-produtos -->
  <div class="container bg-produtos">
    <div class="row"></div>
  </div>
  <!-- bg-produtos -->


  <!-- produtos barra de pesquisas  -->
  <div class="container top25">
    <div class="row text-center">


      <div class="col-xs-12">

        <form action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" method="post">
          <div class=" input-group barra-pesquisa-topo1">
            <input type="text" class="form-control fundo-form1  input-lg" name="busca_topo" placeholder="PESQUISAR PRODUTOS">
            <span class="input-group-btn">
              <button class="btn btn-default input-lg" type="submit"><i class="fa fa-search"></i>
              </button>
            </span>
          </div>
        </form>

      </div>

      

    </div>
  </div>
  <!-- produtos barra de pesquisas  -->


  <!-- flex slider -->
  <div class="container top20">
    <div class="row slider-prod-dentro">

      <div class="col-xs-12 descricao-produtos">

        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" class="btn btn-primary btn-voltar" role="button"><i class="fa fa-angle-double-left"></i>VOLTAR</a>
        <!-- botao voltar --><!-- botao voltar -->
      </div>
      <!-- descricao do produto -->
      <div class="col-xs-12 slider-produtos top20">

        <h2 class="bottom20"><?php Util::imprime($dados_dentro[titulo]) ?></h2>

        <!-- Place somewhere in the <body> of your page -->
        <div id="slider" class="flexslider ">
          <ul class="slides slider-prod">
            
            <?php if (!empty($dados_dentro[imagem])) { ?>
              <li>
                <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 446, 287); ?>
              </li>
            <?php } ?>
            

            <?php 
              $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = $dados_dentro[0] ");
              if (mysql_num_rows($result) > 0) {
                while ($row = mysql_fetch_array($result)) {
                ?>
                  <li>
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 446, 287); ?>
                  </li>
                <?php
                }
              }
              ?>


            <!-- items mirrored twice, total of 12 -->
          </ul>
        </div>


        <div class="slider-prod-dentro1">
          <div id="carousel" class="flexslider">
            <ul class="slides slider-prod-tumb">

              <?php if (!empty($dados_dentro[imagem])) { ?>
                <li>
                  <?php $obj_site->redimensiona_imagem("../uploads/tumb_$dados_dentro[imagem]", 51, 51); ?>
                </li>
              <?php } ?>


              <?php 
              $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = $dados_dentro[0] ");
              if (mysql_num_rows($result) > 0) {
                while ($row = mysql_fetch_array($result)) {
                ?>
                  <li>
                    <?php $obj_site->redimensiona_imagem("../uploads/tumb_$row[imagem]", 51, 51); ?>
                  </li>
                <?php
                }
              }
              ?>

              <!-- items mirrored twice, total of 12 -->
            </ul>
          </div>
        </div>


      </div>
    </div>
  </div>


  <div class="container">
    <div class="row">


      <div class="col-xs-6 top20 descricao-produtos">

        <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
        <p><button type="button" class="btn btn-primary right10">MARCA:</button><?php Util::imprime($dados_dentro[marca]) ?></p>
        <p><button type="button" class="btn btn-primary right10">CATEGORIA:</button><?php Util::imprime( Util::troca_value_nome($dados_dentro[id_categoriaproduto], "tb_categorias_produtos", "idcategoriaproduto", "titulo") ); ?></p>
        <p class="rating"><button type="button" class="btn  btn-primary pull-left">AVALIAÇÕES</button><span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span></p>
      </div>

      <!-- atendimento -->
      <div class="col-xs-6 topo-telefone1 text-right">
       <h6>ATENDIMENTO EM</h6>
       <div class="telefone-topo1 top5">
        
        <?php if (!empty($config[telefone1])) { ?>
          <h4>
            <?php Util::imprime($config[telefone1]) ?>
            <a href="tel:+55<?php Util::imprime($config[telefone1]) ?>" class="btn btn-azul">
              CHAMAR
            </a>
          </h4>
        <?php } ?>
        

        <?php if (!empty($config[telefone2])) { ?>
          <h4 class="top5">
            <?php Util::imprime($config[telefone2]) ?>
            <a href="tel:+55<?php Util::imprime($config[telefone2]) ?>" class="btn btn-azul">
              CHAMAR
            </a>
          </h4>
        <?php } ?>


      </div>

      <a href="javascript:void(0);" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
        <img class="top20" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/btn-carinho.png" alt="">
      </a>
    </div>
    <!-- atendimento -->

  </div>
</div>


<!-- descricao produto dentro -->
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h6>DESCRIÇÃO</h6>

      <p class="top15 bottom35"><?php Util::imprime($dados_dentro[descricao]) ?></p>

      <h6 class="bottom20 top30">COMENTÁRIOS</h6>

    </div>
  </div>
</div>
<!-- descricao produto dentro -->


<!-- comentarios produtos dentro -->
<div class="container">
  <div class="row">

  
    <?php
    $result = $obj_site->select("tb_avaliacoes_produtos", "AND id_produto = '$dados_dentro[0]' ORDER BY data DESC");
    
    if(mysql_num_rows($result) > 0)
    {
        while($row = mysql_fetch_array($result))
        {
        ?>
          <div class="col-xs-12 descricao-comentario1">
            <div class="jumbotron">
              <h4><?php Util::imprime($row[nome]) ?></h4>
              <!-- avaliacao -->
              <div class=" text-left">
                <input id="avaliaca-1" class="avaliacao" type="number" value="<?php Util::imprime($row[nota]) ?>" />
              </div>
              <!-- avaliacao -->
              <p><?php Util::imprime($row[comentario], 3000) ?></p>
            </div>
          </div>
        <?php  
        }
    }else{
      echo '<p class="bg-warning">Nenhum comentário. Seja o primeiro a comentar.</p>';
    }
    ?>

  </div>
</div>
<!-- comentarios produtos dentro -->





<!-- form comentario -->
<div class="container top30">
  <?php 
  if (isset($_POST[comentario])) {
    $obj_site->insert("tb_avaliacoes_produtos");
    Util::alert_bootstrap("Muito obrigado pelo seu comentário.");
  }
  ?>
  <form class="form-inline FormContato fundo-form" role="form" method="post">

        <div class="row">
            <div class="col-xs-12 form-group">
                <label class="glyphicon glyphicon-user"> Nome</label>
                <input type="text" name="nome" class="form-control input100" placeholder="">
            </div>
            <div class="col-xs-12 form-group">
                <label class="glyphicon glyphicon-envelope"> E-mail</label>
                <input type="text" name="email" class="form-control input100" placeholder="">
            </div>
            <div class="col-xs-12 form-group">
                <label class="fa fa-star"> Nota</label>
                <select name="nota" id="nota" class="form-control input100">
                    <option value="">Selecione</option>
                    <option value="5">Excelente</option>
                    <option value="4">Ótimo</option>
                    <option value="3">Bom</option>
                    <option value="2">Regular</option>
                    <option value="1">Ruim</option>
                </select>
            </div>

            <div class="col-xs-12 top20 form-group">
                <label class="glyphicon glyphicon-pencil"> Comentário</label>
                <textarea name="comentario" id="" cols="30" rows="10" class="form-control input100"></textarea>
            </div>

            <div class="clearfix"></div>

            <input type="hidden" name="id_produto" value="<?php echo $dados_dentro[0] ?>">
            <input type="hidden" name="ativo" value="NAO">
            <input type="hidden" name="data" value="<?php echo date("d/m/Y") ?>">

            <div class="text-right top30">
                <button type="submit" class="btn btn-default">
                    ENVIAR
                </button>
            </div>
          </div>

      </form>
</div>
<!-- form comentario -->




<!-- veja tambem -->
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <h6 class="top15">VEJA TAMBÉM</h6>
    </div>

    <?php
    $result = $obj_site->select("tb_produtos", "AND idproduto <> $dados_dentro[0] ORDER BY rand() LIMIT 2");
    if (mysql_num_rows($result) > 0) {
        while($row = mysql_fetch_array($result)){
        ?>
        <!-- produto 01 -->
        <div class="col-xs-6 top20">
          <div class="thumbnail produtos-home ">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
              <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">
            </a>
            <div class="caption">
              <h1><?php Util::imprime($row[titulo]) ?></h1>
              <h3 class="top10"><i class="fa fa-star"></i><?php Util::imprime( Util::troca_value_nome($row[id_categoriaproduto], "tb_categorias_produtos", "idcategoriaproduto", "titulo") ); ?></h3>
              <h3 class="top5"><i class="fa fa-star"></i><?php Util::imprime($row[marca]) ?></h3>
            </div>
          </div>
        </div>
        <?php 
        }
      }
      ?>



  </div>
</div>
<!-- veja tambem -->






<?php require_once('../includes/rodape.php'); ?>


</body>

</html>





<script>
    $(document).ready(function() {
        $('.FormContato').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 nome: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                        
                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                nota: {
                    validators: {
                        notEmpty: {
                        
                        }
                    }
                },
                assunto: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                comentario: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                }
            }
        });
    });
</script>






