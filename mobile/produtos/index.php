<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>


  <!-- FlexSlider -->
  <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
  <!-- Syntax Highlighter -->
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
  <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


  <!-- Syntax Highlighter -->
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


  <!-- Demo CSS -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />


  <script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: true,
        animationLoop: false,
        slideshow: false,
        itemWidth: 130,
        itemMargin: 5,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        customDirectionNav: $(".custom-navigation a"),
        controlNav: true,
        itemWidth: 141,
        itemMargin: 7,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>



</head>

<body>


  <?php require_once('../includes/topo.php'); ?>

  <!-- bg-produtos -->
  <div class="container bg-produtos">
    <div class="row"></div>
  </div>
  <!-- bg-produtos -->


  <!-- produtos barra de pesquisas  -->
  <div class="container top25">
    <div class="row text-center">


      <div class="col-xs-12">
        <form action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" method="post">
          <div class=" input-group barra-pesquisa-topo1">
            <input type="text" class="form-control fundo-form1  input-lg" name="busca_topo" placeholder="PESQUISAR PRODUTOS">
            <span class="input-group-btn">
              <button class="btn btn-default input-lg" type="submit"><i class="fa fa-search"></i>
              </button>
            </span>
          </div>
        </form>
      </div>

      

      
    </div>
  </div>
  <!-- produtos barra de pesquisas  -->


  <!-- flex slider -->
  <div class="container top20">
    <div class="row slider-prod1">
      <div class="col-xs-12">

        <!-- Place somewhere in the <body> of your page -->
        <div id="slider" class="flexslider text-center">
          <ul class="slides lista-categorias">

            <?php
            $result = $obj_site->select("tb_categorias_produtos", "ORDER BY rand() LIMIT 9");
            if (mysql_num_rows($result) > 0) {
                while($row = mysql_fetch_array($result)){
                ?>
                <li>
                    <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/?cat=<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
                        <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" alt="" class="img-rounded">
                        <h5><?php Util::imprime($row[titulo]) ?></h5>
                    </a>
                </li>
                <?php
                }
            }
            ?>

           

<!-- items mirrored twice, total of 12 -->
</ul>


</div>
<!-- seta personalizadas -->
<div class="custom-navigation text-center">
  <a href="#" class="flex-prev"><i class="fa fa-angle-left"></i>
  </a>


  <a href="#" class="flex-next"><i class="fa fa-angle-right"></i>
  </a>

</div>
<!-- seta personalizadas -->

</div>
</div>
</div>


<!-- produtos home  -->
<div class="container">
  <div class="row">
    


    <?php
     //  titulo
    if (isset($_POST[busca_topo])) {
        $complemento .= "AND titulo LIKE '%$_POST[busca_topo]%'";
    }

    //  categoria
    if (isset($_GET[cat])) {
        $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $_GET[cat]);
        $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
    }


    $result = $obj_site->select("tb_produtos", $complemento);
    if (mysql_num_rows($result) == 0) {
      echo '<p class="top40 bottom20 bg-warning">Nenhum produto encontrado.</p>';
    }else{
        while($row = mysql_fetch_array($result)){
        ?>
        <!-- produto 01 -->
        <div class="col-xs-6 top20">
          <div class="thumbnail produtos-home ">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
              <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">
            </a>
            <div class="caption">
              <h1><?php Util::imprime($row[titulo]) ?></h1>
              <h3 class="top10"><i class="fa fa-star"></i><?php Util::imprime( Util::troca_value_nome($row[id_categoriaproduto], "tb_categorias_produtos", "idcategoriaproduto", "titulo") ); ?></h3>
              <h3 class="top5"><i class="fa fa-star"></i><?php Util::imprime($row[marca]) ?></h3>
            </div>
          </div>
        </div>
        <?php 
        }
      }
      ?>



    <!-- botao produto -->
    <div class="col-xs-12 text-center top25 bottom40">
     
    </div>
    <!-- botao produto -->


  </div>
</div>
<!-- produtos home  -->






<?php require_once('../includes/rodape.php'); ?>


</body>

</html>