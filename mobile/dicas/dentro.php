<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// INTERNA DE PRODUTOS
$url = $_GET[get1];


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}


$result = $obj_site->select("tb_dicas", $complemento);
if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/mobile/dicas/");
}

$dados_dentro = mysql_fetch_array($result);

// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>



<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>


</head>

<body>


  <?php require_once('../includes/topo.php'); ?>

  <!-- bg-dicas -->
  <div class="container bg-dicas">
    <div class="row"></div>
  </div>
  <!-- bg-dicas -->


  <!-- dicas gerais  -->
  <div class="container top25">
    <div class="row">
      <div class="col-xs-6">
        <h2>BSB DICAS</h2>
      </div>

      <div class="col-xs-6">
        <form action="<?php echo Util::caminho_projeto() ?>/dicas/" method="post">
          <div class="input-group barra-pesquisa-topo">
            <input type="text" class="form-control  form1 input-lg" name="busca_dica" placeholder="ENTRE COM DICA QUE PRECISA">
            <span class="input-group-btn">
              <button class="btn btn-default input-lg" type="submit"><i class="fa fa-search"></i>
              </button>
            </span>
          </div>
        </form>
      </div>

    </div>
  </div>
  <!-- dicas gerais  -->


  <!-- descricao dicas dentro -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12 dica-dentro">
        <h4 class="top10"><?php Util::imprime($dados_dentro[titulo]) ?></h4>
       
        <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dados_dentro[imagem]) ?>" alt="" class="input100 top20" >
        
        <p class="top20 bottom30"><?php Util::imprime($dados_dentro[descricao]) ?></p>
      </div>
    </div>
  </div>
  <!-- descricao dicas dentro -->


  <!-- comentarios -->
  <div class="container bottom25 top30">
    <div class="row comentarios">
      
      <?php
      $result = $obj_site->select("tb_comentarios_dicas", "AND id_dica = '$dados_dentro[0]' ORDER BY data DESC");
      ?>


      <div class="col-xs-6">
        <h6 class="top5">COMENTÁRIOS(<?php echo mysql_num_rows($result) ?>)</h6>
      </div>
      <!-- BOTAO DEIXE SEU COMENTÁRIO -->
      <div class="col-xs-6 text-right">
        <a href="">
          <button type="button" class="btn btn-primary btn-azul btn-lg">DEIXE SEU COMENTÁRIO</button>
        </a>
      </div>
      <!-- BOTAO DEIXE SEU COMENTÁRIO -->

      
      <!-- descricao comentario -->
      
      <?php
      if(mysql_num_rows($result) > 0)
      {
          while($row = mysql_fetch_array($result))
          {
          ?>
            <div class="col-xs-12 top35">
              <div class="jumbotron descricao-comentario">
                <h1><?php Util::imprime($row[nome]) ?></h1>
                <p class="top20 bottom35"><?php Util::imprime($row[comentario]) ?></p>

              </div>
            </div>
          <?php  
          }
      }else{
        echo '<div class="clearfix"></div> <p class="bg-warning top20">Nenhum comentário. Seja o primeiro a comentar.</p>';
      }
      ?>
      <!-- descricao comentario -->

    </div>
  </div>
  <!-- comentarios -->




  <!-- form comentario -->
<div class="container top30 bottom40">
  <?php 
  if (isset($_POST[comentario])) {
    $obj_site->insert("tb_comentarios_dicas");
    Util::alert_bootstrap("Muito obrigado pelo seu comentário.");
  }
  ?>
  <form class="form-inline FormContato fundo-form" role="form" method="post">

        <div class="row">
            <div class="col-xs-12 form-group">
                <label class="glyphicon glyphicon-user"> Nome</label>
                <input type="text" name="nome" class="form-control input100" placeholder="">
            </div>
            <div class="col-xs-12 form-group">
                <label class="glyphicon glyphicon-envelope"> E-mail</label>
                <input type="text" name="email" class="form-control input100" placeholder="">
            </div>
            <div class="col-xs-12 form-group">
                <label class="fa fa-star"> Nota</label>
                <select name="nota" id="nota" class="form-control input100">
                    <option value="">Selecione</option>
                    <option value="5">Excelente</option>
                    <option value="4">Ótimo</option>
                    <option value="3">Bom</option>
                    <option value="2">Regular</option>
                    <option value="1">Ruim</option>
                </select>
            </div>

            <div class="col-xs-12 top20 form-group">
                <label class="glyphicon glyphicon-pencil"> Comentário</label>
                <textarea name="comentario" id="" cols="30" rows="10" class="form-control input100"></textarea>
            </div>

            <div class="clearfix"></div>

            <input type="hidden" name="id_dica" value="<?php echo $dados_dentro[0] ?>">
            <input type="hidden" name="ativo" value="NAO">
            <input type="hidden" name="data" value="<?php echo date("d/m/Y") ?>">

            <div class="text-right top30">
                <button type="submit" class="btn btn-default">
                    ENVIAR
                </button>
            </div>
          </div>

      </form>
</div>
<!-- form comentario -->


  <?php require_once('../includes/rodape.php'); ?>


</body>

</html>







<script>
    $(document).ready(function() {
        $('.FormContato').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 nome: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                        
                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                nota: {
                    validators: {
                        notEmpty: {
                        
                        }
                    }
                },
                assunto: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                comentario: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                }
            }
        });
    });
</script>






