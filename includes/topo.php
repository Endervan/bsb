<!--contatos-topo -->
<div class="container-fluir fundo-cinza">
  <div class="row">

	<div class="container">
	  <div class="row">

		<div class="topo-telefone">
		  <div class="col-xs-7">
			<h1 class="top20">ATENDIMENTO: </h1>
		  </div>

		  <div class="col-xs-5 top30">
			<h2>
                <span><?php Util::imprime($config[telefone1]) ?></span>
                <?php if(!empty($config[telefone2])){ echo("<span> | ".$config[telefone2]."</span>"); } ?>
                <?php if(!empty($config[telefone3])){ echo("<span> | ".$config[telefone3]."</span>"); } ?>
            </h2>
		  </div>

		</div>
	  </div>


	  <!-- menu-topo -->
	  <div class="row">
		<div class="col-xs-3 posicao-logo">
		  <a href="<?php echo Util::caminho_projeto() ?>">
			<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
		  </a>
		</div>

		<div class="col-xs-9">
		  <div class="topo-pesquisa">
			<div class="col-xs-4">
			  <h1 class="top20">PESQUISAR PRODUTOS:</h1>
			</div>
		  </div>


		  <!-- barra pesquisa -->
          <form class="" action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
              <div class="col-xs-4 top30">
    			<div class="input-group ">
    			  <input type="text" class="form-control barra-pesquisa-topo" name="busca_topo" placeholder="Busca por nome">
    			  <span class="input-group-btn">
    				<button class="btn btn-default" type="submit"><i class="fa fa-search"></i>
    				</button>
    			  </span>
    			</div>
    		  </div>
          </form>



		  <!-- menu topo selec -->
		  <div class="col-xs-4 top28">
			<div class="dropdown ">
			  <button class="btn btn-default barra-pesquisa-topo1 dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				CATEGORIA
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu input100" aria-labelledby="dropdownMenu1">
                  <?php
                   $result = $obj_site->select("tb_categorias_produtos");
                   if (mysql_num_rows($result) > 0) {
                       while($row = mysql_fetch_array($result)){
                       ?>
                       <li><a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]) ?>"><?php Util::imprime($row[titulo]) ?></a></li>
                       <?php
                       }
                   }
                   ?>
			  </ul>
			</div>
		  </div>
		</div>

	  </div>



	</div>
  </div>
</div>
<!-- contatos-topo -->



<div class="container top20 subir-menu ">
  <div class="row">

	<!-- menu -->
	<div class="col-xs-12">
	 <nav class="navbar navbar-default " role="navigation">
	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class="collapse navbar-collapse navbar-ex1-collapse">
	   <ul class="nav navbar-nav">
		<li class="active">
		 <a href="<?php echo Util::caminho_projeto() ?>">
		  <i class="fa fa-home"></i> <br>
		  HOME
		</a>
		</li>
		<li>
		 <a href="<?php echo Util::caminho_projeto() ?>/empresa">
		  <i class="fa fa-users"></i> <br>
		  A EMPRESA
		</a>
		</li>
		<li>
	   <a href="<?php echo Util::caminho_projeto() ?>/produtos">
		<i class="fa fa-life-ring"></i> <br>
		PRODUTOS
		  </a>
		</li>
		<li>
		 <a href="<?php echo Util::caminho_projeto() ?>/servicos">
		  <i class="fa fa-cog"></i> <br>
		  SERVIÇOS
		</a>
		  </li>
		  <li>
		   <a href="<?php echo Util::caminho_projeto() ?>/dicas">
			<i class="fa fa-file-text-o"></i> <br>
			DICAS
		  </a>
		  </li>
		  <li>
		   <a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">
			<i class="fa fa-envelope"></i> <br>
			FALE CONOSCO
		  </a>
		  </li>
		  
	  </ul>
		</div>

		<!-- /.navbar-collapse -->
		  </nav>
		</div>



<!-- menu -->

  </div>

</div>


<!-- carrinho topo -->
<div class="container">
  <div class="row">
	<div class="col-xs-3 col-xs-offset-9 personalizar-carrinho right20 text-right">
	  <div class="dropdown">
		<a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="btn-meu-orcamento">
		  <h3>
			<i class="fa fa-shopping-cart"></i>
			MEU <br> ORÇAMENTO<span class="badge"><?php echo count($_SESSION[solicitacoes_produtos]) + count($_SESSION[solicitacoes_servicos]) ?></span><i class="fa fa-angle-double-down"></i>
		  </h3>
		</a>
		<div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">



          <?php
          if(count($_SESSION[solicitacoes_produtos]) > 0)
          {
              echo '<h6 class="bottom20 top20">MEUS PRODUTOS('. count($_SESSION[solicitacoes_produtos]) .')</h6>';

              for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
              {
                  $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                  ?>
                  <div class="lista-itens-carrinho">
                      <div class="col-xs-2">
                          <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">
                      </div>
                      <div class="col-xs-8">
                          <h1><?php Util::imprime($row[titulo]) ?></h1>
                      </div>
                      <div class="col-xs-1">
                          <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                      </div>
                  </div>
                  <?php
              }
          }
          ?>




          <?php
          if(count($_SESSION[solicitacoes_servicos]) > 0)
          {
              echo '<h6 class="bottom20">MEUS SERVIÇOS('. count($_SESSION[solicitacoes_servicos]) .')</h6>';

              for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
              {
                  $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                  ?>
                  <div class="lista-itens-carrinho">
                      <div class="col-xs-2">
                          <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">
                      </div>
                      <div class="col-xs-8">
                          <h1><?php Util::imprime($row[titulo]) ?></h1>
                      </div>
                      <div class="col-xs-1">
                          <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                      </div>
                  </div>
                  <?php
              }
          }
          ?>

		  <div class="text-right bottom20">
			<a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="Finalizar" class="btn btn-azul">
			  FINALIZAR
			</a>
		  </div>


		</div>

	  </div>
	</div>
  </div>
</div>
<!-- carrinho topo -->



<!-- menu-topo
