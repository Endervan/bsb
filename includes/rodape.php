<div class="container-fluid rodape-cinza">
	<div class="row">

		<!-- menu -->
		 <div class="container bg-menu-rodape top40">
			 <div class="row">
				 <div class="col-xs-12">
					 <ul class="menu-rodape">
						 <li>
							 <a href="<?php echo Util::caminho_projeto() ?>/">HOME</a>
							 <a href="<?php echo Util::caminho_projeto() ?>/empresa">EMPRESA</a>
							 <a href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a>
						 	 <a href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS</a>
							 <a href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a>
							 <a href="<?php echo Util::caminho_projeto() ?>/portfolios">PORTFÓLIO</a>
							 <a href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a>
							 <a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO</a>
						 </li>
					 </ul>
				 </div>
			 </div>
		 </div>
		 <!-- menu -->

		 <!-- logo, endereco, telefone -->
		<div class="container top40">
			<div class="row">
				<div class="col-xs-3">
					<a href="<?php echo Util::caminho_projeto() ?>">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
					</a>
				</div>
				<div class="col-xs-7">
					<p class="bottom10"><i class="glyphicon glyphicon-home"></i><?php Util::imprime($config[endereco]) ?></p>
					<p>
						<i class="glyphicon glyphicon-phone"></i>
						<?php Util::imprime($config[telefone1]) ?>
						<?php if(!empty($config[telefone2])){ echo(" | ".$config[telefone2]); } ?>
						<?php if(!empty($config[telefone3])){ echo(" | ".$config[telefone3]); } ?>
						<?php if(!empty($config[telefone4])){ echo(" | ".$config[telefone4]); } ?>
					</p>
				</div>
				<div class="col-xs-2">
					
					<?php if ($config[google_plus] != "") { ?> 
						 <a class="pull-left top25 right10" href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" > 
							<i style="color: #fff" class="fa fa-google-plus"></i>
						 </a> 
					 <?php } ?>


					<a href="http://www.homewebbrasil.com.br" target="_blank">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-masmidia.png"  alt="">
					</a>
				</div>
			</div>
		</div>
		 <!-- logo, endereco, telefone -->

	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="rodape-preto">
			<h5 class="text-center">© Copyright BSB PISCINAS</h5>
		</div>
	</div>
</div>



<script type="text/javascript">
    $(document).ready(function() {

        $(".produto-hover").hover(
          function () {
              $(this).stop().animate({opacity:1});
          },
          function () {
              $(this).stop().animate({opacity:0});
          }
        );

    });
</script>
