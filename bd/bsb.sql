-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 04-Nov-2015 às 20:49
-- Versão do servidor: 10.0.17-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bsb`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_avaliacoes_produtos`
--

CREATE TABLE `tb_avaliacoes_produtos` (
  `idavaliacaoproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `nota` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_avaliacoes_produtos`
--

INSERT INTO `tb_avaliacoes_produtos` (`idavaliacaoproduto`, `nome`, `email`, `comentario`, `ativo`, `ordem`, `url_amigavel`, `id_produto`, `data`, `nota`) VALUES
(19, ' ', ' ', ' ', 'NAO', 0, ' ', 1, '2015-11-04', 0),
(20, 'Marcio', 'marciomas@gmail.com', 'Aqui fica o comentário.', 'SIM', 0, ' ', 1, '2015-11-04', 4),
(21, 'Marcio', 'marciomas@gmail.com', 'Aqui fica o comentário.', 'NAO', 0, ' ', 0, '2015-11-04', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners`
--

CREATE TABLE `tb_banners` (
  `idbanner` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`) VALUES
(31, 'Banner 1', '2810201508262688135326.jpg', 'SIM', NULL, '1', 'banner-1', 'http://www.globo.com/'),
(32, 'Banner 2', '2810201508267618811089.jpg', 'SIM', NULL, '1', 'banner-2', ''),
(33, 'Banner 3', '2810201508271540953537.jpg', 'SIM', NULL, '1', 'banner-3', ''),
(34, 'Promoção de Iluminação', '0211201505305992622801.jpg', 'SIM', NULL, '2', 'promocao-de-iluminacao', 'http://www.globo.com/');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_produtos`
--

CREATE TABLE `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desCategoria_Dica_Modelcricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `desCategoria_Dica_Modelcricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(45, 'PISCINAS EM VINIL', NULL, '0311201512516120088501.png', 'SIM', NULL, 'piscinas-em-vinil', NULL, NULL, NULL),
(46, 'AQUECEDOR PARA PISCINAS', NULL, '0311201512515426771847.png', 'SIM', NULL, 'aquecedor-para-piscinas', NULL, NULL, NULL),
(47, 'SAUNA E VAPOR', NULL, '0311201501014606375516.png', 'SIM', NULL, 'sauna-e-vapor', NULL, NULL, NULL),
(48, 'FILTROS E BOMBAS', NULL, '0311201512523630530242.png', 'SIM', NULL, 'filtros-e-bombas', NULL, NULL, NULL),
(49, 'CATEGORIA 1', NULL, '0311201512516120088501.png', 'SIM', NULL, NULL, NULL, NULL, NULL),
(50, 'CATEGORIA 2', NULL, '0311201512515426771847.png', 'SIM', NULL, NULL, NULL, NULL, NULL),
(51, 'CATEGORIA 3', NULL, '0311201501014606375516.png', 'SIM', NULL, NULL, NULL, NULL, NULL),
(52, 'CATEGORIA 4', NULL, '0311201512523630530242.png', 'SIM', NULL, NULL, NULL, NULL, NULL),
(53, 'CATEGORIA 5', NULL, '0311201512516120088501.png', 'SIM', NULL, NULL, NULL, NULL, NULL),
(54, 'CATEGORIA 6', NULL, '0311201512515426771847.png', 'SIM', NULL, NULL, NULL, NULL, NULL),
(55, 'CATEGORIA 7', NULL, '0311201501014606375516.png', 'SIM', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_dicas`
--

CREATE TABLE `tb_comentarios_dicas` (
  `idcomentariodica` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_dica` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_comentarios_dicas`
--

INSERT INTO `tb_comentarios_dicas` (`idcomentariodica`, `nome`, `email`, `comentario`, `ativo`, `ordem`, `url_amigavel`, `id_dica`, `data`) VALUES
(1, 'Marcio', 'marciomas@gmail.com', 'Aqui fica o comentário.', 'SIM', 0, ' ', 36, '2015-11-04');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_produtos`
--

CREATE TABLE `tb_comentarios_produtos` (
  `idcomentarioproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_servicos`
--

CREATE TABLE `tb_comentarios_servicos` (
  `idcomentarioservico` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_servico` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_comentarios_servicos`
--

INSERT INTO `tb_comentarios_servicos` (`idcomentarioservico`, `nome`, `email`, `comentario`, `ativo`, `ordem`, `url_amigavel`, `id_servico`, `data`) VALUES
(2, 'Marcio', 'marciomas@gmail.com', 'Aqui fica o comentário Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr.', 'SIM', 0, ' ', 35, '2015-11-04');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_configuracoes`
--

CREATE TABLE `tb_configuracoes` (
  `idconfiguracao` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `title_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `titulo`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `telefone3`, `telefone4`) VALUES
(1, 'Configurações', 'TTT', '', '', 'SIM', 0, 'configuracoes', 'Quadra 100 Lote 2', '(61)3425-0987', '(61)3425-2123', 'marciomas@gmail.com', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3837.7291625819453!2d-47.97135358455928!3d-15.870825829068952!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a2e59437b3569%3A0x3f2e953cec8c1a02!2sBSB+Piscinas!5e0!3m2!1spt-BR!2sbr!4v14458693176', NULL, NULL, '', '(61)3425-6544', '(61)3425-0098');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_depoimentos`
--

CREATE TABLE `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `depoimento` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_depoimentos`
--

INSERT INTO `tb_depoimentos` (`iddepoimento`, `titulo`, `depoimento`, `ativo`, `ordem`, `url_amigavel`, `imagem`) VALUES
(1, 'Maria e Paulo', '<p>\r\n	A BSB Piscinas oferece constru&ccedil;&atilde;o e reformas de piscinas de azulejos paginados, em vinil ou de pastilhas de cer&acirc;mica e vidro. Instala&ccedil;&atilde;o de equipamentos em geral, assist&ecirc;ncia t&eacute;cnica especializada, aquecimen to de piscinas, vendas e instala&ccedil;&atilde;o de saunas, equipamentos e aces s&oacute;rios, duchas e cascatas, produtos qu&iacute;micos em geral e muito mais&nbsp;A BSB Piscinas oferece constru&ccedil;&atilde;o e reformas de piscinas de azulejos paginados, em vinil ou de pastilhas de cer&acirc;mica e vidro. Instala&ccedil;&atilde;o de equipamentos em geral, assist&ecirc;ncia t&eacute;cnica especializada, aquecimen to de piscinas, vendas e instala&ccedil;&atilde;o de saunas, equipamentos e aces s&oacute;rios, duchas e cascatas, produtos qu&iacute;micos em geral e muito mais</p>', 'SIM', NULL, 'maria-e-paulo', '2810201511229672371703..jpg'),
(2, 'João Alves', '<p>\r\n	A BSB Piscinas oferece constru&ccedil;&atilde;o e reformas de piscinas de azulejos paginados, em vinil ou de pastilhas de cer&acirc;mica e vidro. Instala&ccedil;&atilde;o de equipamentos em geral, assist&ecirc;ncia t&eacute;cnica especializada, aquecimen to de piscinas, vendas e instala&ccedil;&atilde;o de saunas, equipamentos e aces s&oacute;rios, duchas e cascatas, produtos qu&iacute;micos em geral e muito mais</p>', 'SIM', NULL, 'joao-alves', '2810201511226531449891..jpg'),
(3, 'Gabriella', '<p>\r\n	A BSB Piscinas oferece constru&ccedil;&atilde;o e reformas de piscinas de azulejos paginados, em vinil ou de pastilhas de cer&acirc;mica e vidro. Instala&ccedil;&atilde;o de equipamentos em geral, assist&ecirc;ncia t&eacute;cnica especializada, aquecimen to de piscinas, vendas e instala&ccedil;&atilde;o de saunas, equipamentos e aces s&oacute;rios, duchas e cascatas, produtos qu&iacute;micos em geral e muito mais</p>', 'SIM', NULL, 'gabriella', '2810201511225013969879..jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_dicas`
--

CREATE TABLE `tb_dicas` (
  `iddica` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(36, 'Cuidar da piscina ? Dicas para manter a piscina sempre linda.', '<p>\r\n	Os benef&iacute;cios de ter uma piscina em casa s&atilde;o enormes. Acordar cedo e dar um mergulho &eacute; uma vantagem que muitos querem e nem todos podem. E apesar de ser um enorme e irrecus&aacute;vel privil&eacute;gio, ter uma piscina em casa tamb&eacute;m nos traz algumas responsabilidades.</p>\r\n<p>\r\n	<br />\r\n	Pode-se imaginar que a piscina &eacute; como um carro: Se voc&ecirc; n&atilde;o cuidar, acaba dando problema, e se n&atilde;o tratar de resolver o problema pode acabar o perdendo. Nesta linha de racioc&iacute;nio, manter a piscina sempre limpa &eacute; algo que deve ser feito constantemente, n&atilde;o importando o tipo da piscina e muito menos se ela est&aacute; ou n&atilde;o em uso.<br />\r\n	Na realidade existem uma maneira de interromper o uso da piscina, &eacute; o que chamamos de hiberna&ccedil;&atilde;o da piscina, mas at&eacute; isto demanda um certo cuidado. Confira no texto: &ldquo;Interrompendo o uso da piscina &ndash; Como hibernar a piscina&ldquo;.<br />\r\n	Hoje vamos dar a voc&ecirc; algumas dicas para cuidar da piscina e evitar que os problemas apare&ccedil;am. Vamos l&aacute;?</p>', '0411201502455354965122.jpeg', 'SIM', NULL, 'cuidar-da-piscina--dicas-para-manter-a-piscina-sempre-linda', NULL, NULL, NULL, NULL),
(37, 'Como manter a água da piscina cristalina e saudável', '<p>\r\n	Se voc&ecirc; quer saber como manter a &aacute;gua da piscina cristalina e saud&aacute;vel, chegou ao lugar certo!<br />\r\n	N&atilde;o vamos te dar um segredo m&aacute;gico pra isso pois definitivamente n&atilde;o h&aacute;!</p>\r\n<p>\r\n	<br />\r\n	Todos n&oacute;s sabemos que uma piscina l&iacute;mpida e com sa&uacute;de &eacute; geralmente fruto de um acompanhamento constante.<br />\r\n	Para um bom come&ccedil;o, indicamos a leitura de dois artigos: a import&acirc;ncia do controle do pH da piscina e a import&acirc;ncia do controle da alcalinidade da piscina.</p>\r\n<p>\r\n	De qualquer forma, vale ressaltar que n&atilde;o &eacute; nenhum bicho de sete cabe&ccedil;as deixar a piscina assim, cristalina e saud&aacute;vel.</p>\r\n<p>\r\n	Vamos ent&atilde;o &agrave; nossa dica de como manter a piscina cristalina&hellip;</p>\r\n<p>\r\n	Como manter a &aacute;gua da piscina cristalina e saud&aacute;vel</p>\r\n<p>\r\n	Utilizando flutuador para manter a &aacute;gua da piscina cristalina e saud&aacute;velNossa principal dica para manter a &aacute;gua da piscina sempre clarinha e bem cuidada &eacute; a utiliza&ccedil;&atilde;o de cloradores flutuantes, ou como alguns chamam: margaridas!<br />\r\n	Al&eacute;m de todos os passos necess&aacute;rios para se ter uma piscina cristalina &ndash; manter o controle do pH, controle da alcalinidade, controle do cloro, realizar o tratamento f&iacute;sico da piscina e etc &ndash; n&oacute;s indicamos fortemente a utiliza&ccedil;&atilde;o de cloradores flutuantes para manter a &aacute;gua da piscina cristalina e saud&aacute;vel!<br />\r\n	Esse clorador flutuante foi desenvolvido para fazer com que o tratamento contra bact&eacute;rias e micro organismos da piscina seja mais tranquilo para o limpador da piscina ou at&eacute; mesmo o propriet&aacute;rio da piscina.<br />\r\n	Regule o tamanho dos orif&iacute;cios do flutuadorOs tabletes de cloro v&atilde;o se dissolvendo aos poucos dentro do flutuador e mantendo o residual de cloro da piscina.<br />\r\n	Como consequ&ecirc;ncia voc&ecirc; mant&eacute;m o aspecto da piscina mais cristalino e sua qu&iacute;mica mais &lsquo;saud&aacute;vel&rsquo; para os banhistas uma vez que a quantidade de cloro na piscina tende a ser mais uniforme.<br />\r\n	Veja como &eacute; f&aacute;cil tratar a sua piscina com os flutuadores:<br />\r\n	Coloque o flutuador na piscina1) Escolha o flutuador de acordo com o tamanho da sua piscina.<br />\r\n	O pequeno para piscinas entre 15 e 30.000 litros, e o grande para piscinas entre 30 e 70.000 litros. Por&eacute;m n&atilde;o se prenda a estes valores pois voc&ecirc; poder&aacute; comprar mais de um clorador caso haja necessidade.<br />\r\n	<br />\r\n	Se o flutuador tombar ou afundar, est&aacute; na hora de trocar!2) Verifique a Alcalinidade (entre 80 e 120 ppm) e o pH (entre 7,2 e 7,6) ajustando-os se necess&aacute;rio.<br />\r\n	3) Girando a base do flutuador ajuste o tamanho dos orif&iacute;cios de acordo com o tamanho da sua piscina.<br />\r\n	Quanto maior ficar o buraco no flutuador, mais r&aacute;pido o cloro ir&aacute; passar para a piscina.<br />\r\n	Retire os tabletes se for usar a piscina4) Caso queira utilizar a piscina, retire o flutuador e coloque-o num balde com &aacute;gua da pr&oacute;pria piscina.<br />\r\n	Depois do uso retorne com a &aacute;gua do balde e o flutuador para dentro da piscina.<br />\r\n	Os flutuadores fazem grande parte do trabalho de tratamento qu&iacute;mico da piscina para voc&ecirc;, na dosagem correta e sem desperd&iacute;cio de produto.<br />\r\n	Somente ser&aacute; necess&aacute;rio a limpeza da borda da piscina e a aspira&ccedil;&atilde;o do fundoBasta encontrar a quantidade certa de pastilhas de cloro s&atilde;o necess&aacute;rias para manter sua piscina clorada por dia ou por semana&hellip;<br />\r\n	Somente ser&aacute; necess&aacute;rio efetuar a aspira&ccedil;&atilde;o e limpeza das bordas. (N&atilde;o deixe de ler: a maneira correta de limpar a borda da piscina)<br />\r\n	Feito isso, voc&ecirc; ter&aacute; a &aacute;gua da piscina cristalina e saud&aacute;vel como voc&ecirc; deseja!</p>', '0411201502473523317765.jpeg', 'SIM', NULL, 'como-manter-a-agua-da-piscina-cristalina-e-saudavel', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_empresa`
--

CREATE TABLE `tb_empresa` (
  `idempresa` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'Index - CONHEÇA UM POUCO MAIS A BSB PISCINAS', '<p>\r\n	A BSB Piscinas oferece constru&ccedil;&atilde;o e reformas de piscinas de azulejos paginados, em vinil ou de pastilhas de cer&acirc;mica e vidro.</p>\r\n<p>\r\n	Instala&ccedil;&atilde;o de equipamentos em geral, assist&ecirc;ncia t&eacute;cnica especializada, aquecimento de piscinas, vendas e instala&ccedil;&atilde;o de saunas, equipamentos e acess&oacute;rios, duchas e cascatas, produtos qu&iacute;micos em geral e muito mais.</p>\r\n<p>\r\n	A empresa trabalha com profissionais experientes e altamente qualificados para melhor atend&ecirc;-lo..</p>', 'SIM', 0, '', '', '', 'index--conheca-um-pouco-mais-a-bsb-piscinas', NULL, NULL, NULL),
(2, 'A Empresa', '<p>\r\n	A BSB Piscinas oferece constru&ccedil;&atilde;o e reformas de piscinas de azulejos paginados, em vinil ou de pastilhas de cer&acirc;mica e vidro.</p>\r\n<p>\r\n	Instala&ccedil;&atilde;o de equipamentos em geral, assist&ecirc;ncia t&eacute;cnica especializada, aquecimento de piscinas, vendas e instala&ccedil;&atilde;o de saunas, equipamentos e acess&oacute;rios, duchas e cascatas, produtos qu&iacute;micos em geral e muito mais.<br />\r\n	A empresa trabalha com profissionais experientes e altamente qualificados para melhor atend&ecirc;-lo.</p>', 'SIM', 0, '', '', '', 'a-empresa', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_portifolios`
--

CREATE TABLE `tb_galerias_portifolios` (
  `idgaleriaportifolio` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_portifolio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_portifolios`
--

INSERT INTO `tb_galerias_portifolios` (`idgaleriaportifolio`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_portifolio`) VALUES
(1, '0411201506239651027830.jpeg', 'SIM', NULL, NULL, 1),
(2, '0411201506235183670363.jpg', 'SIM', NULL, NULL, 1),
(3, '0411201506234140090975.jpg', 'SIM', NULL, NULL, 1),
(4, '0411201506234406818358.jpeg', 'SIM', NULL, NULL, 2),
(5, '0411201506236145845365.jpg', 'SIM', NULL, NULL, 1),
(6, '0411201506233839700569.jpg', 'SIM', NULL, NULL, 2),
(7, '0411201506233631361822.jpg', 'SIM', NULL, NULL, 2),
(8, '0411201506236449683846.jpg', 'SIM', NULL, NULL, 2),
(9, '0411201506236134491943.jpg', 'SIM', NULL, NULL, 1),
(10, '0411201506235130425782.jpg', 'SIM', NULL, NULL, 2),
(11, '0411201506239262168254.jpeg', 'SIM', NULL, NULL, 2),
(12, '0411201506231635387692.jpg', 'SIM', NULL, NULL, 2),
(13, '0411201506239128339576.jpg', 'SIM', NULL, NULL, 2),
(14, '0411201506232502181289.jpeg', 'SIM', NULL, NULL, 1),
(15, '0411201506235205096461.jpg', 'SIM', NULL, NULL, 2),
(16, '0411201506238175841714.jpg', 'SIM', NULL, NULL, 2),
(17, '0411201506232243622206.jpg', 'SIM', NULL, NULL, 2),
(18, '0411201506238541443910.jpg', 'SIM', NULL, NULL, 1),
(19, '0411201506236506189091.jpg', 'SIM', NULL, NULL, 1),
(20, '0411201506237385737010.jpg', 'SIM', NULL, NULL, 1),
(21, '0411201506232647089867.jpg', 'SIM', NULL, NULL, 1),
(22, '0411201506237731989535.jpg', 'SIM', NULL, NULL, 1),
(23, '0411201508063481073379.jpeg', 'SIM', NULL, NULL, 3),
(24, '0411201508062845037190.jpg', 'SIM', NULL, NULL, 3),
(25, '0411201508061842153291.jpeg', 'SIM', NULL, NULL, 4),
(26, '0411201508064368237116.jpg', 'SIM', NULL, NULL, 3),
(27, '0411201508062662440124.jpg', 'SIM', NULL, NULL, 4),
(28, '0411201508068443491136.jpg', 'SIM', NULL, NULL, 3),
(29, '0411201508065434382479.jpg', 'SIM', NULL, NULL, 4),
(30, '0411201508063214953111.jpg', 'SIM', NULL, NULL, 3),
(31, '0411201508065924873422.jpeg', 'SIM', NULL, NULL, 5),
(32, '0411201508061267982981.jpg', 'SIM', NULL, NULL, 5),
(33, '0411201508061210565587.jpg', 'SIM', NULL, NULL, 4),
(34, '0411201508066361786793.jpeg', 'SIM', NULL, NULL, 3),
(35, '0411201508067481603328.jpg', 'SIM', NULL, NULL, 5),
(36, '0411201508062542211918.jpg', 'SIM', NULL, NULL, 4),
(37, '0411201508064004338176.jpg', 'SIM', NULL, NULL, 3),
(38, '0411201508063154635007.jpg', 'SIM', NULL, NULL, 5),
(39, '0411201508064547286650.jpeg', 'SIM', NULL, NULL, 4),
(40, '0411201508064579786923.jpg', 'SIM', NULL, NULL, 3),
(41, '0411201508068873735496.jpg', 'SIM', NULL, NULL, 5),
(42, '0411201508069395706616.jpg', 'SIM', NULL, NULL, 4),
(43, '0411201508062701918143.jpeg', 'SIM', NULL, NULL, 5),
(44, '0411201508066810686480.jpg', 'SIM', NULL, NULL, 3),
(45, '0411201508063384246623.jpg', 'SIM', NULL, NULL, 4),
(46, '0411201508062814928363.jpg', 'SIM', NULL, NULL, 5),
(47, '0411201508062766676346.jpg', 'SIM', NULL, NULL, 3),
(48, '0411201508077544390985.jpg', 'SIM', NULL, NULL, 4),
(49, '0411201508078078659425.jpg', 'SIM', NULL, NULL, 5),
(50, '0411201508072364095677.jpg', 'SIM', NULL, NULL, 3),
(51, '0411201508074010415176.jpg', 'SIM', NULL, NULL, 4),
(52, '0411201508072190840260.jpg', 'SIM', NULL, NULL, 5),
(53, '0411201508072022096220.jpg', 'SIM', NULL, NULL, 4),
(54, '0411201508078380788030.jpg', 'SIM', NULL, NULL, 5),
(55, '0411201508078412186913.jpg', 'SIM', NULL, NULL, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_produtos`
--

CREATE TABLE `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(60, '0411201512378861499846.jpg', 'SIM', NULL, NULL, 1),
(61, '0411201512375680738633.jpg', 'SIM', NULL, NULL, 1),
(62, '0411201512377488805792.jpg', 'SIM', NULL, NULL, 1),
(63, '0411201512377394942070.jpg', 'SIM', NULL, NULL, 2),
(64, '0411201512376991435645.jpg', 'SIM', NULL, NULL, 1),
(65, '0411201512372540643177.jpg', 'SIM', NULL, NULL, 2),
(66, '0411201512376876153751.jpg', 'SIM', NULL, NULL, 1),
(67, '0411201512375633686569.jpg', 'SIM', NULL, NULL, 3),
(68, '0411201512372393239190.jpg', 'SIM', NULL, NULL, 3),
(69, '0411201512371952858500.jpg', 'SIM', NULL, NULL, 2),
(70, '0411201512379939975096.jpg', 'SIM', NULL, NULL, 1),
(71, '0411201512379966245426.jpg', 'SIM', NULL, NULL, 3),
(72, '0411201512376187474549.jpg', 'SIM', NULL, NULL, 2),
(73, '0411201512375143316897.jpg', 'SIM', NULL, NULL, 4),
(74, '0411201512374877993687.jpg', 'SIM', NULL, NULL, 2),
(75, '0411201512377018904437.jpg', 'SIM', NULL, NULL, 3),
(76, '0411201512373950905505.jpg', 'SIM', NULL, NULL, 5),
(77, '0411201512377911590577.jpg', 'SIM', NULL, NULL, 4),
(78, '0411201512372960734404.jpg', 'SIM', NULL, NULL, 5),
(79, '0411201512374669137919.jpg', 'SIM', NULL, NULL, 5),
(80, '0411201512379297010215.jpg', 'SIM', NULL, NULL, 2),
(81, '0411201512374583601160.jpg', 'SIM', NULL, NULL, 3),
(82, '0411201512376096030895.jpg', 'SIM', NULL, NULL, 5),
(83, '0411201512376084787059.jpg', 'SIM', NULL, NULL, 5),
(84, '0411201512376785101364.jpg', 'SIM', NULL, NULL, 4),
(85, '0411201512376059426503.jpg', 'SIM', NULL, NULL, 3),
(86, '0411201512373721688120.jpg', 'SIM', NULL, NULL, 4),
(87, '0411201512372820774983.jpg', 'SIM', NULL, NULL, 5),
(88, '0411201512371872003670.jpg', 'SIM', NULL, NULL, 6),
(89, '0411201512374021880535.jpg', 'SIM', NULL, NULL, 6),
(90, '0411201512371451600760.jpg', 'SIM', NULL, NULL, 4),
(91, '0411201512376665309750.jpg', 'SIM', NULL, NULL, 6),
(92, '0411201512379068817322.jpg', 'SIM', NULL, NULL, 4),
(93, '0411201512376274125311.jpg', 'SIM', NULL, NULL, 6),
(94, '0411201512377250133078.jpg', 'SIM', NULL, NULL, 6),
(95, '0411201512374498628348.jpg', 'SIM', NULL, NULL, 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins`
--

CREATE TABLE `tb_logins` (
  `idlogin` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `nome`, `senha`, `ativo`, `id_grupologin`, `email`) VALUES
(4, 'HomeWeb', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logs_logins`
--

CREATE TABLE `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1575, 'EXCLUSÃO DO LOGIN 30, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''30''', '2015-10-28', '20:23:46', 4),
(1576, 'ALTERAÇÃO DO CLIENTE ', '', '2015-10-28', '21:25:09', 4),
(1577, 'ALTERAÇÃO DO CLIENTE ', '', '2015-10-28', '22:52:55', 4),
(1578, 'CADASTRO DO CLIENTE ', '', '2015-10-28', '23:06:06', 4),
(1579, 'CADASTRO DO CLIENTE ', '', '2015-10-28', '23:06:49', 4),
(1580, 'CADASTRO DO CLIENTE ', '', '2015-10-28', '23:07:14', 4),
(1581, 'ALTERAÇÃO DO CLIENTE ', '', '2015-10-28', '23:18:54', 4),
(1582, 'ALTERAÇÃO DO CLIENTE ', '', '2015-10-28', '23:22:02', 4),
(1583, 'ALTERAÇÃO DO CLIENTE ', '', '2015-10-28', '23:22:13', 4),
(1584, 'ALTERAÇÃO DO CLIENTE ', '', '2015-10-28', '23:22:59', 4),
(1585, 'CADASTRO DO CLIENTE ', '', '2015-11-02', '15:28:45', 4),
(1586, 'CADASTRO DO CLIENTE ', '', '2015-11-02', '15:29:15', 4),
(1587, 'CADASTRO DO CLIENTE ', '', '2015-11-02', '15:29:27', 4),
(1588, 'CADASTRO DO CLIENTE ', '', '2015-11-02', '15:29:35', 4),
(1589, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-02', '15:30:01', 4),
(1590, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '12:51:39', 4),
(1591, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '12:51:50', 4),
(1592, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '12:52:06', 4),
(1593, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '12:52:29', 4),
(1594, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '13:01:28', 4),
(1595, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '13:01:44', 4),
(1596, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '13:01:59', 4),
(1597, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '13:02:16', 4),
(1598, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '13:02:26', 4),
(1599, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '13:16:30', 4),
(1600, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '13:16:59', 4),
(1601, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '13:17:40', 4),
(1602, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '13:18:18', 4),
(1603, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '13:18:55', 4),
(1604, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '19:37:44', 4),
(1605, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '19:41:57', 4),
(1606, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '19:59:41', 4),
(1607, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '20:00:17', 4),
(1608, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '20:04:24', 4),
(1609, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '20:05:46', 4),
(1610, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '01:47:30', 4),
(1611, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '01:49:43', 4),
(1612, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '01:57:12', 4),
(1613, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '02:45:47', 4),
(1614, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '02:47:42', 4),
(1615, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '02:56:06', 4),
(1616, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '02:59:14', 4),
(1617, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '10:33:57', 4),
(1618, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '16:09:25', 4),
(1619, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '16:10:40', 4),
(1620, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '18:19:51', 4),
(1621, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '18:20:14', 4),
(1622, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '18:20:33', 4),
(1623, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '18:21:44', 4),
(1624, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '18:21:59', 4),
(1625, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '18:22:22', 4),
(1626, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '18:22:46', 4),
(1627, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '18:22:55', 4),
(1628, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '18:23:05', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_parceiros`
--

CREATE TABLE `tb_parceiros` (
  `idparceiro` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_parceiros`
--

INSERT INTO `tb_parceiros` (`idparceiro`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(36, 'IBM', NULL, '0211201503282460764567..jpg', 'SIM', NULL, 'ibm', NULL, NULL, NULL, NULL),
(37, 'BT', NULL, '0211201503294581665971..jpg', 'SIM', NULL, 'bt', NULL, NULL, NULL, NULL),
(38, 'Carrefour', NULL, '0211201503303237260079..jpg', 'SIM', NULL, 'carrefour', NULL, NULL, NULL, NULL),
(39, 'CK', NULL, '0211201503295867157201..jpg', 'SIM', NULL, 'ck', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_portifolios`
--

CREATE TABLE `tb_portifolios` (
  `idportifolio` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_portifolios`
--

INSERT INTO `tb_portifolios` (`idportifolio`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Portifólio 1', '0411201506229681114678.jpeg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, NULL, NULL, 'SIM', NULL, 'portifolio-1'),
(2, 'Portifólio 2', '0411201506223606474871..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, NULL, NULL, 'SIM', NULL, 'portifolio-2'),
(3, 'Portifólio 3', '0411201506235768058577..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, NULL, NULL, 'SIM', NULL, 'portifolio-3'),
(4, 'Portifólio 4', '0411201506214453638978..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, NULL, NULL, 'SIM', NULL, 'portifolio-4'),
(5, 'Portifólio 5', '0411201506211880079429..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, NULL, NULL, 'SIM', NULL, 'portifolio-5'),
(6, 'Portifólio 6', '0411201506221576606424..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, NULL, NULL, 'SIM', NULL, 'portifolio-6');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

CREATE TABLE `tb_produtos` (
  `idproduto` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) UNSIGNED NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`) VALUES
(1, 'Piscina concreto armado 20', '0311201501163261914327..jpg', '<p>\r\n	Piscina de concreto&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'piscina-concreto-armado-20', 45, 'INOX', NULL, NULL),
(2, 'Alvenaria Padrão', '0311201501163130524849..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'alvenaria-padrao', 45, 'TIGRE', NULL, NULL),
(3, 'Piscinas de azulejo', '0311201501174414952268..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'piscinas-de-azulejo', 45, 'AMIS', NULL, NULL),
(4, 'Piscinas de Vinil Viniltec', '0311201501185986939817..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'piscinas-de-vinil-viniltec', 45, 'TUWER', NULL, NULL),
(5, 'Piscina de vinil 2', '0311201501189363978082..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'piscina-de-vinil-2', 45, 'AMARU', NULL, NULL),
(6, 'Filtro e Bomba p/ Piscina Desmontável', '0311201507419294189131..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'filtro-e-bomba-p-piscina-desmontavel', 48, 'FILTRA MAIS', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_servicos`
--

CREATE TABLE `tb_servicos` (
  `idservico` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) UNSIGNED NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`) VALUES
(35, 'Construção de Piscina', '<p>\r\n	Constru&ccedil;&atilde;o de piscina&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0411201501574373335382..jpg', 'SIM', NULL, 'construcao-de-piscina', '', '', '', 0, '', ''),
(36, 'Limpeza de Piscina', '<p>\r\n	Limpeza de piscina&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0411201501497639182739..jpg', 'SIM', NULL, 'limpeza-de-piscina', '', '', '', 0, '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  ADD PRIMARY KEY (`idavaliacaoproduto`);

--
-- Indexes for table `tb_banners`
--
ALTER TABLE `tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- Indexes for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  ADD PRIMARY KEY (`idcategoriaproduto`);

--
-- Indexes for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  ADD PRIMARY KEY (`idcomentariodica`);

--
-- Indexes for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  ADD PRIMARY KEY (`idcomentarioproduto`);

--
-- Indexes for table `tb_comentarios_servicos`
--
ALTER TABLE `tb_comentarios_servicos`
  ADD PRIMARY KEY (`idcomentarioservico`);

--
-- Indexes for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  ADD PRIMARY KEY (`idconfiguracao`);

--
-- Indexes for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  ADD PRIMARY KEY (`iddepoimento`);

--
-- Indexes for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  ADD PRIMARY KEY (`iddica`);

--
-- Indexes for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indexes for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  ADD PRIMARY KEY (`idgaleriaportifolio`);

--
-- Indexes for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  ADD PRIMARY KEY (`id_galeriaproduto`);

--
-- Indexes for table `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`idlogin`);

--
-- Indexes for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  ADD PRIMARY KEY (`idloglogin`);

--
-- Indexes for table `tb_parceiros`
--
ALTER TABLE `tb_parceiros`
  ADD PRIMARY KEY (`idparceiro`);

--
-- Indexes for table `tb_portifolios`
--
ALTER TABLE `tb_portifolios`
  ADD PRIMARY KEY (`idportifolio`);

--
-- Indexes for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`idproduto`);

--
-- Indexes for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  ADD PRIMARY KEY (`idservico`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  MODIFY `idavaliacaoproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tb_banners`
--
ALTER TABLE `tb_banners`
  MODIFY `idbanner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  MODIFY `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  MODIFY `idcomentariodica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  MODIFY `idcomentarioproduto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_comentarios_servicos`
--
ALTER TABLE `tb_comentarios_servicos`
  MODIFY `idcomentarioservico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  MODIFY `idconfiguracao` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  MODIFY `iddepoimento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  MODIFY `iddica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  MODIFY `idempresa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  MODIFY `idgaleriaportifolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  MODIFY `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  MODIFY `idloglogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1629;
--
-- AUTO_INCREMENT for table `tb_parceiros`
--
ALTER TABLE `tb_parceiros`
  MODIFY `idparceiro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `tb_portifolios`
--
ALTER TABLE `tb_portifolios`
  MODIFY `idportifolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `idproduto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  MODIFY `idservico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
