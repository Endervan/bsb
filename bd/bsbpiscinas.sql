-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: mysql01.bsbpiscinas.hospedagemdesites.ws
-- Generation Time: 24-Mar-2016 às 08:39
-- Versão do servidor: 5.6.21-69.0-log
-- PHP Version: 5.6.14-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bsbpiscinas`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_avaliacoes_produtos`
--

CREATE TABLE `tb_avaliacoes_produtos` (
  `idavaliacaoproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `nota` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_avaliacoes_produtos`
--

INSERT INTO `tb_avaliacoes_produtos` (`idavaliacaoproduto`, `nome`, `email`, `comentario`, `ativo`, `ordem`, `url_amigavel`, `id_produto`, `data`, `nota`) VALUES
(1, 'aqkbeffk', 'sample@email.tst', '1', 'NAO', 0, ' ', 48, '2016-01-20', 5),
(2, 'fforofgs', 'sample@email.tst', '1', 'NAO', 0, ' ', 48, '2016-01-20', 4),
(3, 'qmldbsuw', 'sample@email.tst', '1', 'NAO', 0, ' ', 48, '2016-01-20', 3),
(4, 'esginlnt', 'sample@email.tst', '1', 'NAO', 0, ' ', 56, '2016-01-20', 5),
(5, 'vxqhcpcc', 'sample@email.tst', '1', 'NAO', 0, ' ', 56, '2016-01-20', 4),
(6, 'ghtimgff', 'sample@email.tst', '1', 'NAO', 0, ' ', 56, '2016-01-20', 3),
(7, 'kkfopfld', 'sample@email.tst', '1', 'NAO', 0, ' ', 71, '2016-01-20', 5),
(8, 'muapalie', 'sample@email.tst', '1', 'NAO', 0, ' ', 71, '2016-01-20', 4),
(9, 'axfumigh', 'sample@email.tst', '1', 'NAO', 0, ' ', 71, '2016-01-20', 3),
(10, 'edsllstr', 'sample@email.tst', '1', 'NAO', 0, ' ', 17, '2016-01-20', 5),
(11, 'apbxybnt', 'sample@email.tst', '1', 'NAO', 0, ' ', 17, '2016-01-20', 4),
(12, 'tcsinpog', 'sample@email.tst', '1', 'NAO', 0, ' ', 17, '2016-01-20', 3),
(13, 'jtwxevrq', 'sample@email.tst', '1', 'NAO', 0, ' ', 68, '2016-01-20', 5),
(14, 'jggtwxbp', 'sample@email.tst', '1', 'NAO', 0, ' ', 68, '2016-01-20', 4),
(15, 'ieinayuu', 'sample@email.tst', '1', 'NAO', 0, ' ', 68, '2016-01-20', 3),
(16, 'whhxkcal', 'sample@email.tst', '1', 'NAO', 0, ' ', 63, '2016-01-20', 5),
(17, 'vthiymtj', 'sample@email.tst', '1', 'NAO', 0, ' ', 63, '2016-01-20', 4),
(18, 'tjwsvmvt', 'sample@email.tst', '1', 'NAO', 0, ' ', 63, '2016-01-20', 3),
(19, 'wgyxojtu', 'sample@email.tst', '1', 'NAO', 0, ' ', 51, '2016-01-20', 5),
(20, 'hiddcuwf', 'sample@email.tst', '1', 'NAO', 0, ' ', 51, '2016-01-20', 3),
(21, 'ddqesumi', 'sample@email.tst', '1', 'NAO', 0, ' ', 51, '2016-01-20', 4),
(22, 'umvmiqhr', 'sample@email.tst', '1', 'NAO', 0, ' ', 36, '2016-01-20', 5),
(23, 'kmhbfvhd', 'sample@email.tst', '1', 'NAO', 0, ' ', 36, '2016-01-20', 4),
(24, 'mdyswrmc', 'sample@email.tst', '1', 'NAO', 0, ' ', 36, '2016-01-20', 3),
(25, 'xamkwand', 'sample@email.tst', '1', 'NAO', 0, ' ', 7, '2016-01-20', 3),
(26, 'sxndhaue', 'sample@email.tst', '1', 'NAO', 0, ' ', 7, '2016-01-20', 5),
(27, 'qtumsbig', 'sample@email.tst', '1', 'NAO', 0, ' ', 7, '2016-01-20', 4),
(28, 'vvyvvwyl', 'sample@email.tst', '1', 'NAO', 0, ' ', 45, '2016-01-20', 5),
(29, 'rsfhgkhb', 'sample@email.tst', '1', 'NAO', 0, ' ', 45, '2016-01-20', 4),
(30, 'twxoorci', 'sample@email.tst', '1', 'NAO', 0, ' ', 45, '2016-01-20', 3),
(31, 'kwhvrydr', 'sample@email.tst', '1', 'NAO', 0, ' ', 39, '2016-01-20', 5),
(32, 'spdujbej', 'sample@email.tst', '1', 'NAO', 0, ' ', 39, '2016-01-20', 4),
(33, 'vdtcgbtd', 'sample@email.tst', '1', 'NAO', 0, ' ', 39, '2016-01-20', 3),
(34, 'wouvtwni', 'sample@email.tst', '1', 'NAO', 0, ' ', 62, '2016-01-20', 5),
(35, 'lahoxipi', 'sample@email.tst', '1', 'NAO', 0, ' ', 62, '2016-01-20', 4),
(36, 'oebuyxwm', 'sample@email.tst', '1', 'NAO', 0, ' ', 62, '2016-01-20', 3),
(37, 'nouungnt', 'sample@email.tst', '1', 'NAO', 0, ' ', 47, '2016-01-20', 5),
(38, 'wwjmkafk', 'sample@email.tst', '1', 'NAO', 0, ' ', 47, '2016-01-20', 4),
(39, 'dwyxcpga', 'sample@email.tst', '1', 'NAO', 0, ' ', 47, '2016-01-20', 3),
(40, 'pbovllot', 'sample@email.tst', '1', 'NAO', 0, ' ', 46, '2016-01-20', 5),
(41, 'axbjgmow', 'sample@email.tst', '1', 'NAO', 0, ' ', 46, '2016-01-20', 4),
(42, 'grpwiqkg', 'sample@email.tst', '1', 'NAO', 0, ' ', 46, '2016-01-20', 3),
(43, 'jrsebqgj', 'sample@email.tst', '1', 'NAO', 0, ' ', 16, '2016-01-20', 5),
(44, 'ptmyifmy', 'sample@email.tst', '1', 'NAO', 0, ' ', 16, '2016-01-20', 4),
(45, 'lqtedhib', 'sample@email.tst', '1', 'NAO', 0, ' ', 16, '2016-01-20', 3),
(46, 'oxpnnvxy', 'sample@email.tst', '1', 'NAO', 0, ' ', 25, '2016-01-20', 5),
(47, 'epibkuij', 'sample@email.tst', '1', 'NAO', 0, ' ', 25, '2016-01-20', 3),
(48, 'ftwsbeoc', 'sample@email.tst', '1', 'NAO', 0, ' ', 25, '2016-01-20', 4),
(49, 'fkmmphsi', 'sample@email.tst', '1', 'NAO', 0, ' ', 41, '2016-01-20', 5),
(50, 'iyydhthk', 'sample@email.tst', '1', 'NAO', 0, ' ', 41, '2016-01-20', 4),
(51, 'iebstign', 'sample@email.tst', '1', 'NAO', 0, ' ', 41, '2016-01-20', 3),
(52, 'jtiwnmgx', 'sample@email.tst', '1', 'NAO', 0, ' ', 34, '2016-01-20', 5),
(53, 'osgesbli', 'sample@email.tst', '1', 'NAO', 0, ' ', 34, '2016-01-20', 4),
(54, 'jxlxopxl', 'sample@email.tst', '1', 'NAO', 0, ' ', 34, '2016-01-20', 3),
(55, 'dherbvbj', 'sample@email.tst', '1', 'NAO', 0, ' ', 24, '2016-01-20', 5),
(56, 'dovdpctm', 'sample@email.tst', '1', 'NAO', 0, ' ', 24, '2016-01-20', 4),
(57, 'satlxxcw', 'sample@email.tst', '1', 'NAO', 0, ' ', 24, '2016-01-20', 3),
(58, 'pflcljum', 'sample@email.tst', '1', 'NAO', 0, ' ', 44, '2016-01-20', 5),
(59, 'jeglwsnb', 'sample@email.tst', '1', 'NAO', 0, ' ', 44, '2016-01-20', 3),
(60, 'okdmpwmu', 'sample@email.tst', '1', 'NAO', 0, ' ', 44, '2016-01-20', 4),
(61, 'fptbvwnr', 'sample@email.tst', '1', 'NAO', 0, ' ', 43, '2016-01-20', 5),
(62, 'bcjqrfql', 'sample@email.tst', '1', 'NAO', 0, ' ', 43, '2016-01-20', 4),
(63, 'yyvmedla', 'sample@email.tst', '1', 'NAO', 0, ' ', 43, '2016-01-20', 3),
(64, 'nmcuiqlg', 'sample@email.tst', '1', 'NAO', 0, ' ', 30, '2016-01-20', 5),
(65, 'iixyokvd', 'sample@email.tst', '1', 'NAO', 0, ' ', 30, '2016-01-20', 4),
(66, 'enhoaeqf', 'sample@email.tst', '1', 'NAO', 0, ' ', 30, '2016-01-20', 3),
(67, 'Mark', 'mark357177@hotmail.com', 'nEtO6j http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 'NAO', 0, ' ', 41, '2016-01-30', 2),
(68, 'Mark', 'mark357177@hotmail.com', 'NHGUDG http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 'NAO', 0, ' ', 40, '2016-01-30', 5),
(69, 'Mark', 'mark357177@hotmail.com', 'PGFH2J http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 'NAO', 0, ' ', 68, '2016-01-30', 5),
(70, 'Mark', 'mark357177@hotmail.com', '2dlgum http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 'NAO', 0, ' ', 65, '2016-01-30', 3),
(71, 'Mark', 'mark357177@hotmail.com', 'vqSkFj http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 'NAO', 0, ' ', 9, '2016-02-01', 2),
(72, 'Mark', 'mark357177@hotmail.com', 'RfxYPk http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 'NAO', 0, ' ', 57, '2016-02-01', 2),
(73, 'Mark', 'mark357177@hotmail.com', 'Tszvdp http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 'NAO', 0, ' ', 31, '2016-02-01', 5),
(74, 'Mark', 'mark357177@hotmail.com', 'OLsz2t http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 'NAO', 0, ' ', 31, '2016-02-02', 3),
(75, 'chaba', 'barny182@hotmail.com', '3lhWgq http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 'NAO', 0, ' ', 67, '2016-02-02', 2),
(76, 'Mark', 'mark357177@hotmail.com', 'Ip7O80 http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 'NAO', 0, ' ', 67, '2016-02-03', 5),
(77, 'Mark', 'mark357177@hotmail.com', 'zXs0Z1 http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 'NAO', 0, ' ', 30, '2016-02-03', 2),
(78, 'gordon', 'barny182@hotmail.com', 'tdVFhf http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 'NAO', 0, ' ', 18, '2016-02-07', 5),
(79, 'Mark', 'mark357177@hotmail.com', 'xNGsDV http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 'NAO', 0, ' ', 73, '2016-02-08', 2),
(80, 'gordon', 'barny182@hotmail.com', 'xNecAm http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 'NAO', 0, ' ', 72, '2016-02-09', 4),
(81, 'matt', 'derby451@hotmail.com', 'vj8vPn http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 'NAO', 0, ' ', 29, '2016-02-10', 4),
(82, 'matt', 'derby451@hotmail.com', 'k5bCVl http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 'NAO', 0, ' ', 68, '2016-02-10', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners`
--

CREATE TABLE `tb_banners` (
  `idbanner` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`) VALUES
(31, 'Banner 1', '2301201612211369810870.jpg', 'SIM', 1, '1', 'banner-1', ''),
(32, 'Banner 2', '2001201603012211523970.jpg', 'SIM', 3, '1', 'banner-2', ''),
(33, 'Banner 3', '1901201607176251566745.jpg', 'SIM', 4, '1', 'banner-3', ''),
(34, 'Promoção de Iluminação', '0211201505305992622801.jpg', 'NAO', NULL, '2', 'promocao-de-iluminacao', 'http://www.globo.com/'),
(35, 'Banner 4', '2001201603052885419796.jpg', 'SIM', 5, '1', 'banner-4', ''),
(36, 'Banner 5', '2301201601308551650870.jpg', 'SIM', 2, '1', 'banner-5', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_produtos`
--

CREATE TABLE `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desCategoria_Dica_Modelcricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `desCategoria_Dica_Modelcricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(45, 'PISCINAS EM VINIL', NULL, '0311201512516120088501.png', 'SIM', 2, 'piscinas-em-vinil', NULL, NULL, NULL),
(46, 'AQUECEDOR PARA PISCINAS', NULL, '0311201512515426771847.png', 'SIM', 3, 'aquecedor-para-piscinas', NULL, NULL, NULL),
(47, 'SAUNA E VAPOR', NULL, '0311201501014606375516.png', 'SIM', 4, 'sauna-e-vapor', NULL, NULL, NULL),
(48, 'FILTROS E BOMBAS', NULL, '0311201512523630530242.png', 'SIM', 6, 'filtros-e-bombas', NULL, NULL, NULL),
(49, 'ILUMINAÇÃO', NULL, '1901201610443565609339.png', 'SIM', 8, 'iluminacao', NULL, NULL, NULL),
(50, 'PRODUTOS QUÍMICOS', NULL, '1901201610499791999533.png', 'SIM', 7, 'produtos-quimicos', NULL, NULL, NULL),
(51, 'ACESSÓRIOS', NULL, '1901201610574782003499.png', 'SIM', 5, 'acessorios', NULL, NULL, NULL),
(52, 'CASCATAS INOX', NULL, '1901201611018566858325.png', 'SIM', 9, 'cascatas-inox', NULL, NULL, NULL),
(56, 'PISCINAS', NULL, '1801201604211166166555.png', 'SIM', 1, 'piscinas', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_dicas`
--

CREATE TABLE `tb_comentarios_dicas` (
  `idcomentariodica` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_dica` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_comentarios_dicas`
--

INSERT INTO `tb_comentarios_dicas` (`idcomentariodica`, `nome`, `email`, `comentario`, `ativo`, `ordem`, `url_amigavel`, `id_dica`, `data`) VALUES
(1, 'umevfmuk', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(2, 'mscpnild', 'sample@email.tst', '1', 'NAO', 0, ' ', 36, '2016-01-20'),
(3, 'oydcwsva', 'sample@email.tst', '1', 'NAO', 0, ' ', 36, '2016-01-20'),
(4, 'clhmlalo', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(5, 'ciomlqiy', 'sample@email.tst', '1', 'res', 0, ' ', 37, '2016-01-20'),
(6, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(7, 'ciomlqiy', 'sample@email.tst', '1', '\'+r', 0, ' ', 37, '2016-01-20'),
(8, 'sbrgtghd', 'sample@email.tst', '1', 'set', 0, ' ', 37, '2016-01-20'),
(9, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(10, 'iutbpkrx', 'sample@email.tst', '1', '\r\n ', 0, ' ', 37, '2016-01-20'),
(11, 'ciomlqiy', 'sample@email.tst', '1', '"+r', 0, ' ', 37, '2016-01-20'),
(12, 'sbrgtghd', 'sample@email.tst', '1', '\'se', 0, ' ', 37, '2016-01-20'),
(13, 'sthxxcfw', 'sample@email.tst', '1', '-1 ', 0, ' ', 37, '2016-01-20'),
(14, 'iutbpkrx', 'sample@email.tst', '1', '\n S', 0, ' ', 37, '2016-01-20'),
(15, 'ciomlqiy', 'sample@email.tst', 'response.write(9989667*9758513)', 'NAO', 0, ' ', 37, '2016-01-20'),
(16, 'sbrgtghd', 'sample@email.tst', '1', '"se', 0, ' ', 37, '2016-01-20'),
(17, 'sthxxcfw', 'sample@email.tst', '1', '-1 ', 0, ' ', 37, '2016-01-20'),
(18, 'iutbpkrx', 'sample@email.tst', '1', '\r S', 0, ' ', 37, '2016-01-20'),
(19, 'sbrgtghd', 'sample@email.tst', '1', '\nse', 0, ' ', 37, '2016-01-20'),
(20, 'ciomlqiy', 'sample@email.tst', '\'+response.write(9989667*9758513)+\' ', 'NAO', 0, ' ', 37, '2016-01-20'),
(21, 'uoqthvma', 'sample@email.tst', '1', '..%', 0, ' ', 37, '2016-01-20'),
(22, 'sthxxcfw', 'sample@email.tst', '1', '-1 ', 0, ' ', 37, '2016-01-20'),
(23, 'iutbpkrx', 'sample@email.tst', '\r\n SomeCustomInjectedHeader:injected_by_wvs', 'NAO', 0, ' ', 37, '2016-01-20'),
(24, 'sbrgtghd', 'sample@email.tst', '1', '`se', 0, ' ', 37, '2016-01-20'),
(25, 'ciomlqiy', 'sample@email.tst', '"+response.write(9989667*9758513)+"', 'NAO', 0, ' ', 37, '2016-01-20'),
(26, 'sthxxcfw', 'sample@email.tst', '1', '-1\'', 0, ' ', 37, '2016-01-20'),
(27, 'iutbpkrx', 'sample@email.tst', '\n SomeCustomInjectedHeader:injected_by_wvs', 'NAO', 0, ' ', 37, '2016-01-20'),
(28, 'sbrgtghd', 'sample@email.tst', '1', ';se', 0, ' ', 37, '2016-01-20'),
(29, 'ciomlqiy', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(30, 'sthxxcfw', 'sample@email.tst', '1', '-1"', 0, ' ', 37, '2016-01-20'),
(31, 'iutbpkrx', 'sample@email.tst', '\r SomeCustomInjectedHeader:injected_by_wvs', 'NAO', 0, ' ', 37, '2016-01-20'),
(32, 'sbrgtghd', 'sample@email.tst', 'set|set&set', 'NAO', 0, ' ', 37, '2016-01-20'),
(33, 'ciomlqiy', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(34, 'sthxxcfw', 'sample@email.tst', '1', 'if(', 0, ' ', 37, '2016-01-20'),
(35, 'iutbpkrx', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(36, 'hjundarq', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(37, 'sbrgtghd', 'sample@email.tst', '\'set|set&set\' ', 'NAO', 0, ' ', 37, '2016-01-20'),
(38, 'ciomlqiy', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(39, 'uoqthvma', 'sample@email.tst', '1', 'Li4', 0, ' ', 37, '2016-01-20'),
(40, 'sthxxcfw', 'sample@email.tst', '1', '(se', 0, ' ', 37, '2016-01-20'),
(41, 'iutbpkrx', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(42, 'hjundarq', 'sample@email.tst', '1', ' ', 0, ' ', 37, '2016-01-20'),
(43, 'uoqthvma', 'sample@email.tst', '1', '..%', 0, ' ', 37, '2016-01-20'),
(44, 'sthxxcfw', 'sample@email.tst', '1', 'pmt', 0, ' ', 37, '2016-01-20'),
(45, 'iutbpkrx', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(46, 'ciomlqiy', 'response.write(9102502*9340041)', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(47, 'hjundarq', 'sample@email.tst', '1', '123', 0, ' ', 37, '2016-01-20'),
(48, 'sthxxcfw', 'sample@email.tst', '1', 'nI9', 0, ' ', 37, '2016-01-20'),
(49, 'iutbpkrx', '\r\n SomeCustomInjectedHeader:injected_by_wvs', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(50, 'ciomlqiy', '\'+response.write(9102502*9340041)+\' ', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(51, 'udcfhvoh', 'sample@email.tst', '1', '${9', 0, ' ', 37, '2016-01-20'),
(52, 'hjundarq', 'sample@email.tst', '1', ' ', 0, ' ', 37, '2016-01-20'),
(53, 'uoqthvma', 'sample@email.tst', '1', '..?', 0, ' ', 37, '2016-01-20'),
(54, 'sthxxcfw', 'sample@email.tst', '1', 'tqC', 0, ' ', 37, '2016-01-20'),
(55, 'iutbpkrx', '\n SomeCustomInjectedHeader:injected_by_wvs', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(56, 'ciomlqiy', '"+response.write(9102502*9340041)+"', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(57, 'udcfhvoh', 'sample@email.tst', '${99192+100215}', 'NAO', 0, ' ', 37, '2016-01-20'),
(58, 'sthxxcfw', 'sample@email.tst', '1', 'w40', 0, ' ', 37, '2016-01-20'),
(59, 'hjundarq', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(60, 'sbrgtghd', 'sample@email.tst', '"set|set&set"', 'NAO', 0, ' ', 37, '2016-01-20'),
(61, 'iutbpkrx', '\r SomeCustomInjectedHeader:injected_by_wvs', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(62, 'ciomlqiy', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(63, 'udcfhvoh', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(64, 'sthxxcfw', 'sample@email.tst', '1', 'tRf', 0, ' ', 37, '2016-01-20'),
(65, 'sbrgtghd', 'sample@email.tst', '\nset|set&set\n', 'NAO', 0, ' ', 37, '2016-01-20'),
(66, 'hjundarq', 'sample@email.tst', ' ', 'NAO', 0, ' ', 37, '2016-01-20'),
(67, 'ciomlqiy', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(68, 'iutbpkrx', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(69, 'udcfhvoh', '${99801+99668}', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(70, 'sthxxcfw', 'sample@email.tst', '1', '1Oc', 0, ' ', 37, '2016-01-20'),
(71, 'sbrgtghd', 'sample@email.tst', '`set|set&set`', 'NAO', 0, ' ', 37, '2016-01-20'),
(72, 'hjundarq', 'sample@email.tst', '12345\'"\\\'\\");|]*{\r\n<\0>?\'\'?', 'NAO', 0, ' ', 37, '2016-01-20'),
(73, 'ciomlqiy', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(74, 'iutbpkrx', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(75, 'udcfhvoh', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(76, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(77, 'sbrgtghd', 'sample@email.tst', ';set|set&set;', 'NAO', 0, ' ', 37, '2016-01-20'),
(78, 'uoqthvma', 'sample@email.tst', '1', 'WEB', 0, ' ', 37, '2016-01-20'),
(79, 'response.write(9744775*9950444)', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(80, 'iutbpkrx', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(81, 'hjundarq', 'sample@email.tst', ' ', 'NAO', 0, ' ', 37, '2016-01-20'),
(82, '${99925+99465}', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(83, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(84, 'sbrgtghd', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(85, '\'+response.write(9744775*9950444)+\' ', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(86, '\r\n SomeCustomInjectedHeader:injected_by_wvs', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(87, 'xkrqbdav', 'sample@email.tst', '1', 'htt', 0, ' ', 37, '2016-01-20'),
(88, 'sthxxcfw', 'sample@email.tst', '-1 OR 2+783-783-1=0+0+0+1 --', 'NAO', 0, ' ', 37, '2016-01-20'),
(89, 'hjundarq', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(90, 'irgppudm', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(91, 'uoqthvma', 'sample@email.tst', '1', 'WEB', 0, ' ', 37, '2016-01-20'),
(92, 'sbrgtghd', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(93, '"+response.write(9744775*9950444)+"', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(94, '\n SomeCustomInjectedHeader:injected_by_wvs', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(95, 'xkrqbdav', 'sample@email.tst', '1', '1so', 0, ' ', 37, '2016-01-20'),
(96, 'sthxxcfw', 'sample@email.tst', '-1 OR 2+703-703-1=0+0+0+1', 'NAO', 0, ' ', 37, '2016-01-20'),
(97, 'hjundarq', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(98, 'T05XT2xsZ2M=', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(99, 'uoqthvma', 'sample@email.tst', '1', '../', 0, ' ', 37, '2016-01-20'),
(100, 'sbrgtghd', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(101, 'eoffowbi', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(102, '\r SomeCustomInjectedHeader:injected_by_wvs', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(103, 'xkrqbdav', 'sample@email.tst', '1', 'Htt', 0, ' ', 37, '2016-01-20'),
(104, 'sthxxcfw', 'sample@email.tst', '-1 OR 3+703-703-1=0+0+0+1', 'NAO', 0, ' ', 37, '2016-01-20'),
(105, 'sbrgtghd', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(106, 'hjundarq', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(107, 'tiunmhik', 'sample@email.tst', '1', ')', 0, ' ', 37, '2016-01-20'),
(108, 'fwtrpoye', 'sample@email.tst', '1&n949448=v979699', 'NAO', 0, ' ', 37, '2016-01-20'),
(109, 'tgcshgfr', 'sample@email.tst', '1', ' ', 0, ' ', 37, '2016-01-20'),
(110, 'xkrqbdav', 'sample@email.tst', '1', 'htt', 0, ' ', 37, '2016-01-20'),
(111, 'sthxxcfw', 'sample@email.tst', '-1 OR 3*2<(0+5+703-703)', 'NAO', 0, ' ', 37, '2016-01-20'),
(112, 'sbrgtghd', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(113, 'tiunmhik', 'sample@email.tst', '1', '!((', 0, ' ', 37, '2016-01-20'),
(114, 'bgnuogsf', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(115, 'tgcshgfr', 'sample@email.tst', '1', '\'"(', 0, ' ', 37, '2016-01-20'),
(116, 'hjundarq', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(117, 'xkrqbdav', 'sample@email.tst', '1', 'tes', 0, ' ', 37, '2016-01-20'),
(118, 'uoqthvma', 'sample@email.tst', '1', '../', 0, ' ', 37, '2016-01-20'),
(119, 'sthxxcfw', 'sample@email.tst', '-1 OR 3*2>(0+5+703-703)', 'NAO', 0, ' ', 37, '2016-01-20'),
(120, 'sbrgtghd', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(121, 'tiunmhik', 'sample@email.tst', '1', '^(#', 0, ' ', 37, '2016-01-20'),
(122, 'bbadcutf', 'sample@email.tst&n938439=v906274', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(123, 'tgcshgfr', 'sample@email.tst', ' ', 'NAO', 0, ' ', 37, '2016-01-20'),
(124, 'xkrqbdav', 'sample@email.tst', 'http://some-inexistent-website.acu/some_inexistent_file_with_long_name?.jpg', 'NAO', 0, ' ', 37, '2016-01-20'),
(125, 'uoqthvma', 'sample@email.tst', '1', '???', 0, ' ', 37, '2016-01-20'),
(126, 'hjundarq', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(127, 'sthxxcfw', 'sample@email.tst', '-1\' OR 2+564-564-1=0+0+0+1 --', 'NAO', 0, ' ', 37, '2016-01-20'),
(128, 'sbrgtghd', 'set|set&set', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(129, 'tiunmhik', 'sample@email.tst', ')', 'NAO', 0, ' ', 37, '2016-01-20'),
(130, 'dllebvlg', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(131, 'tgcshgfr', 'sample@email.tst', '\'"()', 'NAO', 0, ' ', 37, '2016-01-20'),
(132, 'xkrqbdav', 'sample@email.tst', '1some_inexistent_file_with_long_name\0.jpg', 'NAO', 0, ' ', 37, '2016-01-20'),
(133, 'uoqthvma', 'sample@email.tst', '1', '...', 0, ' ', 37, '2016-01-20'),
(134, 'sbrgtghd', '\'set|set&set\' ', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(135, 'sthxxcfw', 'sample@email.tst', '-1\' OR 3+564-564-1=0+0+0+1 --', 'NAO', 0, ' ', 37, '2016-01-20'),
(136, 'hjundarq', ' ', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(137, 'tiunmhik', 'sample@email.tst', '!(()&&!|*|*|', 'NAO', 0, ' ', 37, '2016-01-20'),
(138, 'tgcshgfr', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(139, 'xkrqbdav', 'sample@email.tst', 'Http://testasp.vulnweb.com/t/fit.txt', 'NAO', 0, ' ', 37, '2016-01-20'),
(140, 'uoqthvma', 'sample@email.tst', '1', '..\\', 0, ' ', 37, '2016-01-20'),
(141, 'sbrgtghd', '"set|set&set"', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(142, 'sthxxcfw', 'sample@email.tst', '-1" OR 2+457-457-1=0+0+0+1 --', 'NAO', 0, ' ', 37, '2016-01-20'),
(143, 'hjundarq', '12345\'"\\\'\\");|]*{\r\n<\0>?\'\'?', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(144, 'tiunmhik', 'sample@email.tst', '^(#$!@#$)(()))******', 'NAO', 0, ' ', 37, '2016-01-20'),
(145, 'tgcshgfr', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(146, 'xkrqbdav', 'sample@email.tst', 'http://testasp.vulnweb.com/t/fit.txt?.jpg', 'NAO', 0, ' ', 37, '2016-01-20'),
(147, 'sbrgtghd', '\nset|set&set\n', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(148, 'uoqthvma', 'sample@email.tst', '1', '/.\\', 0, ' ', 37, '2016-01-20'),
(149, 'sthxxcfw', 'sample@email.tst', '-1" OR 3+457-457-1=0+0+0+1 --', 'NAO', 0, ' ', 37, '2016-01-20'),
(150, 'hjundarq', ' ', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(151, 'tiunmhik', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(152, 'xkrqbdav', 'sample@email.tst', 'testasp.vulnweb.com', 'NAO', 0, ' ', 37, '2016-01-20'),
(153, 'tgcshgfr', ' ', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(154, 'uoqthvma', 'sample@email.tst', '1', '../', 0, ' ', 37, '2016-01-20'),
(155, 'sbrgtghd', '`set|set&set`', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(156, 'sthxxcfw', 'sample@email.tst', '-1" OR 3*2<(0+5+457-457) --', 'NAO', 0, ' ', 37, '2016-01-20'),
(157, 'hjundarq', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(158, 'tiunmhik', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(159, 'xkrqbdav', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(160, 'efoyvuxd&n945817=v960730', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(161, 'tgcshgfr', '\'"()', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(162, 'sbrgtghd', ';set|set&set;', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(163, 'uoqthvma', 'sample@email.tst', '1', '../', 0, ' ', 37, '2016-01-20'),
(164, 'sthxxcfw', 'sample@email.tst', 'if(now()=sysdate(),sleep(9),0)/*\'XOR(if(now()=sysdate(),sleep(9),0))OR\'"XOR(if(now()=sysdate(),sleep(9),0))OR"*/', 'NAO', 0, ' ', 37, '2016-01-20'),
(165, 'tiunmhik', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(166, 'hjundarq', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(167, 'xkrqbdav', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(168, 'umhajmyj', 'sample@email.tst', '1', ';pr', 0, ' ', 37, '2016-01-20'),
(169, 'tgcshgfr', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(170, 'sbrgtghd', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(171, 'uoqthvma', 'sample@email.tst', '1', 'une', 0, ' ', 37, '2016-01-20'),
(172, 'sthxxcfw', 'sample@email.tst', '(select(0)from(select(sleep(3)))v)/*\'+(select(0)from(select(sleep(3)))v)+\'"+(select(0)from(select(sleep(3)))v)+"*/', 'NAO', 0, ' ', 37, '2016-01-20'),
(173, 'tiunmhik', ')', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(174, 'umhajmyj', 'sample@email.tst', '1', '\';p', 0, ' ', 37, '2016-01-20'),
(175, 'xkrqbdav', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(176, 'hjundarq', 'sample@email.tst', '1', 'NAO', 0, ' ', 12345, '2016-01-20'),
(177, 'sbrgtghd', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(178, 'uoqthvma', 'sample@email.tst', '1', 'WEB', 0, ' ', 37, '2016-01-20'),
(179, 'sthxxcfw', 'sample@email.tst', '-1; waitfor delay \'0:0:3\' --', 'NAO', 0, ' ', 37, '2016-01-20'),
(180, 'tiunmhik', '!(()&&!|*|*|', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(181, 'umhajmyj', 'sample@email.tst', '1', '";p', 0, ' ', 37, '2016-01-20'),
(182, 'xkrqbdav', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(183, 'sbrgtghd', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(184, 'tiunmhik', '^(#$!@#$)(()))******', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(185, 'uoqthvma', 'sample@email.tst', '1', 'WEB', 0, ' ', 37, '2016-01-20'),
(186, 'sthxxcfw', 'sample@email.tst', '-1); waitfor delay \'0:0:3\' --', 'NAO', 0, ' ', 37, '2016-01-20'),
(187, 'umhajmyj', 'sample@email.tst', '1', '${@', 0, ' ', 37, '2016-01-20'),
(188, 'xkrqbdav', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(189, 'hjundarq', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(190, 'sbrgtghd', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(191, 'tiunmhik', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(192, 'tgcshgfr', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(193, 'sthxxcfw', 'sample@email.tst', '-1)); waitfor delay \'0:0:3\' --', 'NAO', 0, ' ', 37, '2016-01-20'),
(194, 'uoqthvma', 'sample@email.tst', '..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2Fetc%2Fpasswd%00.jpg', 'NAO', 0, ' ', 37, '2016-01-20'),
(195, 'umhajmyj', 'sample@email.tst', '1', '${@', 0, ' ', 37, '2016-01-20'),
(196, 'xkrqbdav', 'http://some-inexistent-website.acu/some_inexistent_file_with_long_name?.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(197, 'sbrgtghd', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(198, 'tiunmhik', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(199, 'hjundarq', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(200, ' ', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(201, 'sthxxcfw', 'sample@email.tst', 'MikEmt0C\'; waitfor delay \'0:0:6\' --', 'NAO', 0, ' ', 37, '2016-01-20'),
(202, 'umhajmyj', 'sample@email.tst', ';print(md5(acunetix_wvs_security_test));', 'NAO', 0, ' ', 37, '2016-01-20'),
(203, 'xkrqbdav', '1some_inexistent_file_with_long_name\0.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(204, 'sbrgtghd', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(205, '\'"()', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(206, 'tiunmhik', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(207, 'uoqthvma', 'sample@email.tst', 'Li4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vZXRjL3Bhc3N3ZAAucG5n', 'NAO', 0, ' ', 37, '2016-01-20'),
(208, ' ', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(209, 'sthxxcfw', 'sample@email.tst', '5cjeycLU\'); waitfor delay \'0:0:6\' --', 'NAO', 0, ' ', 37, '2016-01-20'),
(210, 'xkrqbdav', 'Http://testasp.vulnweb.com/t/fit.txt', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(211, ')', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(212, 'bdxpjpyk', 'sample@email.tst', '1', 'htt', 0, ' ', 37, '2016-01-20'),
(213, 'uoqthvma', 'sample@email.tst', '..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2Fetc%2Fpasswd%00.jpg', 'NAO', 0, ' ', 37, '2016-01-20'),
(214, 'sthxxcfw', 'sample@email.tst', 'SvPpy0Hu\')); waitfor delay \'0:0:6\' --', 'NAO', 0, ' ', 37, '2016-01-20'),
(215, '12345\'"\\\'\\");|]*{\r\n<\0>?\'\'?', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(216, 'xkrqbdav', 'http://testasp.vulnweb.com/t/fit.txt?.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(217, '!(()&&!|*|*|', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(218, 'bdxpjpyk', 'sample@email.tst', 'http://testasp.vulnweb.com/t/xss.html?%00.jpg', 'NAO', 0, ' ', 37, '2016-01-20'),
(219, 'sthxxcfw', 'sample@email.tst', '-1;select pg_sleep(6); --', 'NAO', 0, ' ', 37, '2016-01-20'),
(220, ' ', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(221, 'xkrqbdav', 'testasp.vulnweb.com', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(222, 'bdxpjpyk', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(223, '^(#$!@#$)(()))******', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(224, 'sthxxcfw', 'sample@email.tst', '-1);select pg_sleep(6); --', 'NAO', 0, ' ', 37, '2016-01-20'),
(225, 'uoqthvma', 'sample@email.tst', '..??..??..??..??..??..??..??..??etc/passwd', 'NAO', 0, ' ', 37, '2016-01-20'),
(226, 'xkrqbdav', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(227, 'set|set&set', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(228, 'umhajmyj', 'sample@email.tst', '\';print(md5(acunetix_wvs_security_test));$a=\' ', 'NAO', 0, ' ', 37, '2016-01-20'),
(229, 'tybpofvo', 'sample@email.tst', '1', 'htt', 0, ' ', 37, '2016-01-20'),
(230, 'euigujkt', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(231, 'bdxpjpyk', 'http://testasp.vulnweb.com/t/xss.html?%00.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(232, 'sthxxcfw', 'sample@email.tst', '-1));select pg_sleep(9); --', 'NAO', 0, ' ', 37, '2016-01-20'),
(233, 'xkrqbdav', 'sample@email.tst', '1', 'NAO', 0, ' ', 1, '2016-01-20'),
(234, '\'set|set&set\' ', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(235, 'umhajmyj', 'sample@email.tst', '";print(md5(acunetix_wvs_security_test));$a="', 'NAO', 0, ' ', 37, '2016-01-20'),
(236, 'bdxpjpyk', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(237, 'sthxxcfw', 'sample@email.tst', 'jg9DRsN7\';select pg_sleep(9); --', 'NAO', 0, ' ', 37, '2016-01-20'),
(238, 'euigujkt', 'sample@email.tst', '1', '5-d', 0, ' ', 37, '2016-01-20'),
(239, 'xkrqbdav', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(240, 'tybpofvo', 'sample@email.tst', 'http://hit0oBNKobHve.bxss.me/', 'NAO', 0, ' ', 37, '2016-01-20'),
(241, '"set|set&set"', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(242, 'umhajmyj', 'sample@email.tst', '${@print(md5(acunetix_wvs_security_test))}', 'NAO', 0, ' ', 37, '2016-01-20'),
(243, 'http://testasp.vulnweb.com/t/xss.html?%00.jpg', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(244, 'sthxxcfw', 'sample@email.tst', 'hMtPvt4n\');select pg_sleep(9); --', 'NAO', 0, ' ', 37, '2016-01-20'),
(245, 'euigujkt', 'sample@email.tst', '1', '5-d', 0, ' ', 37, '2016-01-20'),
(246, 'xkrqbdav', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(247, 'uoqthvma', 'sample@email.tst', 'WEB-INF/web.xml', 'NAO', 0, ' ', 37, '2016-01-20'),
(248, '\nset|set&set\n', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(249, 'umhajmyj', 'sample@email.tst', '${@print(md5(acunetix_wvs_security_test))}\\', 'NAO', 0, ' ', 37, '2016-01-20'),
(250, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(251, 'sthxxcfw', 'sample@email.tst', 'uuJcZgPb\'));select pg_sleep(3); --', 'NAO', 0, ' ', 37, '2016-01-20'),
(252, 'euigujkt', 'sample@email.tst', '1', '5-d', 0, ' ', 37, '2016-01-20'),
(253, 'uoqthvma', 'sample@email.tst', 'WEB-INF\\web.xml', 'NAO', 0, ' ', 37, '2016-01-20'),
(254, 'xkrqbdav', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(255, '`set|set&set`', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(256, 'umhajmyj', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(257, 'jfiiuixu', 'sample@email.tst', '1', '1\'"', 0, ' ', 37, '2016-01-20'),
(258, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(259, 'euigujkt', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(260, 'uoqthvma', 'sample@email.tst', '../../../../../../../../../../windows/win.ini', 'NAO', 0, ' ', 37, '2016-01-20'),
(261, ';set|set&set;', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(262, 'http://some-inexistent-website.acu/some_inexistent_file_with_long_name?.jpg', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(263, 'umhajmyj', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(264, 'jfiiuixu', 'sample@email.tst', '1', '\\', 0, ' ', 37, '2016-01-20'),
(265, 'euigujkt', 'sample@email.tst', '5-dicas-para-construcao-de-sua-piscina', 'NAO', 0, ' ', 37, '2016-01-20'),
(266, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(267, 'kooayxeq', 'sample@email.tst', '1', ')))', 0, ' ', 37, '2016-01-20'),
(268, 'uoqthvma', 'sample@email.tst', '../../../../../../../../../../windows/win.ini\0.jpg', 'NAO', 0, ' ', 37, '2016-01-20'),
(269, '1some_inexistent_file_with_long_name\0.jpg', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(270, 'umhajmyj', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(271, 'jfiiuixu', 'sample@email.tst', '1', '1?\0', 0, ' ', 37, '2016-01-20'),
(272, 'euigujkt', 'sample@email.tst', '5-dicas-para-construcao-de-sua-piscina\0', 'NAO', 0, ' ', 37, '2016-01-20'),
(273, 'droimsig', 'sample@email.tst', '1', 'www', 0, ' ', 37, '2016-01-20'),
(274, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(275, 'kooayxeq', 'sample@email.tst', ')))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))', 'NAO', 0, ' ', 37, '2016-01-20'),
(276, 'Http://testasp.vulnweb.com/t/fit.txt', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(277, 'uoqthvma', 'sample@email.tst', '????????????????????????????????????????????????windows??win.ini', 'NAO', 0, ' ', 37, '2016-01-20'),
(278, 'jfiiuixu', 'sample@email.tst', '1', '@@i', 0, ' ', 37, '2016-01-20'),
(279, 'umhajmyj', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(280, 'euigujkt', 'sample@email.tst', '5-dicas-para-construcao-de-sua-piscina/.', 'NAO', 0, ' ', 37, '2016-01-20'),
(281, 'tybpofvo', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(282, 'droimsig', 'sample@email.tst', 'www.acunetix.com', 'NAO', 0, ' ', 37, '2016-01-20'),
(283, 'kooayxeq', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(284, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(285, 'http://testasp.vulnweb.com/t/fit.txt?.jpg', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(286, 'uoqthvma', 'sample@email.tst', '................windowswin.ini', 'NAO', 0, ' ', 37, '2016-01-20'),
(287, 'jfiiuixu', 'sample@email.tst', '1', 'JyI', 0, ' ', 37, '2016-01-20'),
(288, 'umhajmyj', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(289, 'euigujkt', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(290, 'droimsig', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(291, 'kooayxeq', ')))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(292, 'testasp.vulnweb.com', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(293, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(294, 'tybpofvo', 'http://hit0lZd0L0PTb.bxss.me/', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(295, 'uoqthvma', 'sample@email.tst', '..\\..\\..\\..\\..\\..\\..\\..\\windows\\win.ini', 'NAO', 0, ' ', 37, '2016-01-20'),
(296, 'jfiiuixu', 'sample@email.tst', '1', '?\'?', 0, ' ', 37, '2016-01-20'),
(297, 'droimsig', 'www.acunetix.com', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(298, 'umhajmyj', ';print(md5(acunetix_wvs_security_test));', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(299, 'euigujkt', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(300, 'nwyrtydd', 'sample@email.tst', '1', '\'"', 0, ' ', 37, '2016-01-20'),
(301, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(302, 'jfiiuixu', 'sample@email.tst', '1', '?\'\'', 0, ' ', 37, '2016-01-20'),
(303, 'droimsig', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(304, 'umhajmyj', '\';print(md5(acunetix_wvs_security_test));$a=\' ', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(305, 'euigujkt', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(306, 'kooayxeq', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(307, 'uoqthvma', 'sample@email.tst', '/.\\\\./.\\\\./.\\\\./.\\\\./.\\\\./.\\\\./windows/win.ini', 'NAO', 0, ' ', 37, '2016-01-20'),
(308, 'tybpofvo', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(309, 'nwyrtydd', 'sample@email.tst', '1', '<!-', 0, ' ', 37, '2016-01-20'),
(310, 'jfiiuixu', 'sample@email.tst', '1', '(se', 0, ' ', 37, '2016-01-20'),
(311, 'www.acunetix.com', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(312, 'rpqtysrh', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(313, 'umhajmyj', '";print(md5(acunetix_wvs_security_test));$a="', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(314, 'euigujkt', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(315, ')))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(316, 'uoqthvma', 'sample@email.tst', '../..//../..//../..//../..//../..//../..//../..//../..//windows/win.ini', 'NAO', 0, ' ', 37, '2016-01-20'),
(317, 'nwyrtydd', 'sample@email.tst', '\'"', 'NAO', 0, ' ', 37, '2016-01-20'),
(318, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(319, 'rpqtysrh', 'sample@email.tst', '1', '\'"(', 0, ' ', 37, '2016-01-20'),
(320, 'euigujkt', 'sample%40email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(321, 'http://hitAGoOEmS9eV.bxss.me/', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(322, 'umhajmyj', '${@print(md5(acunetix_wvs_security_test))}', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(323, 'uoqthvma', 'sample@email.tst', '../.../.././../.../.././../.../.././../.../.././../.../.././../.../.././windows/win.ini', 'NAO', 0, ' ', 37, '2016-01-20'),
(324, 'nwyrtydd', 'sample@email.tst', '<!--', 'NAO', 0, ' ', 37, '2016-01-20'),
(325, 'jfiiuixu', 'sample@email.tst', '1\'"', 'NAO', 0, ' ', 37, '2016-01-20'),
(326, 'euigujkt', '5-dicas-para-construcao-de-sua-piscina', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(327, 'umhajmyj', '${@print(md5(acunetix_wvs_security_test))}\\', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(328, 'uoqthvma', 'sample@email.tst', 'unexisting/../../../../../../../../../../windows/win.ini.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\', 'NAO', 0, ' ', 37, '2016-01-20'),
(329, 'nwyrtydd', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(330, 'jfiiuixu', 'sample@email.tst', '\\', 'NAO', 0, ' ', 37, '2016-01-20'),
(331, 'rpqtysrh', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(332, 'euigujkt', '5-dicas-para-construcao-de-sua-piscina\0', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(333, 'umhajmyj', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(334, 'uoqthvma', 'sample@email.tst', 'WEB-INF/web.xml?', 'NAO', 0, ' ', 37, '2016-01-20'),
(335, 'nwyrtydd', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(336, 'jfiiuixu', 'sample@email.tst', '1?\0xa7??', 'NAO', 0, ' ', 37, '2016-01-20'),
(337, 'tqbojgwp', 'sample@email.tst', '1\'"()&%<ScRiPt >prompt(940998)</ScRiPt>', 'NAO', 0, ' ', 37, '2016-01-20'),
(338, 'euigujkt', '5-dicas-para-construcao-de-sua-piscina/.', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(339, 'umhajmyj', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(340, 'uoqthvma', 'sample@email.tst', 'WEB-INF\\web.xml?', 'NAO', 0, ' ', 37, '2016-01-20'),
(341, 'nwyrtydd', '\'"', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(342, 'jfiiuixu', 'sample@email.tst', '@@vWjo5', 'NAO', 0, ' ', 37, '2016-01-20'),
(343, 'tqbojgwp', 'sample@email.tst', '\'"()&%<ScRiPt >prompt(939414)</ScRiPt>', 'NAO', 0, ' ', 37, '2016-01-20'),
(344, 'euigujkt', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(345, 'umhajmyj', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(346, 'nwyrtydd', '<!--', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(347, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(348, 'tqbojgwp', 'sample@email.tst', '1_958503', 'NAO', 0, ' ', 37, '2016-01-20'),
(349, 'jfiiuixu', 'sample@email.tst', 'JyI=', 'NAO', 0, ' ', 37, '2016-01-20'),
(350, 'umhajmyj', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(351, 'euigujkt', 'sample@email.tst', '1', 'NAO', 0, ' ', 5, '2016-01-20'),
(352, 'nwyrtydd', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(353, 'jhowxmpa', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(354, 'jfiiuixu', 'sample@email.tst', '?\'?"', 'NAO', 0, ' ', 37, '2016-01-20'),
(355, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(356, 'umhajmyj', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(357, 'euigujkt', 'sample@email.tst', '1', 'NAO', 0, ' ', 5, '2016-01-20'),
(358, 'nwyrtydd', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(359, 'jhowxmpa', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(360, 'jfiiuixu', 'sample@email.tst', '?\'\'?""', 'NAO', 0, ' ', 37, '2016-01-20'),
(361, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(362, ';print(md5(acunetix_wvs_security_test));', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(363, 'euigujkt', 'sample@email.tst', '1', 'NAO', 0, ' ', 5, '2016-01-20'),
(364, '\'"', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(365, 'jhowxmpa', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(366, 'jfiiuixu', 'sample@email.tst', '(select convert(int,CHAR(65)))', 'NAO', 0, ' ', 37, '2016-01-20'),
(367, '\';print(md5(acunetix_wvs_security_test));$a=\' ', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(368, 'euigujkt', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(369, '<!--', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(370, 'kystbscr', 'sample%40email.tst\'"()&%<ScRiPt >prompt(968178)</ScRiPt>', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(371, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(372, '5-dicas-para-construcao-de-sua-piscina', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(373, 'kystbscr', '\'"()&%<ScRiPt >prompt(991457)</ScRiPt>', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(374, '";print(md5(acunetix_wvs_security_test));$a="', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(375, '5-dicas-para-construcao-de-sua-piscina\0', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(376, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(377, 'kystbscr', 'sample%40email.tst_989054', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(378, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(379, '${@print(md5(acunetix_wvs_security_test))}', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(380, '5-dicas-para-construcao-de-sua-piscina/.', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(381, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(382, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(383, 'dqidgefd', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(384, '${@print(md5(acunetix_wvs_security_test))}\\', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(385, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(386, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(387, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(388, 'dqidgefd', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(389, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(390, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(391, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(392, 'dqidgefd', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(393, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(394, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(395, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(396, 'dqidgefd\'"()&%<ScRiPt >prompt(976122)</ScRiPt>', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(397, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(398, '\'"()&%<ScRiPt >prompt(956532)</ScRiPt>', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(399, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(400, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(401, 'dqidgefd_941215', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(402, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(403, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(404, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(405, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(406, 'sthxxcfw', 'sample%40email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(407, 'jfiiuixu', 'sample%40email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(408, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(409, 'sthxxcfw', 'sample%40email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(410, 'jfiiuixu', '1\'"', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(411, 'sthxxcfw', 'zyIfcmJ8', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(412, 'jfiiuixu', '\\', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(413, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(414, 'sthxxcfw', '-1 OR 2+422-422-1=0+0+0+1 --', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(415, 'jfiiuixu', '1?\0xa7??', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(416, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(417, 'sthxxcfw', '-1 OR 2+135-135-1=0+0+0+1', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(418, 'jfiiuixu', '@@o0BR9', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(419, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(420, 'sthxxcfw', '-1 OR 3+135-135-1=0+0+0+1', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(421, 'jfiiuixu', 'JyI=', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(422, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(423, 'sthxxcfw', '-1\' OR 2+238-238-1=0+0+0+1 --', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(424, 'jfiiuixu', '?\'?"', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(425, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '0000-00-00'),
(426, 'sthxxcfw', '-1\' OR 3+238-238-1=0+0+0+1 --', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(427, 'jfiiuixu', '?\'\'?""', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(428, 'uoqthvma', '..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2Fetc%2Fpasswd%00.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(429, 'sthxxcfw', '-1\' OR 3*2<(0+5+238-238) --', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(430, 'jfiiuixu', '(select convert(int,CHAR(65)))', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(431, 'uoqthvma', 'Li4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vZXRjL3Bhc3N3ZAAucG5n', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(432, 'sthxxcfw', '-1" OR 2+982-982-1=0+0+0+1 --', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(433, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(434, 'uoqthvma', '..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2Fetc%2Fpasswd%00.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(435, 'sthxxcfw', '-1" OR 3+982-982-1=0+0+0+1 --', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(436, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 1, '2016-01-20'),
(437, 'sthxxcfw', '-1" OR 3*2<(0+5+982-982) --', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(438, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(439, 'sthxxcfw', '-1" OR 3*2>(0+5+982-982) --', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(440, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 1, '2016-01-20'),
(441, 'uoqthvma', '..??..??..??..??..??..??..??..??etc/passwd', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(442, 'sthxxcfw', 'if(now()=sysdate(),sleep(6),0)/*\'XOR(if(now()=sysdate(),sleep(6),0))OR\'"XOR(if(now()=sysdate(),sleep(6),0))OR"*/', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(443, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(444, 'sthxxcfw', '(select(0)from(select(sleep(6)))v)/*\'+(select(0)from(select(sleep(6)))v)+\'"+(select(0)from(select(sleep(6)))v)+"*/', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(445, 'uoqthvma', 'WEB-INF/web.xml', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(446, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(447, 'sthxxcfw', 'Y5CRiJit\'; waitfor delay \'0:0:9\' --', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(448, 'uoqthvma', 'WEB-INF\\web.xml', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(449, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(450, 'sthxxcfw', 'gsSpunGd\'); waitfor delay \'0:0:9\' --', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(451, 'uoqthvma', '../../../../../../../../../../windows/win.ini', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(452, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(453, 'sthxxcfw', 'mxU8Xfz6\')); waitfor delay \'0:0:9\' --', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(454, 'uoqthvma', '../../../../../../../../../../windows/win.ini\0.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(455, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(456, 'sthxxcfw', 'JVlVAyu1\';select pg_sleep(9); --', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(457, 'uoqthvma', '????????????????????????????????????????????????windows??win.ini', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(458, 'jfiiuixu', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(459, 'sthxxcfw', 'wcmCnFIu\');select pg_sleep(9); --', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(460, 'uoqthvma', '................windowswin.ini', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(461, '1\'"', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(462, 'sthxxcfw', 'XKgSyec7\'));select pg_sleep(3); --', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(463, 'uoqthvma', '..\\..\\..\\..\\..\\..\\..\\..\\windows\\win.ini', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(464, '\\', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(465, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(466, 'uoqthvma', '/.\\\\./.\\\\./.\\\\./.\\\\./.\\\\./.\\\\./windows/win.ini', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(467, '1?\0xa7??', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(468, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(469, 'uoqthvma', '../..//../..//../..//../..//../..//../..//../..//../..//windows/win.ini', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(470, '@@2odJ5', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(471, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(472, 'uoqthvma', '../.../.././../.../.././../.../.././../.../.././../.../.././../.../.././windows/win.ini', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(473, 'JyI=', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(474, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(475, 'uoqthvma', 'unexisting/../../../../../../../../../../windows/win.ini.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(476, '?\'?"', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(477, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(478, 'uoqthvma', 'WEB-INF/web.xml?', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(479, '?\'\'?""', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(480, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(481, 'uoqthvma', 'WEB-INF\\web.xml?', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(482, '(select convert(int,CHAR(65)))', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(483, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 47, '2016-01-20'),
(484, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(485, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(486, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(487, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(488, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(489, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(490, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(491, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 47, '2016-01-20'),
(492, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(493, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(494, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(495, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(496, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(497, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', -1, '2016-01-20'),
(498, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(499, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', -1, '2016-01-20'),
(500, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(501, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', -1, '2016-01-20'),
(502, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(503, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(504, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(505, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(506, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(507, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(508, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(509, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', -1, '2016-01-20'),
(510, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(511, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', -1, '2016-01-20');
INSERT INTO `tb_comentarios_dicas` (`idcomentariodica`, `nome`, `email`, `comentario`, `ativo`, `ordem`, `url_amigavel`, `id_dica`, `data`) VALUES
(512, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(513, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', -1, '2016-01-20'),
(514, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(515, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(516, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(517, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(518, 'uoqthvma', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(519, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(520, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(521, '..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2Fetc%2Fpasswd%00.jpg', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(522, 'sthxxcfw', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(523, 'Li4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vZXRjL3Bhc3N3ZAAucG5n', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(524, '..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2F..%2Fetc%2Fpasswd%00.jpg', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(525, 'SY1PEz0l', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(526, '-1 OR 2+184-184-1=0+0+0+1 --', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(527, '..??..??..??..??..??..??..??..??etc/passwd', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(528, '-1 OR 2+698-698-1=0+0+0+1', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(529, '-1 OR 3+698-698-1=0+0+0+1', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(530, 'WEB-INF/web.xml', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(531, '-1 OR 3*2<(0+5+698-698)', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(532, 'WEB-INF\\web.xml', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(533, '-1 OR 3*2>(0+5+698-698)', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(534, '../../../../../../../../../../windows/win.ini', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(535, '-1 OR 2+1-1-1=1 AND 698=698', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(536, '../../../../../../../../../../windows/win.ini\0.jpg', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(537, '-1\' OR 2+975-975-1=0+0+0+1 --', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(538, '????????????????????????????????????????????????windows??win.ini', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(539, '-1\' OR 3+975-975-1=0+0+0+1 --', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(540, '................windowswin.ini', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(541, '-1\' OR 3*2<(0+5+975-975) --', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(542, '..\\..\\..\\..\\..\\..\\..\\..\\windows\\win.ini', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(543, '-1\' OR 3*2>(0+5+975-975) --', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(544, '/.\\\\./.\\\\./.\\\\./.\\\\./.\\\\./.\\\\./windows/win.ini', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(545, '-1" OR 2+872-872-1=0+0+0+1 --', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(546, '../..//../..//../..//../..//../..//../..//../..//../..//windows/win.ini', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(547, '-1" OR 3+872-872-1=0+0+0+1 --', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(548, '../.../.././../.../.././../.../.././../.../.././../.../.././../.../.././windows/win.ini', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(549, '-1" OR 3*2<(0+5+872-872) --', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(550, 'unexisting/../../../../../../../../../../windows/win.ini.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.\\.', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(551, '-1" OR 3*2>(0+5+872-872) --', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(552, 'WEB-INF/web.xml?', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(553, '-1" OR 2+1-1-1=1 AND 872=872 --', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(554, 'WEB-INF\\web.xml?', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(555, 'if(now()=sysdate(),sleep(3),0)/*\'XOR(if(now()=sysdate(),sleep(3),0))OR\'"XOR(if(now()=sysdate(),sleep(3),0))OR"*/', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(556, '(select(0)from(select(sleep(3)))v)/*\'+(select(0)from(select(sleep(3)))v)+\'"+(select(0)from(select(sleep(3)))v)+"*/', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(557, 'ZvXD44G3\'; waitfor delay \'0:0:6\' --', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(558, '9E4KCc9T\'); waitfor delay \'0:0:6\' --', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(559, 'LPRfw912\')); waitfor delay \'0:0:6\' --', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(560, 'PQHjEeIE\';select pg_sleep(9); --', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(561, '648fIU3P\');select pg_sleep(9); --', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20'),
(562, 'YxDlnqBc\'));select pg_sleep(3); --', 'sample@email.tst', '1', 'NAO', 0, ' ', 37, '2016-01-20');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_produtos`
--

CREATE TABLE `tb_comentarios_produtos` (
  `idcomentarioproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_servicos`
--

CREATE TABLE `tb_comentarios_servicos` (
  `idcomentarioservico` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_servico` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_comentarios_servicos`
--

INSERT INTO `tb_comentarios_servicos` (`idcomentarioservico`, `nome`, `email`, `comentario`, `ativo`, `ordem`, `url_amigavel`, `id_servico`, `data`) VALUES
(2, 'Marcio', 'marciomas@gmail.com', 'Aqui fica o comentário Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr.', 'NAO', 0, ' ', 35, '2015-11-04'),
(3, 'claoserr', 'sample@email.tst', '1', 'NAO', 0, ' ', 35, '2016-01-20'),
(4, 'rajhdqeq', 'sample@email.tst', '1', 'NAO', 0, ' ', 36, '2016-01-20'),
(5, 'sqcotqqg', 'sample@email.tst', '1', 'NAO', 0, ' ', 35, '2016-01-20'),
(6, 'brfbkptr', 'sample@email.tst', '1', 'NAO', 0, ' ', 0, '2016-01-20'),
(7, 'Elizabeth', 'elizabeth.pinheiro@globo.com', 'Solicito visita para orçamento da construção de uma piscina. Também modelos para o desenho.', 'NAO', 0, ' ', 35, '2016-02-02');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_configuracoes`
--

CREATE TABLE `tb_configuracoes` (
  `idconfiguracao` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `title_google` longtext NOT NULL,
  `description_google` longtext NOT NULL,
  `keywords_google` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `titulo`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `telefone3`, `telefone4`) VALUES
(1, 'Configurações', 'BSB Piscinas Brasilia - Construção, Reformas, Acessórios e Produtos de Piscinas de Vinil, Azulejo e Pastilhas no DF.', 'Construção e Reformas de Piscinas de Pastilhas, Azulejo e Vinil em Brasília - DF. Acessórios e Produtos de Piscinas de Vinil, Azulejo e Pastilhas no DF', 'Piscinas de vinil, Piscinas de Pastilhas, Piscinas de Azulejo, Acessórios para Piscinas, Manutenção de Piscinas, Construção de Piscinas, Reforma de Piscinas, Produtos para Piscinas.', 'SIM', 0, 'configuracoes', '2° avenida, Lote 275-a, Loja 2, Núcleo Bandeirante, Brasília - DF', '(61) 3552-1121', '(61) 3386-9775', 'bsbpiscinasdf@gmail.com', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3837.7291625819453!2d-47.97135358455928!3d-15.870825829068952!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a2e59437b3569%3A0x3f2e953cec8c1a02!2sBSB+Piscinas!5e0!3m2!1spt-BR!2sbr!4v14458693176', NULL, NULL, 'junior@homewebbrasil.com.br', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_depoimentos`
--

CREATE TABLE `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `depoimento` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_dicas`
--

CREATE TABLE `tb_dicas` (
  `iddica` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(36, 'DICAS PARA CONSTRUÇÃO DE SUA PISCINA DE VINIL', '<p>\r\n	Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS &nbsp;com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2001201602116820875940..jpg', 'SIM', NULL, 'dicas-para-construcao-de-sua-piscina-de-vinil', NULL, NULL, NULL, NULL),
(37, '5 DICAS PARA CONSTRUÇÃO DE SUA PISCINA', '<div>\r\n	1 - Constru&ccedil;&atilde;o de piscina &eacute; coisa s&eacute;ria e cara. Economize na escolha dos materiais, mas n&atilde;o na m&atilde;o de obra.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	2 - Defina qual o tamanho da piscina que ir&aacute; escolher. Se pretende reunir a fam&iacute;lia e muitos amigos evite piscinas pequenas.</div>\r\n<div>\r\n	&nbsp;&nbsp;</div>\r\n<div>\r\n	3 - Se voc&ecirc; tem ou pretende ter crian&ccedil;as e animais, escolha uma piscina que n&atilde;o seja muito funda e se preocupe com a constru&ccedil;&atilde;o de barreiras e coberturas, por quest&otilde;es de seguran&ccedil;a. As medidas mais usadas s&atilde;o de at&eacute; 1,30m ~ 1,40 na parte mais funda e 0,40m ~ 0,50m na parte mais rasa.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	4 - Escolha um local com boa incid&ecirc;ncia de sol. Ningu&eacute;m quer usar piscina que fica na sombra!</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	5 - Evite a constru&ccedil;&atilde;o da piscina em locais com muitas &aacute;rvores, al&eacute;m de fazerem sombra, as folhas podem tornar a limpeza e manuten&ccedil;&atilde;o da piscina um tormento.</div>', '2001201602151171163101..jpg', 'SIM', NULL, '5-dicas-para-construcao-de-sua-piscina', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_empresa`
--

CREATE TABLE `tb_empresa` (
  `idempresa` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `title_google` longtext NOT NULL,
  `keywords_google` longtext NOT NULL,
  `description_google` longtext NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'Index - CONHEÇA UM POUCO MAIS A BSB PISCINAS', '<p>\r\n	A BSB Piscinas oferece constru&ccedil;&atilde;o e reformas de piscinas de azulejos paginados, em vinil ou de pastilhas de cer&acirc;mica e vidro.</p>\r\n<p>\r\n	Instala&ccedil;&atilde;o de equipamentos em geral, assist&ecirc;ncia t&eacute;cnica especializada, aquecimento de piscinas, vendas e instala&ccedil;&atilde;o de saunas, equipamentos e acess&oacute;rios, duchas e cascatas, produtos qu&iacute;micos em geral e muito mais.</p>\r\n<p>\r\n	A empresa trabalha com profissionais experientes e altamente qualificados para melhor atend&ecirc;-lo..</p>', 'SIM', 0, '', '', '', 'index--conheca-um-pouco-mais-a-bsb-piscinas', NULL, NULL, NULL),
(2, 'A Empresa', '<div>\r\n	A BSB Piscinas oferece constru&ccedil;&atilde;o e reformas de piscinas de azulejos paginados, em vinil ou de pastilhas de cer&acirc;mica e vidro.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Instala&ccedil;&atilde;o de equipamentos em geral, assist&ecirc;ncia t&eacute;cnica especializada, aquecimento de piscinas, vendas e instala&ccedil;&atilde;o de saunas, equipamentos e acess&oacute;rios, duchas e cascatas, produtos qu&iacute;micos em geral e muito mais.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	A empresa trabalha com profissionais experientes e altamente qualificados para melhor atend&ecirc;-lo.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Fa&ccedil;a-nos uma visita e confira pessoalmente os produtos e servi&ccedil;os de nossa loja.</div>', 'SIM', 0, '', '', '', 'a-empresa', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_portifolios`
--

CREATE TABLE `tb_galerias_portifolios` (
  `idgaleriaportifolio` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_portifolio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_portifolios`
--

INSERT INTO `tb_galerias_portifolios` (`idgaleriaportifolio`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_portifolio`) VALUES
(56, '1801201601211613461660.jpg', 'SIM', NULL, NULL, 1),
(57, '1801201601219065919071.jpg', 'SIM', NULL, NULL, 1),
(58, '1801201601217500809609.jpg', 'SIM', NULL, NULL, 1),
(59, '1801201601215029438460.jpg', 'SIM', NULL, NULL, 1),
(60, '1801201601219206626911.jpg', 'SIM', NULL, NULL, 1),
(61, '1801201601217337058981.jpg', 'SIM', NULL, NULL, 1),
(62, '1801201601212796318510.jpg', 'SIM', NULL, NULL, 1),
(63, '1801201601214870947226.jpg', 'SIM', NULL, NULL, 1),
(64, '1801201601214052872057.jpg', 'SIM', NULL, NULL, 1),
(65, '1801201601234479846060.jpg', 'SIM', NULL, NULL, 2),
(66, '1801201601235552857716.jpg', 'SIM', NULL, NULL, 2),
(67, '1801201601239736923273.jpg', 'SIM', NULL, NULL, 2),
(68, '1801201601239677388378.jpg', 'SIM', NULL, NULL, 2),
(69, '1801201601233361782841.jpg', 'SIM', NULL, NULL, 2),
(70, '1801201601236905145947.jpg', 'SIM', NULL, NULL, 2),
(71, '1801201601234363822800.jpg', 'SIM', NULL, NULL, 2),
(72, '1801201601233319725452.jpg', 'SIM', NULL, NULL, 2),
(73, '1801201601253418638511.jpg', 'SIM', NULL, NULL, 3),
(74, '1801201601258606996391.jpg', 'SIM', NULL, NULL, 3),
(75, '1801201601256961277429.jpg', 'SIM', NULL, NULL, 3),
(76, '1801201601255179956782.jpg', 'SIM', NULL, NULL, 3),
(77, '1801201601254495808664.jpg', 'SIM', NULL, NULL, 3),
(78, '1801201601257087445027.jpg', 'SIM', NULL, NULL, 3),
(79, '1801201601257658960781.jpg', 'SIM', NULL, NULL, 3),
(80, '1801201601272351279444.jpg', 'SIM', NULL, NULL, 4),
(81, '1801201601273827580859.jpg', 'SIM', NULL, NULL, 4),
(82, '1801201601274413545239.jpg', 'SIM', NULL, NULL, 4),
(83, '1801201601272880898813.jpg', 'SIM', NULL, NULL, 4),
(84, '1801201601272935035944.jpg', 'SIM', NULL, NULL, 4),
(85, '1801201601276911364843.jpg', 'SIM', NULL, NULL, 4),
(86, '1801201601286827376898.jpg', 'SIM', NULL, NULL, 5),
(87, '1801201601293388748102.jpg', 'SIM', NULL, NULL, 5),
(88, '1801201601294080564015.jpg', 'SIM', NULL, NULL, 5),
(89, '1801201601296287310636.jpg', 'SIM', NULL, NULL, 5),
(90, '1801201601292787362589.jpg', 'SIM', NULL, NULL, 5),
(91, '1801201601298141263897.jpg', 'SIM', NULL, NULL, 5),
(92, '1801201601298475536923.jpg', 'SIM', NULL, NULL, 5),
(93, '1801201601304038358603.jpg', 'SIM', NULL, NULL, 6),
(94, '1801201601303377044705.jpg', 'SIM', NULL, NULL, 6),
(95, '1801201601308253097323.jpg', 'SIM', NULL, NULL, 6),
(96, '1801201601305393994553.jpg', 'SIM', NULL, NULL, 6),
(97, '1801201601304159047715.jpg', 'SIM', NULL, NULL, 6),
(98, '1801201601301637177852.jpg', 'SIM', NULL, NULL, 6),
(99, '1801201601302815949014.jpg', 'SIM', NULL, NULL, 6),
(100, '1801201601311504320274.jpg', 'SIM', NULL, NULL, 4),
(101, '1801201601318640931492.jpg', 'SIM', NULL, NULL, 4),
(102, '1801201601311699285618.jpg', 'SIM', NULL, NULL, 4),
(103, '1801201601317334135936.jpg', 'SIM', NULL, NULL, 4),
(104, '1801201601316676422539.jpg', 'SIM', NULL, NULL, 4),
(105, '1801201601314286375289.jpg', 'SIM', NULL, NULL, 4),
(106, '1801201601314904481702.jpg', 'SIM', NULL, NULL, 4),
(107, '1801201601318835341470.jpg', 'SIM', NULL, NULL, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_produtos`
--

CREATE TABLE `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(60, '0411201512378861499846.jpg', 'SIM', NULL, NULL, 1),
(61, '0411201512375680738633.jpg', 'SIM', NULL, NULL, 1),
(62, '0411201512377488805792.jpg', 'SIM', NULL, NULL, 1),
(63, '0411201512377394942070.jpg', 'SIM', NULL, NULL, 2),
(64, '0411201512376991435645.jpg', 'SIM', NULL, NULL, 1),
(65, '0411201512372540643177.jpg', 'SIM', NULL, NULL, 2),
(66, '0411201512376876153751.jpg', 'SIM', NULL, NULL, 1),
(67, '0411201512375633686569.jpg', 'SIM', NULL, NULL, 3),
(68, '0411201512372393239190.jpg', 'SIM', NULL, NULL, 3),
(69, '0411201512371952858500.jpg', 'SIM', NULL, NULL, 2),
(70, '0411201512379939975096.jpg', 'SIM', NULL, NULL, 1),
(71, '0411201512379966245426.jpg', 'SIM', NULL, NULL, 3),
(72, '0411201512376187474549.jpg', 'SIM', NULL, NULL, 2),
(73, '0411201512375143316897.jpg', 'SIM', NULL, NULL, 4),
(74, '0411201512374877993687.jpg', 'SIM', NULL, NULL, 2),
(75, '0411201512377018904437.jpg', 'SIM', NULL, NULL, 3),
(76, '0411201512373950905505.jpg', 'SIM', NULL, NULL, 5),
(77, '0411201512377911590577.jpg', 'SIM', NULL, NULL, 4),
(78, '0411201512372960734404.jpg', 'SIM', NULL, NULL, 5),
(79, '0411201512374669137919.jpg', 'SIM', NULL, NULL, 5),
(80, '0411201512379297010215.jpg', 'SIM', NULL, NULL, 2),
(81, '0411201512374583601160.jpg', 'SIM', NULL, NULL, 3),
(82, '0411201512376096030895.jpg', 'SIM', NULL, NULL, 5),
(83, '0411201512376084787059.jpg', 'SIM', NULL, NULL, 5),
(84, '0411201512376785101364.jpg', 'SIM', NULL, NULL, 4),
(85, '0411201512376059426503.jpg', 'SIM', NULL, NULL, 3),
(86, '0411201512373721688120.jpg', 'SIM', NULL, NULL, 4),
(87, '0411201512372820774983.jpg', 'SIM', NULL, NULL, 5),
(88, '0411201512371872003670.jpg', 'SIM', NULL, NULL, 6),
(89, '0411201512374021880535.jpg', 'SIM', NULL, NULL, 6),
(90, '0411201512371451600760.jpg', 'SIM', NULL, NULL, 4),
(91, '0411201512376665309750.jpg', 'SIM', NULL, NULL, 6),
(92, '0411201512379068817322.jpg', 'SIM', NULL, NULL, 4),
(93, '0411201512376274125311.jpg', 'SIM', NULL, NULL, 6),
(94, '0411201512377250133078.jpg', 'SIM', NULL, NULL, 6),
(95, '0411201512374498628348.jpg', 'SIM', NULL, NULL, 6),
(96, '1901201611126562990798.jpg', 'SIM', NULL, NULL, 9),
(97, '1901201611183179291350.jpg', 'SIM', NULL, NULL, 11),
(98, '1901201611184439705200.jpg', 'SIM', NULL, NULL, 11),
(99, '1901201611185608660777.jpg', 'SIM', NULL, NULL, 11),
(100, '1901201601044333854205.jpg', 'SIM', NULL, NULL, 16),
(101, '1901201606544442354150.jpg', 'SIM', NULL, NULL, 46),
(102, '1901201606545446490310.jpg', 'SIM', NULL, NULL, 46),
(103, '1901201606578331952202.jpg', 'SIM', NULL, NULL, 47),
(104, '1901201606575146636631.jpg', 'SIM', NULL, NULL, 47),
(105, '1901201606591896784669.jpg', 'SIM', NULL, NULL, 48);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins`
--

CREATE TABLE `tb_logins` (
  `idlogin` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `nome`, `senha`, `ativo`, `id_grupologin`, `email`) VALUES
(4, 'HomeWeb', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logs_logins`
--

CREATE TABLE `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1575, 'EXCLUSÃO DO LOGIN 30, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = \'30\'', '2015-10-28', '20:23:46', 4),
(1576, 'ALTERAÇÃO DO CLIENTE ', '', '2015-10-28', '21:25:09', 4),
(1577, 'ALTERAÇÃO DO CLIENTE ', '', '2015-10-28', '22:52:55', 4),
(1578, 'CADASTRO DO CLIENTE ', '', '2015-10-28', '23:06:06', 4),
(1579, 'CADASTRO DO CLIENTE ', '', '2015-10-28', '23:06:49', 4),
(1580, 'CADASTRO DO CLIENTE ', '', '2015-10-28', '23:07:14', 4),
(1581, 'ALTERAÇÃO DO CLIENTE ', '', '2015-10-28', '23:18:54', 4),
(1582, 'ALTERAÇÃO DO CLIENTE ', '', '2015-10-28', '23:22:02', 4),
(1583, 'ALTERAÇÃO DO CLIENTE ', '', '2015-10-28', '23:22:13', 4),
(1584, 'ALTERAÇÃO DO CLIENTE ', '', '2015-10-28', '23:22:59', 4),
(1585, 'CADASTRO DO CLIENTE ', '', '2015-11-02', '15:28:45', 4),
(1586, 'CADASTRO DO CLIENTE ', '', '2015-11-02', '15:29:15', 4),
(1587, 'CADASTRO DO CLIENTE ', '', '2015-11-02', '15:29:27', 4),
(1588, 'CADASTRO DO CLIENTE ', '', '2015-11-02', '15:29:35', 4),
(1589, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-02', '15:30:01', 4),
(1590, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '12:51:39', 4),
(1591, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '12:51:50', 4),
(1592, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '12:52:06', 4),
(1593, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '12:52:29', 4),
(1594, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '13:01:28', 4),
(1595, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '13:01:44', 4),
(1596, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '13:01:59', 4),
(1597, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '13:02:16', 4),
(1598, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '13:02:26', 4),
(1599, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '13:16:30', 4),
(1600, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '13:16:59', 4),
(1601, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '13:17:40', 4),
(1602, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '13:18:18', 4),
(1603, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '13:18:55', 4),
(1604, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '19:37:44', 4),
(1605, 'CADASTRO DO CLIENTE ', '', '2015-11-03', '19:41:57', 4),
(1606, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '19:59:41', 4),
(1607, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '20:00:17', 4),
(1608, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '20:04:24', 4),
(1609, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-03', '20:05:46', 4),
(1610, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '01:47:30', 4),
(1611, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '01:49:43', 4),
(1612, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '01:57:12', 4),
(1613, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '02:45:47', 4),
(1614, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '02:47:42', 4),
(1615, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '02:56:06', 4),
(1616, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '02:59:14', 4),
(1617, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '10:33:57', 4),
(1618, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '16:09:25', 4),
(1619, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '16:10:40', 4),
(1620, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '18:19:51', 4),
(1621, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '18:20:14', 4),
(1622, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '18:20:33', 4),
(1623, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '18:21:44', 4),
(1624, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '18:21:59', 4),
(1625, 'CADASTRO DO CLIENTE ', '', '2015-11-04', '18:22:22', 4),
(1626, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '18:22:46', 4),
(1627, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '18:22:55', 4),
(1628, 'ALTERAÇÃO DO CLIENTE ', '', '2015-11-04', '18:23:05', 4),
(1629, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-12', '10:40:54', 4),
(1630, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-12', '10:42:09', 4),
(1631, 'EXCLUSÃO DO LOGIN 1, NOME: Marcio, Email: marciomas@gmail.com', 'DELETE FROM tb_comentarios_dicas WHERE idcomentariodica = \'1\'', '2016-01-12', '10:44:42', 4),
(1632, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = \'1\'', '2016-01-18', '12:39:50', 4),
(1633, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = \'2\'', '2016-01-18', '12:39:57', 4),
(1634, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = \'3\'', '2016-01-18', '12:40:03', 4),
(1635, 'EXCLUSÃO DO LOGIN 36, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'36\'', '2016-01-18', '12:40:35', 4),
(1636, 'EXCLUSÃO DO LOGIN 37, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'37\'', '2016-01-18', '12:40:41', 4),
(1637, 'EXCLUSÃO DO LOGIN 38, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'38\'', '2016-01-18', '12:40:46', 4),
(1638, 'EXCLUSÃO DO LOGIN 39, NOME: , Email: ', 'DELETE FROM tb_parceiros WHERE idparceiro = \'39\'', '2016-01-18', '12:40:52', 4),
(1639, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-18', '12:56:23', 4),
(1640, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-18', '12:57:46', 4),
(1641, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-18', '13:07:40', 4),
(1642, 'EXCLUSÃO DO LOGIN 19, NOME:  , Email:  ', 'DELETE FROM tb_avaliacoes_produtos WHERE idavaliacaoproduto = \'19\'', '2016-01-18', '13:17:44', 4),
(1643, 'EXCLUSÃO DO LOGIN 20, NOME: Marcio, Email: marciomas@gmail.com', 'DELETE FROM tb_avaliacoes_produtos WHERE idavaliacaoproduto = \'20\'', '2016-01-18', '13:17:49', 4),
(1644, 'EXCLUSÃO DO LOGIN 21, NOME: Marcio, Email: marciomas@gmail.com', 'DELETE FROM tb_avaliacoes_produtos WHERE idavaliacaoproduto = \'21\'', '2016-01-18', '13:17:55', 4),
(1645, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-18', '13:20:23', 4),
(1646, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-18', '13:21:36', 4),
(1647, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-18', '13:22:32', 4),
(1648, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-18', '13:23:59', 4),
(1649, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-18', '13:24:36', 4),
(1650, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-18', '13:26:10', 4),
(1651, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-18', '13:28:14', 4),
(1652, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-18', '13:29:40', 4),
(1653, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-18', '13:30:22', 4),
(1654, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-18', '16:21:09', 4),
(1655, 'CADASTRO DO CLIENTE ', '', '2016-01-18', '16:21:36', 4),
(1656, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-18', '23:36:17', 4),
(1657, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-19', '10:43:17', 4),
(1658, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-19', '10:44:34', 4),
(1659, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-19', '10:49:04', 4),
(1660, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-19', '10:57:07', 4),
(1661, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-19', '11:01:10', 4),
(1662, 'EXCLUSÃO DO LOGIN 53, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'53\'', '2016-01-19', '11:01:25', 4),
(1663, 'EXCLUSÃO DO LOGIN 54, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'54\'', '2016-01-19', '11:01:31', 4),
(1664, 'EXCLUSÃO DO LOGIN 55, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'55\'', '2016-01-19', '11:01:37', 4),
(1665, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '11:07:03', 4),
(1666, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '11:09:25', 4),
(1667, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '11:11:40', 4),
(1668, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '11:14:01', 4),
(1669, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '11:17:36', 4),
(1670, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '11:21:27', 4),
(1671, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '11:24:33', 4),
(1672, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '11:26:10', 4),
(1673, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '13:02:05', 4),
(1674, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '13:04:03', 4),
(1675, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '13:06:24', 4),
(1676, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '13:08:11', 4),
(1677, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '13:09:21', 4),
(1678, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '13:10:13', 4),
(1679, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '13:11:53', 4),
(1680, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '13:15:34', 4),
(1681, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '13:20:40', 4),
(1682, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '13:22:11', 4),
(1683, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '13:23:29', 4),
(1684, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '13:25:06', 4),
(1685, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '13:28:28', 4),
(1686, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '13:33:02', 4),
(1687, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-19', '13:35:59', 4),
(1688, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '15:46:41', 4),
(1689, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '15:52:35', 4),
(1690, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '15:55:29', 4),
(1691, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '15:58:36', 4),
(1692, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-19', '16:00:24', 4),
(1693, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-19', '16:01:03', 4),
(1694, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-19', '16:01:38', 4),
(1695, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-19', '16:04:28', 4),
(1696, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '16:07:56', 4),
(1697, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '16:14:20', 4),
(1698, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '16:18:26', 4),
(1699, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '16:21:48', 4),
(1700, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '16:24:16', 4),
(1701, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '16:28:30', 4),
(1702, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '16:32:05', 4),
(1703, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '16:36:00', 4),
(1704, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '16:39:22', 4),
(1705, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '16:44:50', 4),
(1706, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-19', '16:45:47', 4),
(1707, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '16:49:20', 4),
(1708, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '16:51:31', 4),
(1709, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '16:57:22', 4),
(1710, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'6\'', '2016-01-19', '18:48:41', 4),
(1711, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'1\'', '2016-01-19', '18:48:48', 4),
(1712, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'2\'', '2016-01-19', '18:48:54', 4),
(1713, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'3\'', '2016-01-19', '18:49:00', 4),
(1714, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'4\'', '2016-01-19', '18:49:07', 4),
(1715, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'5\'', '2016-01-19', '18:49:13', 4),
(1716, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '18:53:16', 4),
(1717, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '18:56:56', 4),
(1718, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '18:59:08', 4),
(1719, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '19:01:31', 4),
(1720, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '19:05:57', 4),
(1721, 'CADASTRO DO CLIENTE ', '', '2016-01-19', '19:10:15', 4),
(1722, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-19', '19:20:23', 4),
(1723, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:28:43', 4),
(1724, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:31:13', 4),
(1725, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:33:07', 4),
(1726, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:35:36', 4),
(1727, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:37:30', 4),
(1728, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:39:39', 4),
(1729, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:41:18', 4),
(1730, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:43:14', 4),
(1731, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:43:54', 4),
(1732, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:45:24', 4),
(1733, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:46:35', 4),
(1734, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:48:07', 4),
(1735, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:49:55', 4),
(1736, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:51:00', 4),
(1737, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:53:00', 4),
(1738, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:54:22', 4),
(1739, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:55:29', 4),
(1740, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:56:36', 4),
(1741, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:58:36', 4),
(1742, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '01:59:43', 4),
(1743, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '02:03:17', 4),
(1744, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '02:06:53', 4),
(1745, 'CADASTRO DO CLIENTE ', '', '2016-01-20', '02:09:15', 4),
(1746, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-20', '02:11:25', 4),
(1747, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-20', '02:12:46', 4),
(1748, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-20', '02:15:41', 4),
(1749, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-20', '02:17:21', 4),
(1750, 'DESATIVOU O LOGIN 34', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'34\'', '2016-01-20', '03:03:08', 4),
(1751, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-20', '03:07:49', 4),
(1752, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-20', '03:10:40', 4),
(1753, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-20', '23:12:03', 4),
(1754, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-20', '23:13:51', 4),
(1755, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-20', '23:50:00', 4),
(1756, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-20', '23:50:51', 4),
(1757, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-22', '11:59:29', 4),
(1758, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-22', '11:59:55', 4),
(1759, 'CADASTRO DO CLIENTE ', '', '2016-01-25', '14:34:32', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_parceiros`
--

CREATE TABLE `tb_parceiros` (
  `idparceiro` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_portifolios`
--

CREATE TABLE `tb_portifolios` (
  `idportifolio` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` longtext,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_portifolios`
--

INSERT INTO `tb_portifolios` (`idportifolio`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Portfólio 1', '1801201601202030903080..jpg', '', NULL, NULL, NULL, 'SIM', NULL, 'portfolio-1'),
(2, 'Portfólio 2', '1801201601233753745034..jpg', '', NULL, NULL, NULL, 'SIM', NULL, 'portfolio-2'),
(3, 'Portfólio 3', '1801201601242946623795..jpg', '', NULL, NULL, NULL, 'SIM', NULL, 'portfolio-3'),
(4, 'Portfólio 4', '1801201601261833180300..jpg', '', NULL, NULL, NULL, 'SIM', NULL, 'portfolio-4'),
(5, 'Portfólio 5', '1801201601293639797804..jpg', '', NULL, NULL, NULL, 'SIM', NULL, 'portfolio-5'),
(6, 'Portfólio 6', '1801201601301149383774..jpg', '', NULL, NULL, NULL, 'SIM', NULL, 'portfolio-6');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

CREATE TABLE `tb_produtos` (
  `idproduto` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) UNSIGNED NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`) VALUES
(7, 'CASCATA AMAZONAS AÇO INOX', '1901201611073628499218..jpg', '<p>\r\n	A Cascata Amazonas &eacute; fabricada em a&ccedil;o inox e possui tamanho menor que os outros modelos. O que possibilita ser instalada em piscinas menores e at&eacute; spas.</p>', '', '', '', 'SIM', 0, 'cascata-amazonas-aco-inox', 52, 'Outras Marcas', NULL, NULL),
(8, 'CASCATA NIAGARA AÇO INOX', '1901201611094901437109..jpg', '<p>\r\n	A Cascata Niagara &eacute; fabricada em a&ccedil;o inox e possui tamanho menor que os outros modelos. O que possibilita ser instalada em piscinas menores e at&eacute; spas.</p>', '', '', '', 'SIM', 0, 'cascata-niagara-aco-inox', 52, 'Outras Marcas', NULL, NULL),
(9, 'CASCATA CANYON AÇO INOX', '1901201611116724776451..jpg', '<div>\r\n	A Cascata Canyon &eacute; fabricada em a&ccedil;o inox e possui tamanho menor que os outros modelos. O que possibilita ser instalada em piscinas menores e at&eacute; spas.</div>', '', '', '', 'SIM', 0, 'cascata-canyon-aco-inox', 52, 'Outras Marcas', NULL, NULL),
(10, 'CASCATA TUBULAR SPLASH INOX', '1901201611149312537925..jpg', '<p>\r\n	A Cascata tubular Splash &eacute; fabricada em a&ccedil;o inox e possui tamanho menor que os outros modelos. O que possibilita ser instalada em piscinas menores e at&eacute; spas.</p>', '', '', '', 'SIM', 0, 'cascata-tubular-splash-inox', 52, 'Outras Marcas', NULL, NULL),
(11, 'AQUECEDOR SOLAR PARA PISCINAS', '1901201611174537165082..jpg', '<p>\r\n	O coletor solar para piscinas Sodramar, &eacute; uma fonte de energia renov&aacute;vel e n&atilde;o poluente. Desenvolvido especialmente para trabalhar no aquecimento de piscinas com total praticidade e economia.</p>\r\n<p>\r\n	Seu funcionamento comp&otilde;e-se de um conjunto de mangueiras coletoras que captam a radia&ccedil;&atilde;o solar para aquecimento da &aacute;gua que encontra-se armazenada nos seus perfis. A libera&ccedil;&atilde;o da &aacute;gua aquecida para a piscina, acontecer&aacute; com o bombeamento autom&aacute;tico da &aacute;gua por um controlador diferencial de temperatura.</p>', '', '', '', 'SIM', 0, 'aquecedor-solar-para-piscinas', 46, 'Sodramar', NULL, NULL),
(12, 'CAPATERMICA ECOBLUE', '1901201611218082520299..jpg', '<p>\r\n	A Sibrape Pentair tem o melhor sistema para proteger a sua piscina com a mais conceituada tecnologia mundial em capas t&eacute;rmicas. Diferente das capas tradicionais, a GeoBubble&reg; utiliza uma estrutura de bolhas interligadas que n&atilde;o somente aumenta a vida &uacute;til da piscina como tamb&eacute;m a torna mais resistente &agrave; exposi&ccedil;&atilde;o dos raios UV, economizando energia e o uso de produtos qu&iacute;micos. A Ecoblue mant&eacute;m a temperatura est&aacute;vel, evitando o crescimento de algas, a evapora&ccedil;&atilde;o da &aacute;gua e conservando-a limpa por muito tempo.</p>', '', '', '', 'SIM', 0, 'capatermica-ecoblue', 46, 'Pentair Water', NULL, NULL),
(13, 'CLARIFICANTE E FLOCULANTE HTH', '1901201611243864188806..jpg', '<div>\r\n	Clarificante e floculante de Alto desempenho - Hth - 1L</div>', '', '', '', 'SIM', 0, 'clarificante-e-floculante-hth', 50, 'HtH', NULL, NULL),
(14, 'CORANTES AQUACOULEUR', '1901201611261425177790..jpg', '<div>\r\n	A BSB Piscinas oferece &agrave; venda os corantes AQUACOULEUR que &eacute; um processo patenteado para a colora&ccedil;&atilde;o tempor&aacute;ria de &aacute;guas de piscinas, spas, fontes e aqu&aacute;rios. Prop&otilde;e uma linha de colora&ccedil;&otilde;es de alt&iacute;ssima qualidade.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Quando dilu&iacute;do, AQUACOULEUR n&atilde;o mancha. AQUACOULEUR n&atilde;o altera a qualidade, o equil&iacute;brio ou a transpar&ecirc;ncia da &aacute;gua. AQUACOULEUR dilu&iacute;do n&atilde;o provoca nenhum dano a tecidos ou quaisquer equipamentos ou componentes da piscina, como liners ou o sistemas de filtragem.</div>', '', '', '', 'SIM', 0, 'corantes-aquacouleur', 50, 'AquaCouleur', NULL, NULL),
(15, 'ALGICIDA HTH', '1901201601029354909157..jpg', '<p>\r\n	Algicida Manuten&ccedil;&atilde;o - HTH</p>', '', '', '', 'SIM', 0, 'algicida-hth', 50, 'HTH', NULL, NULL),
(16, 'ILUMINAÇÃO PARA PISCINAS', '1901201601044343401388..jpg', '<p>\r\n	Os refletores sub-aqu&aacute;ticos foram desenvolvidos para oferecer-lhe a mais eficiente ilumina&ccedil;&atilde;o sub-aqu&aacute;tica em piscinas de vinil, fibra e alvenaria com baixo consumo de energia, o que possibilita voc&ecirc; usufruir de sua piscina no per&iacute;odo noturno, al&eacute;m de aprimorar sua est&eacute;tica.</p>', '', '', '', 'SIM', 0, 'iluminacao-para-piscinas', 49, 'Outras Marcas', NULL, NULL),
(17, 'REFLETORES LED SIBRAPE', '1901201601063332454744..jpg', '', '', '', '', 'SIM', 0, 'refletores-led-sibrape', 49, 'Sibrape', NULL, NULL),
(18, 'REFLETORES LED CMB AQUA', '1901201601084880696323..jpg', '', '', '', '', 'SIM', 0, 'refletores-led-cmb-aqua', 49, 'CMB Aqua', NULL, NULL),
(19, 'REFLETORES LED CMB', '1901201601092932374800..jpg', '', '', '', '', 'SIM', 0, 'refletores-led-cmb', 49, 'CMB Aqua', NULL, NULL),
(20, 'REFLETORES INOX FUNDO PISCINA', '1901201601108088855581..jpg', '', '', '', '', 'SIM', 0, 'refletores-inox-fundo-piscina', 49, 'Outras Marcas', NULL, NULL),
(21, 'SAUNA COMPACT LINE PREMIUM', '1901201601115088503385..jpg', '<p>\r\n	A sauna Compact Line foi projetada para ambientes de sauna &uacute;mida com medidas entre 6 e 50m3. Possui formato compacto e gabinete com tr&ecirc;s fun&ccedil;&otilde;es (chave liga e desliga, luz indicativa para controle do n&iacute;vel de &aacute;gua e luz indicativa de funcionamento), al&eacute;m de um tempo de gera&ccedil;&atilde;o de vapor em aproximadamente 5 minutos.</p>', '', '', '', 'SIM', 0, 'sauna-compact-line-premium', 47, 'Sodramar', NULL, NULL),
(22, 'ÓLEO EUCALIPTO', '1901201601156959747225..jpg', '<div>\r\n	Indica&ccedil;&otilde;es:</div>\r\n<div>\r\n	Calmante, desinfetante, descongestionante, purifica retirando energias negativas.</div>\r\n<div>\r\n	Aplica&ccedil;&atilde;o:</div>\r\n<div>\r\n	Ambientes, Saunas e Tamb&eacute;m na Limpeza de Pisos a agrad&aacute;vel sensa&ccedil;&atilde;o de Limpeza.</div>', '', '', '', 'SIM', 0, 'oleo-eucalipto', 47, 'Freole', NULL, NULL),
(23, 'CLORO GRANULADO 65% HTH', '1901201601204482283923..jpg', '<p>\r\n	Se tem algo que n&atilde;o pode faltar em sua piscina &eacute; confian&ccedil;a. A confian&ccedil;a de produtos de &oacute;tima qualidade como os Cloros, granulados e em tabletes, das linhas hth&reg; e Pace&reg;. O Cloro &eacute; indispens&aacute;vel no tratamento da &aacute;gua da piscina porque garante o m&aacute;ximo de prote&ccedil;&atilde;o contra bact&eacute;rias, odores desagrad&aacute;veis e diversas doen&ccedil;as infecciosas.</p>', '', '', '', 'SIM', 0, 'cloro-granulado-65-hth', 50, 'HtH', NULL, NULL),
(24, 'PACE AÇÃO TOTAL 10KG', '1901201601226850636671..jpg', '<div>\r\n	Cloro granulado HTH PACE 7em1.</div>\r\n<div>\r\n	10kg</div>', '', '', '', 'SIM', 0, 'pace-acao-total-10kg', 50, 'HtH', NULL, NULL),
(25, 'CLORO-HIPOCLRORITO CÁLCIO 65%', '1901201601239355006085..jpg', '<p>\r\n	O Cloro Hipoc&aacute;lcio &eacute; um poderoso desinfetante para &aacute;guas de piscinas com 65% de cloro ativo na sua composi&ccedil;&atilde;o a base de HIPOCLORITO DE C&Aacute;LCIO.</p>', '', '', '', 'SIM', 0, 'clorohipoclrorito-calcio-65', 50, 'Neocolor', NULL, NULL),
(26, 'CLORO PURO DOMCLOR', '1901201601353139471432..jpg', '<p>\r\n	Cloro puro 65% Hipoclorito de C&aacute;lcio: - CONTEM TAMPA DOSADORA, - N&Atilde;O CONTEM SODA C&Aacute;USTICA, - &Eacute; QUIMICAMENTE EST&Aacute;VEL, - CONTEM A CONCENTRA&Ccedil;&Atilde;O IDEAL DE CLORO ATIVO EM MENOR VOLUME DO PRODUTO,- TAMPA COM TRAVA IMPEDINDO A ABERTURA ACIDENTAL, - CERTIFICADO PELA ANVISA, como produto para uso tamb&eacute;m em tratamento de &aacute;gua pot&aacute;vel.</p>', '', '', '', 'SIM', 0, 'cloro-puro-domclor', 50, 'Domclor', NULL, NULL),
(27, 'CLORO GRANULADO', '1901201601285212941786..jpg', '<div>\r\n	Hidrosan &eacute; o Dicloro Org&acirc;nico exclusivo da HidroAll, fabricado de acordo com as caracter&iacute;sticas f&iacute;sico-qu&iacute;micas das &aacute;guas brasileiras. A f&oacute;rmula &uacute;nica (Patente Requerida) de Hidrosan o diferencia dos demais dicloros fabricados para aplica&ccedil;&otilde;es industriais e que s&atilde;o vendidos para tratamento de piscinas.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Hidrosan n&atilde;o reduz a Alcalinidade Total da &aacute;gua nem o pH, por isso, piscinas tratadas com Hidrosan permanecem com &aacute;gua balanceada, enquanto as tratadas com outros produtos se estragam ap&oacute;s 2 ou 3 meses de aplica&ccedil;&atilde;o.</div>', '', '', '', 'SIM', 0, 'cloro-granulado', 50, 'HidroAll', NULL, NULL),
(28, 'CLORO GRANULADO HIDROAZUL', '1901201601331579183832..jpg', '<div>\r\n	Cloro org&acirc;nico multifuncional, azul, granulado com dissolu&ccedil;&atilde;o r&aacute;pida, muito pr&aacute;tico porque reune v&aacute;rias fun&ccedil;&otilde;es em um unico produto:</div>\r\n<div>\r\n	Forte a&ccedil;&atilde;o desinfetante -&nbsp;</div>\r\n<div>\r\n	Algist&aacute;tico com prote&ccedil;&atilde;o cont&iacute;nua -&nbsp;</div>\r\n<div>\r\n	Exclusiva fun&ccedil;&atilde;o Fungicida -&nbsp;</div>\r\n<div>\r\n	Estabilizador de cloro com prote&ccedil;&atilde;o prolongada -</div>\r\n<div>\r\n	Oxidante de mat&eacute;ria org&acirc;nica gerada.</div>', '', '', '', 'SIM', 0, 'cloro-granulado-hidroazul', 50, 'HidroAzul', NULL, NULL),
(29, 'CLARIFICANTE E FLOCULANTE NEOFLOC', '1901201603463346078896..jpg', '<p>\r\n	Neofloc &eacute; um floculante, clarificante e auxiliar de filtra&ccedil;&atilde;o, que aglomera as particulas em suspens&atilde;o, de forma que o filtro consiga ret&ecirc;-las e sejam rapidamente decantadas para o fundo da piscina.</p>', '', '', '', 'SIM', 0, 'clarificante-e-floculante-neofloc', 50, 'Neofloc', NULL, NULL),
(30, 'CLORO MINERAL BRILLIANCE 10X1', '1901201603527916538649..jpg', '<p>\r\n	Sua piscina nunca brilhou assim. A tecnologia Mineral Brilliance&trade; &eacute; uma formula&ccedil;&atilde;o exclusiva de hth&reg; que combina o tradicional hipoclorito de c&aacute;lcio com outros ingredientes ativos. Esses ingredientes atuam em sinergia e geram um produto que traz m&uacute;ltiplos benef&iacute;cios, &eacute; o hth&reg; Cloro Aditivado Mineral Brilliance&trade; 10em1. Enquanto o produto age na elimina&ccedil;&atilde;o imediata de todos os germes (inclusive fungos causadores de frieira e candid&iacute;ase), ele libera &iacute;ons minerais na &aacute;gua que clarificam continuamente a piscina atrav&eacute;s da elimina&ccedil;&atilde;o de micropart&iacute;culas na filtra&ccedil;&atilde;o e previne a &aacute;gua verde. Finalmente, os &iacute;ons minerais s&atilde;o capazes de intensificar a reflex&atilde;o da luz numa &aacute;gua pura e isso &eacute; percebido como o efeito Mineral Brilliance&trade;, o n&iacute;vel mais elevado de cristalinidade da &aacute;gua. O resultado &eacute; uma &aacute;gua t&atilde;o pura e cristalina que brilha.</p>', '', '', '', 'SIM', 0, 'cloro-mineral-brilliance-10x1', 50, 'HtH', NULL, NULL),
(31, 'KIT TESTE PH E CLORO HTH', '1901201603557100296827..jpg', '<div>\r\n	Um kit para medir o cloro e o n&iacute;vel de pH da &aacute;gua. Seus resultados s&atilde;o confi&aacute;veis e simples de serem obtidos. Basta pingar nossa solu&ccedil;&atilde;o em um frasco com &aacute;gua da piscina e conferir na tabela de cores em qual n&iacute;vel est&aacute;.</div>\r\n<div>\r\n	Indicado para se obter resultados confi&aacute;veis de maneira pr&aacute;tica e simples. Um kit que mede o n&iacute;vel de cloro e pH da &aacute;gua, recomendado para ser utilizado antes da manuten&ccedil;&atilde;o rotineira da piscina.</div>', '', '', '', 'SIM', 0, 'kit-teste-ph-e-cloro-hth', 50, 'HtH', NULL, NULL),
(32, 'ADVANCED HTH FLUTUADOR', '1901201603583888787879..jpg', '<div>\r\n	Cloro de A&ccedil;&atilde;o Cont&iacute;nua.</div>\r\n<div>\r\n	Cuidado F&aacute;cil por at&eacute; 30 dias.</div>\r\n<div>\r\n	Dosador inteligente.</div>\r\n<div>\r\n	A&ccedil;&atilde;o Imediata.</div>\r\n<div>\r\n	A&ccedil;&atilde;o cont&iacute;nua</div>\r\n<div>\r\n	Elimina 99,99 % das bact&eacute;rias.</div>\r\n<div>\r\n	Clarifica a &aacute;gua.</div>\r\n<div>\r\n	Previne &aacute;gua verde.</div>\r\n<div>\r\n	Funcionalidade:</div>\r\n<div>\r\n	Purifica a &aacute;gua e inibe o crescimento de algas e aglomera res&iacute;duos em micropart&iacute;culas eliminando-as na filtra&ccedil;&atilde;o.</div>\r\n<div>\r\n	Resultado:</div>\r\n<div>\r\n	Piscina bem cuidada por 30 dias de uma maneira muito f&aacute;cil.</div>', '', '', '', 'SIM', 0, 'advanced-hth-flutuador', 50, 'HtH', NULL, NULL),
(33, 'CLORO ORGANICO ESTABILIZADO 10KG', '1901201604074446217016..jpg', '<p>\r\n	Um cloro org&acirc;nico e um poderoso desinfetante para &aacute;gua, com 100% de ingrediente ativo na sua composi&ccedil;&atilde;o.</p>', '', '', '', 'SIM', 0, 'cloro-organico-estabilizado-10kg', 50, 'Neoclor', NULL, NULL),
(34, 'CLORO MULTIAÇÃO 3X1', '1901201604147247636214..jpg', '<div>\r\n	Produto 3 em 1, com efeito do cloro (sanitizante), clarificante e algist&aacute;tico;</div>\r\n<div>\r\n	Com estabilizante de pH e alcalinidade;&nbsp;</div>\r\n<div>\r\n	Reduz o consumo de clarificante e algicida de manuten&ccedil;&atilde;o.</div>', '', '', '', 'SIM', 0, 'cloro-multiacao-3x1', 50, 'Neoclor', NULL, NULL),
(35, 'WEEKEND DESINFETANTE PARA PISCINA', '1901201604186690437793..jpg', '<div>\r\n	-DOSAGEM &Uacute;NICA</div>\r\n<div>\r\n	-DISPENSA OUTROS PRODUTOS</div>\r\n<div>\r\n	-LIMPA A PISCINA DE NOITE PARA O DIA</div>\r\n<div>\r\n	-IDEAL PARA SITIOS, CH&Aacute;CARAS E CASA DE VERANEIOS</div>\r\n<div>\r\n	Peso: 04-Kg</div>', '', '', '', 'SIM', 0, 'weekend-desinfetante-para-piscina', 50, 'HidroAzul', NULL, NULL),
(36, 'BARRILHA LEVE (CARBONATO DE SÓDIO)', '1901201604214871442177..jpg', '<p>\r\n	A Barrilha Hidroazul &eacute; um produto destinado a elevar o Ph Da &Aacute;gua. &Eacute; altamente sol&uacute;vel e isenta de impurezas.</p>', '', '', '', 'SIM', 0, 'barrilha-leve-carbonato-de-sodio', 50, 'HidroAzul', NULL, NULL),
(37, 'LIMPA BORDAS (GEL)', '1901201604244255845993..jpg', '<div>\r\n	Limpa Bordas Hidroazul</div>\r\n<div>\r\n	Gel biodegrad&aacute;vel</div>\r\n<div>\r\n	Perfume suave</div>\r\n<div>\r\n	N&atilde;o irrita as m&atilde;os</div>\r\n<div>\r\n	Possui f&oacute;rmula concentrada que rende mais</div>\r\n<div>\r\n	Remove &oacute;leos e gorduras das bordas das piscinas, piscinas infl&aacute;veis e m&oacute;veis pl&aacute;sticos de piscinas</div>\r\n<div>\r\n	N&atilde;o altera o pH</div>\r\n<div>\r\n	N&atilde;o provoca espuma</div>\r\n<div>\r\n	Frasco de 1 litro com bico dosador e visor de dosagem</div>', '', '', '', 'SIM', 0, 'limpa-bordas-gel', 50, 'HidroAzul', NULL, NULL),
(38, 'CLORO HIPOCLOR 65%', '1901201604285607539945..jpg', '<div>\r\n	Hipoclor &eacute; o Dicloro Org&acirc;nico exclusivo da HidroAll, fabricado de acordo com as caracter&iacute;sticas f&iacute;sico-qu&iacute;micas das &aacute;guas brasileiras. A f&oacute;rmula &uacute;nica (Patente Requerida) de Hipoclor o diferencia dos demais dicloros fabricados para aplica&ccedil;&otilde;es industriais e que s&atilde;o vendidos para tratamento de piscinas.</div>\r\n<div>\r\n	Hipoclor n&atilde;o reduz a Alcalinidade Total da &aacute;gua nem o pH, por isso, piscinas tratadas com Hipoclor permanecem com &aacute;gua balanceada, enquanto as tratadas com outros produtos se estragam ap&oacute;s 2 ou 3 meses de aplica&ccedil;&atilde;o.</div>', '', '', '', 'SIM', 0, 'cloro-hipoclor-65', 50, 'HidroAll', NULL, NULL),
(39, 'CLORO HCL PLUS', '1901201604321773447820..jpg', '<div>\r\n	O novo HCL Plus Dicloro &eacute; muito mais moderno, pr&aacute;tico e f&aacute;cil de usar, pois possui estabilizador em sua f&oacute;rmula que impede a decomposi&ccedil;&atilde;o do cloro residual pelo sol e elimina a necessidade de usar produtos complementares, j&aacute; que n&atilde;o altera o pH e a alcalinidade da &aacute;gua.</div>\r\n<div>\r\n	Agora com exclusivos gr&acirc;nulos altamente sol&uacute;veis, n&atilde;o deixa a &aacute;gua com aspecto leitoso nem res&iacute;duos na superf&iacute;cie e &eacute; ideal para todos os tipos de piscinas.</div>', '', '', '', 'SIM', 0, 'cloro-hcl-plus', 50, 'HidroAll', NULL, NULL),
(40, 'CLORO TABLETE 200G', '1901201604357603213778..jpg', '<p>\r\n	HCL Penta &eacute; o Tricloro Org&acirc;nico aditivado com clarificante, algicida, floculante e eliminador de mat&eacute;ria org&acirc;nica. Em formato de tabletes.</p>', '', '', '', 'SIM', 0, 'cloro-tablete-200g', 50, 'Outras Marcas', NULL, NULL),
(41, 'CLORO PROPOOL 3X1', '1901201604393946907660..jpg', '<div>\r\n	PROPOOL 3em1:&nbsp;</div>\r\n<div>\r\n	Dicloro estabilizado com tripla a&ccedil;&atilde;o: bactericida, algic&iacute;da e clarificante. O tratamento Propool 3 em 1 foi desenvolvido para proporcionar higieniza&ccedil;&atilde;o com praticidade aos consumidores.</div>\r\n<div>\r\n	Bactericida</div>\r\n<div>\r\n	Algicist&aacute;tico</div>\r\n<div>\r\n	Clarificante</div>\r\n<div>\r\n	Pr&aacute;tico e moderno</div>', '', '', '', 'SIM', 0, 'cloro-propool-3x1', 50, 'Outras Marcas', NULL, NULL),
(42, 'CLORO DICLORO PROMOCIONAL', '1901201604455408634572..jpg', '<div>\r\n	Produto de Multi fun&ccedil;&otilde;es onde transmite ao tratamento qualidade invej&aacute;vel com menos esfor&ccedil;o e maior facilidade.</div>\r\n<div>\r\n	- Pr&oacute;prio para dosagens manuais di&aacute;rias ou prepara&ccedil;&atilde;o f&aacute;cil e r&aacute;pida de solu&ccedil;&otilde;es com alto teor de cloro ativo.</div>\r\n<div>\r\n	- Minimiza as etapas do tratamento por conter em sua formula diversos componentes associados.</div>\r\n<div>\r\n	- Dicloro em gr&acirc;nulos n&atilde;o produz poeira no manuseio, eliminando riscos para os olhos, pele, nariz e roupas.</div>\r\n<div>\r\n	- F&aacute;cil manuseio.</div>\r\n<div>\r\n	- Pode ser armazenado sem perda apreci&aacute;vel de sua concentra&ccedil;&atilde;o de cloro. Validade de 1 ano ap&oacute;s data de fabrica&ccedil;&atilde;o.</div>\r\n<div>\r\n	- Indicado tanto para super clora&ccedil;&atilde;o como para clora&ccedil;&atilde;o de manuten&ccedil;&atilde;o de piscinas de qualquer tamanho.</div>\r\n<div>\r\n	- Como &eacute; utilizado em pequenas dosagens, produz pouco impacto sobre o pH da &aacute;gua.</div>\r\n<div>\r\n	- Baixo teor de insol&uacute;vei.</div>\r\n<div>\r\n	- O produto cont&eacute;m 40% de cloro ativo, isto &eacute;, 1 quilo de Dicloro Mult- a&ccedil;&atilde;o Limper fornece 400 gramas de cloro ativo.</div>\r\n<div>\r\n	- 100% ativo &ndash; composto de Dicloro org&acirc;nico, clarificante</div>', '', '', '', 'SIM', 0, 'cloro-dicloro-promocional', 50, 'Limper', NULL, NULL),
(43, 'ALGICIDA DE CHOQUE 1LT', '1901201604498746561718..jpg', '<div>\r\n	Indicado para eliminar as algas da sua piscina. Com f&oacute;rmula mais concentrada, &eacute; utilizado em tratamentos de choque, n&atilde;o mancha revestimentos. Algas s&atilde;o organismos microsc&oacute;picos que se desenvolvem na presen&ccedil;a de &aacute;gua e luz solar, e que podem, rapidamente, tornar a &aacute;gua da piscina verde e turva, com pisos e paredes escorregadias. Algas pretas aparecem como manchas escuras nas paredes e fundo da piscina e nas juntas dos azulejos. Algumas esp&eacute;cies formam col&ocirc;nias de cor amarelada (mostarda), ou marrom. A infesta&ccedil;&atilde;o de algas geralmente acontece no ver&atilde;o, ap&oacute;s chuvas intensas, quando residuais de cloro se encontram abaixo do n&iacute;vel recomendado. Mas, depois que elas se desenvolvem somente um tratamento de choque consegue elimin&aacute;-las.</div>\r\n<div>\r\n	Embalagem: Frasco de 1 l e 5 lt</div>', '', '', '', 'SIM', 0, 'algicida-de-choque-1lt', 50, 'Limper', NULL, NULL),
(44, 'SULFATO DE COBRE 1KG', '1901201604511984764221..jpg', '<p>\r\n	O sulfato de cobre, tamb&eacute;m conhecido como pedra azul, &eacute; usado algumas vezes em piscinas como um algicida. O &quot;American Journal of Public Health&quot; recomenda usar o produto em piscinas apenas como um m&eacute;todo de controlar o crescimento de algas. Portanto, ele n&atilde;o deve ser usado como parte do tratamento qu&iacute;mico regular de sua piscina. Ao estimar a alcalinidade da &aacute;gua, &eacute; poss&iacute;vel determinar a quantidade necess&aacute;ria de sulfato de cobre a ser adicionada para controlar o crescimento de algas.</p>', '', '', '', 'SIM', 0, 'sulfato-de-cobre-1kg', 50, 'Variadas Marcas', NULL, NULL),
(45, 'OXIDANTE 500G', '1901201604577630420633..jpg', '<div>\r\n	&gt; Elimina a mat&eacute;ria org&acirc;nica e outros contaminantes sem formar cloraminas e sem cheiro.</div>\r\n<div>\r\n	&gt; Substitui a superclora&ccedil;&atilde;o ou clora&ccedil;&atilde;o de choque com vantagens.</div>\r\n<div>\r\n	&gt; Reduz o consumo e aumenta o poder desinfetante do cloro.</div>\r\n<div>\r\n	&gt; N&atilde;o forma trihalometanos (res&iacute;duos cancer&iacute;genos).</div>\r\n<div>\r\n	&gt; N&atilde;o precisa pr&eacute;-dissolver.</div>\r\n<div>\r\n	&gt; N&atilde;o &eacute; necess&aacute;rio interditar a piscina (em 15 minutos a a&ccedil;&atilde;o est&aacute; completa).</div>\r\n<div>\r\n	&gt; Restaura o brilho da &aacute;gua.</div>', '', '', '', 'SIM', 0, 'oxidante-500g', 50, 'Outras Marcas', NULL, NULL),
(46, 'FILTROS SODRAMAR', '1901201606536624509522..jpg', '<div>\r\n	Filtros e Conjuntos Filtrantes Sodramar.</div>\r\n<div>\r\n	Os Filtros Sodramar possuem tanques mais resistentes o suficiente para aguentar as press&otilde;es internas a que s&atilde;o submetidos e s&atilde;o produzidos com material anti-corrosivo e possui v&aacute;lvula multivias que melhora a qualidade de filtra&ccedil;&atilde;o.</div>', '', '', '', 'SIM', 0, 'filtros-sodramar', 48, 'Sodramar', NULL, NULL),
(47, 'FILTROS JACUZZI', '1901201606561160371075..jpg', '<div>\r\n	Filtros e Conjuntos Filtrantes Jacuzzi -&nbsp;</div>\r\n<div>\r\n	Os filtros TP s&atilde;o equipamentos de filtra&ccedil;&atilde;o de alta vaz&atilde;o, totalmente &agrave; prova de corros&atilde;o, destinados a remover mat&eacute;ria em suspens&atilde;o e coloidal e, portanto, indispens&aacute;veis &agrave; purifica&ccedil;&atilde;o da &aacute;gua.</div>\r\n<div>\r\n	O filtro CFA &eacute; um equipamento de filtra&ccedil;&atilde;o de alta vaz&atilde;o, com areia permanente, destinado a remover mat&eacute;ria em suspens&atilde;o coloidal e, portanto, indispens&aacute;vel &agrave; purifica&ccedil;&atilde;o da &aacute;gua.</div>\r\n<div>\r\n	Os filtros FIT s&atilde;o indicados para piscinas de pequeno porte, com alta vaz&atilde;o com taxa de filtra&ccedil;&atilde;o de at&eacute; 1.450 m3/ m2/ dia.</div>', '', '', '', 'SIM', 0, 'filtros-jacuzzi', 48, 'Jacuzzi', NULL, NULL),
(48, 'FILTROS SIBRAPE', '1901201606592053443727..jpg', '<div>\r\n	Filtros e Conjuntos Filtrantes SIBRAPE.</div>\r\n<div>\r\n	O Filtro S20 &eacute; indicado para piscinas de at&eacute; 20m&sup3;.</div>\r\n<div>\r\n	O Filtro S30 &eacute; indicado para piscinas de at&eacute; 31,2m&sup3;.</div>\r\n<div>\r\n	O Filtro S40 &eacute; indicado para piscinas de at&eacute; 44,8m&sup3;.</div>\r\n<div>\r\n	O Filtro S50 &eacute; indicado para piscinas de at&eacute; 70,4m&sup3;.</div>\r\n<div>\r\n	O Filtro S60 &eacute; indicado para piscinas de at&eacute; 98,4m&sup3;.</div>\r\n<div>\r\n	&Eacute; importante saber que o filtro da piscina deve ser dimensionado de acordo com o tamanho da piscina e quantidade de &aacute;gua (m&sup3;), pois toda piscina precisa de um tempo m&iacute;nimo de filtragem de acordo com o tamanho, para que toda &aacute;gua passe pelo filtro e retorne limpa. No caso de piscinas residenciais, esse tempo &eacute; de 8 horas por dia e, nas de uso coletivo, &eacute; importante que o sistema tenha a capacidade de filtrar toda a &aacute;gua da piscina em apenas 6 horas. A capacidade de vaz&atilde;o do conjunto filtrante (motobomba / filtro) deve ser bem dimensionada para n&atilde;o haver desperd&iacute;cio de tempo e energia</div>', '', '', '', 'SIM', 0, 'filtros-sibrape', 48, 'Sibrape', NULL, NULL),
(49, 'FILTROS E CARTUCHOS JACUZZI', '1901201607018944942909..jpg', '', '', '', '', 'SIM', 0, 'filtros-e-cartuchos-jacuzzi', 48, 'Jacuzzi', NULL, NULL),
(50, 'FILTROS E BOMBAS PENTAIR WATER', '1901201607052281559767..jpg', '<p>\r\n	O filtro Pentair Water Piscinas&trade; Linha Pro possuem v&aacute;lvulas diferenciadas, que possibilitam maior vaz&atilde;o, filtram muito mais &aacute;gua e consomem menos energia, gerando qualidade e economia para o usu&aacute;rio. Os tanques s&atilde;o rotomoldados em material termopl&aacute;stico, totalmente &agrave; prova de corros&atilde;o. A espessura do tanque &eacute; a maior do mercado, assegurando maior vida &uacute;til do equipamento.</p>', '', '', '', 'SIM', 0, 'filtros-e-bombas-pentair-water', 48, 'Pentair Water', NULL, NULL),
(51, 'FILTROS E BOMBAS ALBACETE', '1901201607108617510770..jpg', '<div>\r\n	Filtro de Fibra de Vidro S&eacute;rie A &ndash; S&atilde;o produzidos em resina de poli&eacute;ster refor&ccedil;ada com fibra de vidro e acabamento em gel-coat, possuem v&aacute;lvula seletora de seis opera&ccedil;&otilde;es fabricada em termopl&aacute;stico, refor&ccedil;ada com fibra de vidro.</div>\r\n<div>\r\n	Filtros S&eacute;rie AP e P - Os filtros s&eacute;rie AP s&atilde;o os &uacute;nicos no Brasil fabricados em polietitileno soprado Tecnologia Albace, al&eacute;m de contar com a Linha P onde o conjunto filtrante Filtro + motobomba s&atilde;o acoplados a um carrinho que oferece total deslocamento, perfeitos para Piscinas sem casa de m&aacute;quinas ou para profissionais da &aacute;rea.</div>\r\n<div>\r\n	S&eacute;rie R &ndash; Linha profissional Albacete - &nbsp;produzidos em resina de poli&eacute;ster refor&ccedil;ada com fibra de vidro e acabamento em gel-coat isofit&aacute;lico branco; proporcionando grande resist&ecirc;ncia e durabilidade.</div>', '', '', '', 'SIM', 0, 'filtros-e-bombas-albacete', 48, 'Albacete', NULL, NULL),
(52, 'ADAPTADOR DE MANGUEIRA', '2001201601289753685411..jpg', '', '', '', '', 'SIM', 0, 'adaptador-de-mangueira', 51, 'Sibrape', NULL, NULL),
(53, 'PONTEIRA DE BORRACHA', '2001201601313295043425..jpg', '', '', '', '', 'SIM', 0, 'ponteira-de-borracha', 51, 'Sibrape', NULL, NULL),
(54, 'RODO ASPIRADOR C/ 03-RODAS', '2001201601332574410736..jpg', '', '', '', '', 'SIM', 0, 'rodo-aspirador-c-03rodas', 51, 'Sibrape', NULL, NULL),
(55, 'ASPIRADOR GRID 8 RODAS', '2001201601354946925313..jpg', '', '', '', '', 'SIM', 0, 'aspirador-grid-8-rodas', 51, 'Sibrape', NULL, NULL),
(56, 'SPIRADOR DELTA C/ESCOVA', '2001201601377093731628..jpg', '', '', '', '', 'SIM', 0, 'spirador-delta-cescova', 51, 'Sibrape', NULL, NULL),
(57, 'CABO ALUMINIO TELESCÓPICO 3MT', '2001201601397771127360..jpg', '', '', '', '', 'SIM', 0, 'cabo-aluminio-telescopico-3mt', 51, 'Sibrape', NULL, NULL),
(58, 'CABO ALUMINIO TELESCÓPICO 4MT', '2001201601419763045596..jpg', '', '', '', '', 'SIM', 0, 'cabo-aluminio-telescopico-4mt', 51, 'Sibrape', NULL, NULL),
(59, 'CABO ALUMINIO TELESCÓPICO 5MT', '2001201601439708498749..jpg', '', '', '', '', 'SIM', 0, 'cabo-aluminio-telescopico-5mt', 51, 'Sibrape', NULL, NULL),
(60, 'CABO ALUMINIO TELESCÓPICO 6MT', '2001201601432009807829..jpg', '', '', '', '', 'SIM', 0, 'cabo-aluminio-telescopico-6mt', 51, 'Sibrape', NULL, NULL),
(61, 'ESCADAS EM AÇO INOX', '2001201601455384418326..jpg', '<p>\r\n	Escadas Para Piscinas em A&ccedil;o Inox.</p>', '', '', '', 'SIM', 0, 'escadas-em-aco-inox', 51, 'Variadas Marcas', NULL, NULL),
(62, 'ESTERELIZADOR', '2001201601464862793460..jpg', '<p>\r\n	Esterelizador para Piscinas.</p>', '', '', '', 'SIM', 0, 'esterelizador', 51, 'Outras Marcas', NULL, NULL),
(63, 'KIT LIMPEZA PARA PISCINAS', '2001201601489931142955..jpg', '<div>\r\n	O Kit Limpeza agrega os produtos necess&aacute;rios para limpeza de sua piscina.</div>\r\n<div>\r\n	Na vers&atilde;o para piscina de Vinil, Fibra e ou Alvenaria acompanha: 02 ponteiras de borracha 1 1/2, 01 adaptador pl&aacute;stico 1 1/2, 01 escova de nylon curva, 01 peneira pl&aacute;stica plus e 01 aspirador com escova tipo Asa Delta.</div>', '', '', '', 'SIM', 0, 'kit-limpeza-para-piscinas', 51, 'Variadas Marcas', NULL, NULL),
(64, 'SKIMMER', '2001201601494024067463..jpg', '<p>\r\n	Os skimmers promovem a movimenta&ccedil;&atilde;o e remo&ccedil;&atilde;o constante da camada superficial da &aacute;gua da piscina, retirando as impurezas em suspens&atilde;o na l&acirc;mina d&aacute;gua da piscina, onde se encontra o maior &iacute;ndice de contamina&ccedil;&atilde;o, em fun&ccedil;&atilde;o da menor presen&ccedil;a do teor de cloro, maior n&uacute;mero de microrganismo, al&eacute;m dos res&iacute;duos flutuantes como insetos, folhas, cabelos, pap&eacute;is, &oacute;leos bronzeadores, secre&ccedil;&otilde;es buco - nasais etc. Para piscinas de alvenaria, vinil e fibra , sendo estes nas op&ccedil;&otilde;es boca larga e boca pequena. Fabricados em termopl&aacute;stico de alta qualidade (ABS), possuem insertos em lat&atilde;o e sa&iacute;da para acoplamento de aspira&ccedil;&atilde;o, o que reduz o custo da obra. Nos modelos boca larga e boca pequena existe o sistema exclusivo de regulagem de altura , adaptando-se aos diferentes tipos de revestimentos no piso.</p>', '', '', '', 'SIM', 0, 'skimmer', 51, 'Variadas Marcas', NULL, NULL),
(65, 'ESTERELIZADOR ES-55', '2001201601501307706170..jpg', '<p>\r\n	Esterelizador Pool Clean ES-55 - Sibrape</p>', '', '', '', 'SIM', 0, 'esterelizador-es55', 51, 'Sibrape', NULL, NULL),
(66, 'CLORADOR COM TERMÔMETRO SIBRAPE', '2001201601534960292792..jpg', '', '', '', '', 'SIM', 0, 'clorador-com-termometro-sibrape', 51, 'Sibrape', NULL, NULL),
(67, 'PENEIRAS CATA FOLHAS', '2001201601547431704840..jpg', '', '', '', '', 'SIM', 0, 'peneiras-cata-folhas', 51, 'Sibrape', NULL, NULL),
(68, 'DRENO FUNDO ABS UNIVERSAL', '2001201601558386034042..jpg', '', '', '', '', 'SIM', 0, 'dreno-fundo-abs-universal', 51, 'Sibrape', NULL, NULL),
(69, 'DISPOSITIVO RETORNO ABS UNIVERSAL', '2001201601569767962495..jpg', '', '', '', '', 'SIM', 0, 'dispositivo-retorno-abs-universal', 51, 'Sibrape', NULL, NULL),
(70, 'SKIMMER EM ABS UNIVERSAL BOCA LARGA', '2001201601583109748235..jpg', '', '', '', '', 'SIM', 0, 'skimmer-em-abs-universal-boca-larga', 51, 'Sibrape', NULL, NULL),
(71, 'ESCADA INOX C/ DEGRAU ABS', '2001201601598793857383..jpg', '', '', '', '', 'SIM', 0, 'escada-inox-c-degrau-abs', 51, 'Sibrape', NULL, NULL),
(72, 'PISCINAS DE VINIL', '2001201602035602488223..jpg', '<div>\r\n	Constru&ccedil;&atilde;o e reforma de piscinas de vinil em Brasilia - DF e entorno.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Uma piscina de vinil &eacute; mais do que uma evolu&ccedil;&atilde;o no jeito de construir uma piscina, &eacute; uma evolu&ccedil;&atilde;o da piscina. S&oacute; &eacute; necess&aacute;rio o revestimento vin&iacute;lico, e uma estrutura forte de concreto mixto.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Projetos Ousados: a versatilidade do vinil e a constru&ccedil;&atilde;o da estrutura em alvenaria acompanham curvas e &acirc;ngulos dos projetos mais ousados. &Eacute; uma piscina exclusiva em raz&atilde;o da variedade de estampas combinadas com seu projeto.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Cole&ccedil;&atilde;o de Estampas: cada lan&ccedil;amento revela as tend&ecirc;ncias mundiais de design.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Constru&ccedil;&atilde;o e Instala&ccedil;&atilde;o: pode ficar pronta em at&eacute; 45 dias. Como o revestimento vin&iacute;lico &eacute; imperme&aacute;vel, dispensa a impermeabiliza&ccedil;&atilde;o. S&oacute; &eacute; necess&aacute;rio uma estrutura de concreto mixto. O revestimento vinilico &eacute; instalado em horas, voc&ecirc; economiza tempo e m&atilde;o-de-obra.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Imperme&aacute;vel: o revestimento vin&iacute;lico pode ficar pr&oacute;ximo ao alicerce da casa. Caso ocorra alguma pequena fissura, a solda &eacute; feita em minutos com a piscina cheia. Tratamento f&iacute;sico da piscina &eacute; mais f&aacute;cil de limpar. N&atilde;o tem os rejuntes da piscina de azulejos, que facilitam a forma&ccedil;&atilde;o de algas e fungos.</div>', '', '', '', 'SIM', 0, 'piscinas-de-vinil', 45, 'Variadas Marcas', NULL, NULL),
(73, 'CONSTRUÇÃO PISCINAS DE PASTILHAS 10X10', '2001201602064459488718..jpg', '<p>\r\n	Constru&ccedil;&atilde;o e reforma de piscinas de pastilhas em Brasilia - DF e entorno.</p>', '', '', '', 'SIM', 0, 'construcao-piscinas-de-pastilhas-10x10', 56, 'Sibrape', NULL, NULL),
(74, 'PISCINAS DE AZULEJO E PASTILHAS', '2001201602093834455658..jpg', '<div>\r\n	Constru&ccedil;&atilde;o e reforma de piscinas de azulejo e pastilhas em Brasilia e entorno.</div>\r\n<div>\r\n	Nos dias de hoje, as &lsquo;&rsquo;pastilhas&rsquo;&rsquo; que s&atilde;o fabricadas s&atilde;o normalmente muito brilhantes, coloridas e f&aacute;ceis de aplicar. Gra&ccedil;as a isto, est&atilde;o mais populares do que nunca. Estes belos mosaicos apresentam uma superf&iacute;cie extremamente lisa e rugada, com bordos arredondados ou n&atilde;o, de acordo com o gosto do cliente em quest&atilde;o, podendo ser usados em qualquer tipo de superf&iacute;cie. Permite projetos personalizados (formatos e modelos). &Eacute; poss&iacute;vel executar com degraus, spas, cascatas, etc. Tem grande resist&ecirc;ncia mec&acirc;nica.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	A BSB PISCINAS oferece alto padr&atilde;o de qualidade t&eacute;cnica para constru&ccedil;&atilde;o e reforma de piscinas em azulejo e pastilhas de cer&acirc;mica. Contando com a melhor equipe do centro-oeste do Brasil. Solicite visita t&eacute;cnica e confirme nossa seriedade e veracidade de atendimento.</div>', '', '', '', 'SIM', 0, 'piscinas-de-azulejo-e-pastilhas', 56, 'Variadas Marcas', NULL, NULL),
(75, 'CONSTRUÇÃO DE PISCINAS DE VINIL', '2501201602344899173370..jpg', '<p>\r\n	Constru&ccedil;&atilde;o e reforma de piscinas de vinil em Brasilia - DF e entorno. Uma piscina de vinil &eacute; mais do que uma evolu&ccedil;&atilde;o no jeito de construir uma piscina, &eacute; uma evolu&ccedil;&atilde;o da piscina. S&oacute; &eacute; necess&aacute;rio o revestimento vin&iacute;lico, e uma estrutura forte de concreto mixto. &nbsp; Projetos Ousados: a versatilidade do vinil e a constru&ccedil;&atilde;o da estrutura em alvenaria acompanham curvas e &acirc;ngulos dos projetos mais ousados. &Eacute; uma piscina exclusiva em raz&atilde;o da variedade de estampas combinadas com seu projeto. &nbsp; Cole&ccedil;&atilde;o de Estampas: cada lan&ccedil;amento revela as tend&ecirc;ncias mundiais de design. &nbsp; Constru&ccedil;&atilde;o e Instala&ccedil;&atilde;o: pode ficar pronta em at&eacute; 45 dias. Como o revestimento vin&iacute;lico &eacute; imperme&aacute;vel, dispensa a impermeabiliza&ccedil;&atilde;o. S&oacute; &eacute; necess&aacute;rio uma estrutura de concreto mixto. O revestimento vinilico &eacute; instalado em horas, voc&ecirc; economiza tempo e m&atilde;o-de-obra. &nbsp; Imperme&aacute;vel: o revestimento vin&iacute;lico pode ficar pr&oacute;ximo ao alicerce da casa. Caso ocorra alguma pequena fissura, a solda &eacute; feita em minutos com a piscina cheia. Tratamento f&iacute;sico da piscina &eacute; mais f&aacute;cil de limpar. N&atilde;o tem os rejuntes da piscina de azulejos, que facilitam a forma&ccedil;&atilde;o de algas e fungos.</p>', '', '', '', 'SIM', 0, 'construcao-de-piscinas-de-vinil', 56, 'Variadas Marcas', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_servicos`
--

CREATE TABLE `tb_servicos` (
  `idservico` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `id_categoriaservico` int(10) UNSIGNED NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`) VALUES
(35, 'Construção de Piscinas', '<div>\r\n	A BSB Piscinas &eacute; uma empresa especializada na constru&ccedil;&atilde;o e reformas de piscinas em vinil, pastilhas de cer&acirc;mica e vidro e azulejos paginados.Com a garantia de ter a realiza&ccedil;&atilde;o de seu projeto com total seguran&ccedil;a e qualidade, sempre utilizando os materiais mais adequados e zelando pela efici&ecirc;ncia em cada etapa do processo.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	ENTRE EM CONTATO COM A NOSSA EQUIPE PARA SOLICITAR UM OR&Ccedil;AMENTO PERSONALIZADO.</div>', '1901201607208140155010..jpg', 'SIM', NULL, 'construcao-de-piscinas', '', '', '', 0, '', ''),
(36, 'Equipamentos e Assistência Técnica', '<p>\r\n	A BSB Piscinas oferece a todos, uma presta&ccedil;&atilde;o de servi&ccedil;os com m&atilde;o-de-obra especializada na:</p>\r\n<p>\r\n	- instala&ccedil;&atilde;o de equipamentos em geral,</p>\r\n<p>\r\n	- assist&ecirc;ncia t&eacute;cnica,</p>\r\n<p>\r\n	- instala&ccedil;&atilde;o de aquecimento de piscinas,</p>\r\n<p>\r\n	- venda e instala&ccedil;&atilde;o de saunas,</p>\r\n<p>\r\n	- instala&ccedil;&atilde;o de acess&oacute;rios, duchas e cascatas,</p>\r\n<p>\r\n	- venda de produtos qu&iacute;micos em geral e muito mais.</p>\r\n<p>\r\n	ENTRE EM CONTATO COM A BSB PISCINAS E SOLICITE OR&Ccedil;AMENTO PARA O SERVI&Ccedil;O OU PRODUTO QUE VOC&Ecirc; PROCURA.</p>', '0411201501497639182739..jpg', 'SIM', NULL, 'equipamentos-e-assistencia-tecnica', '', '', '', 0, '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  ADD PRIMARY KEY (`idavaliacaoproduto`);

--
-- Indexes for table `tb_banners`
--
ALTER TABLE `tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- Indexes for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  ADD PRIMARY KEY (`idcategoriaproduto`);

--
-- Indexes for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  ADD PRIMARY KEY (`idcomentariodica`);

--
-- Indexes for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  ADD PRIMARY KEY (`idcomentarioproduto`);

--
-- Indexes for table `tb_comentarios_servicos`
--
ALTER TABLE `tb_comentarios_servicos`
  ADD PRIMARY KEY (`idcomentarioservico`);

--
-- Indexes for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  ADD PRIMARY KEY (`idconfiguracao`);

--
-- Indexes for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  ADD PRIMARY KEY (`iddepoimento`);

--
-- Indexes for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  ADD PRIMARY KEY (`iddica`);

--
-- Indexes for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indexes for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  ADD PRIMARY KEY (`idgaleriaportifolio`);

--
-- Indexes for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  ADD PRIMARY KEY (`id_galeriaproduto`);

--
-- Indexes for table `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`idlogin`);

--
-- Indexes for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  ADD PRIMARY KEY (`idloglogin`);

--
-- Indexes for table `tb_parceiros`
--
ALTER TABLE `tb_parceiros`
  ADD PRIMARY KEY (`idparceiro`);

--
-- Indexes for table `tb_portifolios`
--
ALTER TABLE `tb_portifolios`
  ADD PRIMARY KEY (`idportifolio`);

--
-- Indexes for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`idproduto`);

--
-- Indexes for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  ADD PRIMARY KEY (`idservico`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  MODIFY `idavaliacaoproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `tb_banners`
--
ALTER TABLE `tb_banners`
  MODIFY `idbanner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  MODIFY `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  MODIFY `idcomentariodica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=563;
--
-- AUTO_INCREMENT for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  MODIFY `idcomentarioproduto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_comentarios_servicos`
--
ALTER TABLE `tb_comentarios_servicos`
  MODIFY `idcomentarioservico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  MODIFY `idconfiguracao` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  MODIFY `iddepoimento` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  MODIFY `iddica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  MODIFY `idempresa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  MODIFY `idgaleriaportifolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  MODIFY `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  MODIFY `idloglogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1760;
--
-- AUTO_INCREMENT for table `tb_parceiros`
--
ALTER TABLE `tb_parceiros`
  MODIFY `idparceiro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `tb_portifolios`
--
ALTER TABLE `tb_portifolios`
  MODIFY `idportifolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `idproduto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  MODIFY `idservico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
