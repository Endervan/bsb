
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>
    <!-- FlexSlider -->
<script defer src="http://masmidia.com.br/clientes/san-remo/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
<!-- Syntax Highlighter -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

<!-- Optional FlexSlider Additions -->
<script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
<script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
<script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


<!-- Syntax Highlighter -->
<link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


<!-- Demo CSS -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />


<script>
        $(window).load(function() {
          // The slider being synced must be initialized first
          $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 170,
            itemMargin: 5,
            asNavFor: '#slider'
          });
         
          $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel"
          });
        });
    </script>

<!-- adicionando ou diminuindo quantidade de produto -->
<script>
      $(document).ready(function() {
      $("#mais").click(function(){
       
      if($("#mudeValor").val() < 10){
      $('#mudeValor').val(parseInt($('#mudeValor').val())+1); return false;
      } else {
      $('#limite').html('limite maximo 10');
      return false;
      }
       
      });
      $("#menos").click(function(){
      $('#limite').html('');
      if($("#mudeValor").val()!=0){$('#mudeValor').val(parseInt($('#mudeValor').val())-1);} return false;
      });
      });
</script>



</head>
<body>

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->



    <!-- bg-empresa -->
    <div class="container-fluir">
        <div class="row">
            <div class="bg-produtos"></div>
        </div>
    </div>
    <!-- bg-empresa -->



    <!-- descricao-empresa -->
    <div class="container">
        <div class="row">
            <div class="col-xs-3 top40 bottom10">
                <div class="descricao-empresa">
                    <h3>PRODUTOS</h3>
                </div>
            </div>

            <div class="col-xs-9 top20">
              
              <div class="topo-pesquisa1">
                <div class="col-xs-3">
                  <h1 class="top20">PESQUISAR PRODUTOS:</h1>
                </div>
              </div>

              <!-- barra pesquisa -->
              <div class="col-xs-3 top30">
                <div class="input-group ">
                  <input type="text" class="form-control barra-pesquisa-topo " placeholder="Busca por nome">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                  </span>
                </div>
            </div>
            <!-- menu topo selec -->
            <div class="col-xs-3 top28">
                <div class="dropdown ">
                  <button class="btn btn-default barra-pesquisa-topo11 dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    CATEGORIA
                    <span class="caret"></span>
                </button>
                    <ul class="dropdown-menu input100" aria-labelledby="dropdownMenu1">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-xs-3 top28">
                <div class="dropdown ">
                  <button class="btn btn-default barra-pesquisa-topo11 dropdown-toggle" type="button" id="dropdownMenu11" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    ORDENAR POR
                    <span class="caret"></span>
                </button>
                    <ul class="dropdown-menu input100" aria-labelledby="dropdownMenu11">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>
            </div>


         </div>
  </div>
</div>
<!-- descricao-empresa -->


<!-- descricao de produtos internos -->
<div class="container">
  <div class="row top40">
      <div class="col-xs-4 descricao-produtos">
        <!-- botao voltar -->
        <a href="#" class="btn btn-primary btn-voltar bottom20" role="button"><i class="fa fa-angle-left"></i>VOLTAR</a>
        <h6>PISCINAS EM VINIL</h6>
        <!-- botao voltar -->
      </div>


      <!-- atendimento -->
       <div class="col-xs-4 topo-telefone1">
         <h6>ATENDIMENTO EM</h6>
         <h2><span>(61)</span> 3552-1121 |  <span>(61)</span> 3552-1121</h2>

         
       </div>
        <!-- atendimento -->

        <div class="col-xs-4">
          <!-- adicinar quantidade-->
         <div class="pull-right">
          <a href="">
           <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-carrinho.png" alt="">
         </a>
         </div>

        </div>


    <!-- descricao do produto -->
    <div class="col-xs-12 slider-produtos1">
        <!-- Place somewhere in the <body> of your page -->
        <div id="slider" class="flexslider">
          <ul class="slides slider-prod">
               <li>
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/produtos-dentro-piscinas.jpg" />
              </li>

               <li>
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/produtos-dentro-piscinas.jpg" />
              </li>

              <li>
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/produtos-dentro-piscinas.jpg" />
              </li>

              <li>
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/produtos-dentro-piscinas.jpg" />
              </li>

              <li>
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/produtos-dentro-piscinas.jpg" />
              </li>
             <!-- items mirrored twice, total of 12 -->
          </ul>
        </div>


        <div class="slider-prod-dentro1">
        <div id="carousel" class="flexslider">
        <div class="col-xs-2 top25">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/selecione-orcamento.png" alt="">
        </div>
          <ul class="slides slider-prod-tumb col-xs-10">
              <li >
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/produtos-dentro-piscinas-tumbs.jpg" />
              </li>

              <li >
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/produtos-dentro-piscinas-tumbs.jpg" />
              </li>
               <li>
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/produtos-dentro-piscinas-tumbs.jpg" />
              </li>

              <li>
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/produtos-dentro-piscinas-tumbs.jpg" />
              </li>

              <li>
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/produtos-dentro-piscinas-tumbs.jpg" />
              </li>
              
              <li>
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/produtos-dentro-piscinas-tumbs.jpg" />
              </li>

              <li>
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/produtos-dentro-piscinas-tumbs.jpg" />
              </li>
          </ul>
        </div>
        </div>


 </div>

 
  </div>
</div>
<!-- descricao de produtos internos -->

  <!-- descricao de atentimento -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12 top60">
        <div class="descricao-empresa">
            <h3>DESCRIÇÃO</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ip
            sum has been the industry's standard dummy text ever since the 1500s, when an un
            known printer took a galley of type and scrambled it to make a type specimen book.
             It has survived not only five centuries, but also the leap into electronic typesetting, r
            emaining essentially unchanged. It was popularised in the 1960s with the release of 
            Letraset sheets containing LoremIpsum passages, and more recently with deskt
            </p>
        </div>
      </div>
    </div>
  </div>
  <!-- descricao de atentimento -->

  <!-- comentarios  -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12 top80">
        <div class="descricao-empresa11">
            <h3>COMENTÁRIOS</h3>
            <div class="jumbotron top50">
              <h6>MARCELO BARRETO</h6>
              <!-- avaliacao -->
              <div class="descricao-produtos text-left">
              <p class="rating"><span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span></p>
            </div>
            <!-- avaliacao -->
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived
              not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 196
              </p>
            </div>

            <div class="jumbotron">
              <h6>MARCELO BARRETO</h6>
              <!-- avaliacao -->
              <div class="descricao-produtos text-left">
              <p class="rating"><span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span></p>
            </div>
            <!-- avaliacao -->
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived
              not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 196
              </p>
            </div>

            <div class="jumbotron">
              <h6>MARCELO BARRETO</h6>
              <!-- avaliacao -->
              <div class="descricao-produtos text-left">
              <p class="rating"><span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span></p>
            </div>
            <!-- avaliacao -->
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived
              not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 196
              </p>
            </div>
        </div>
      </div>
    </div>
  </div>
  <!-- comentarios  -->

  <!-- veja tambem -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
      <div class="descricao-empresa11">
            <h3>VEJA TAMBÉM</h3>
       </div>

           <!-- item 01 -->
            <div class="col-xs-3 top30 bottom60">
             <div class="thumbnail produtos-home">

                <div class="view view-second">
                 <a href="<?php echo Util::caminho_projeto() ?>/produto">
                  <img src="<?php echo Util::caminho_projeto() ?>/imgs/produtos-home.jpg" alt="">
                </a>

                <div class="mask"></div>

                <div class="content">
                  <div class="col-xs-6">
                        <a href="<?php echo Util::caminho_projeto() ?>/orcamento" class="info"><i class="fa fa-plus-circle fa-4x"></i></a>
                        <h2>SAIBA MAIS</h2>
                  </div>
                  <div class="col-xs-6">
                        <a href="<?php echo Util::caminho_projeto() ?>/orcamento" class="info"><i class="fa fa-plus-circle fa-4x"></i></a>
                        <h2>SAIBA MAIS</h2>
                 </div>
               </div>

                </div>


                <div class="caption">
                  <h1>Piscinas de Azulejo e Pastilhas</h1>
                  <h3><i class="fa fa-star"></i>EM PISCINAS</h3>
                  <h3><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
                </div>
            </div>
          </div>


           <!-- item 01 -->
            <div class="col-xs-3 top30 bottom60">
             <div class="thumbnail produtos-home">

                <div class="view view-second">
                 <a href="<?php echo Util::caminho_projeto() ?>/produto">
                  <img src="<?php echo Util::caminho_projeto() ?>/imgs/produtos-home.jpg" alt="">
                </a>

                <div class="mask"></div>

                <div class="content">
                  <div class="col-xs-6">
                        <a href="<?php echo Util::caminho_projeto() ?>/orcamento" class="info"><i class="fa fa-plus-circle fa-4x"></i></a>
                        <h2>SAIBA MAIS</h2>
                  </div>
                  <div class="col-xs-6">
                        <a href="<?php echo Util::caminho_projeto() ?>/orcamento" class="info"><i class="fa fa-plus-circle fa-4x"></i></a>
                        <h2>SAIBA MAIS</h2>
                 </div>
               </div>

                </div>


                <div class="caption">
                  <h1>Piscinas de Azulejo e Pastilhas</h1>
                  <h3><i class="fa fa-star"></i>EM PISCINAS</h3>
                  <h3><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
                </div>
            </div>
          </div>


           <!-- item 01 -->
            <div class="col-xs-3 top30 bottom60">
             <div class="thumbnail produtos-home">

                <div class="view view-second">
                 <a href="<?php echo Util::caminho_projeto() ?>/produto">
                  <img src="<?php echo Util::caminho_projeto() ?>/imgs/produtos-home.jpg" alt="">
                </a>

                <div class="mask"></div>

                <div class="content">
                  <div class="col-xs-6">
                        <a href="<?php echo Util::caminho_projeto() ?>/orcamento" class="info"><i class="fa fa-plus-circle fa-4x"></i></a>
                        <h2>SAIBA MAIS</h2>
                  </div>
                  <div class="col-xs-6">
                        <a href="<?php echo Util::caminho_projeto() ?>/orcamento" class="info"><i class="fa fa-plus-circle fa-4x"></i></a>
                        <h2>SAIBA MAIS</h2>
                 </div>
               </div>

                </div>


                <div class="caption">
                  <h1>Piscinas de Azulejo e Pastilhas</h1>
                  <h3><i class="fa fa-star"></i>EM PISCINAS</h3>
                  <h3><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
                </div>
            </div>
          </div>

          
           <!-- item 01 -->
            <div class="col-xs-3 top30 bottom60">
             <div class="thumbnail produtos-home">

                <div class="view view-second">
                 <a href="<?php echo Util::caminho_projeto() ?>/produto">
                  <img src="<?php echo Util::caminho_projeto() ?>/imgs/produtos-home.jpg" alt="">
                </a>

                <div class="mask"></div>

                <div class="content">
                  <div class="col-xs-6">
                        <a href="<?php echo Util::caminho_projeto() ?>/orcamento" class="info"><i class="fa fa-plus-circle fa-4x"></i></a>
                        <h2>SAIBA MAIS</h2>
                  </div>
                  <div class="col-xs-6">
                        <a href="<?php echo Util::caminho_projeto() ?>/orcamento" class="info"><i class="fa fa-plus-circle fa-4x"></i></a>
                        <h2>SAIBA MAIS</h2>
                 </div>
               </div>

                </div>


                <div class="caption">
                  <h1>Piscinas de Azulejo e Pastilhas</h1>
                  <h3><i class="fa fa-star"></i>EM PISCINAS</h3>
                  <h3><i class="fa fa-star"></i>MARCAS VARIADAS</h3>
                </div>
            </div>
          </div>

    </div>
  </div>
</div>
  <!-- veja tambem -->


<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>



<?php echo Util::caminho_projeto() ?>