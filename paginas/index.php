<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>
        <!--    ==============================================================  -->
        <!--    ROYAL SLIDER    -->
        <!--    ==============================================================  -->

        <!-- slider JS files -->
        <script src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery-1.8.0.min.js"></script>
        <link href="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/royalslider.css" rel="stylesheet">
        <script src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery.royalslider.min.js"></script>
        <script>
            jQuery(document).ready(function($) {
                // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
                // it's recommended to disable them when using autoHeight module
                $('#content-slider-1').royalSlider({
                    autoHeight: true,
                    arrowsNav: true,
                    fadeinLoadedSlide: false,
                    controlNavigationSpacing: 0,
                    controlNavigation: 'bullets',
                    imageScaleMode: 'none',
                    imageAlignCenter: false,
                    loop: true,
                    loopRewind: true,
                    numImagesToPreload: 6,
                    keyboardNavEnabled: true,
                    usePreloader: false,
                    autoPlay: {
                        // autoplay options go gere
                        enabled: true,
                        pauseOnHover: true
                    }

                });
            });
        </script>


        <!-- FlexSlider -->
        <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
        <!-- Syntax Highlighter -->
        <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
        <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
        <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

        <!-- Optional FlexSlider Additions -->
        <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
        <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
        <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


        <!-- Syntax Highlighter -->
        <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


        <!-- Demo CSS -->
        <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />


        <script>
            $(window).load(function() {
                // The slider being synced must be initialized first
                $('#carousel').flexslider({
                    animation: "slide",
                    controlNav: false,
                    animationLoop: false,
                    slideshow: false,
                    itemWidth: 170,
                    itemMargin: 40,
                });

                $('#slider').flexslider({
                    animation: "slide",
                    controlNav: false,
                    animationLoop: false,
                    slideshow: false,
                    sync: "#carousel"
                });
            });
        </script>



</head>



<body>
    <!-- ==============================================================  -->
    <!--  BANNERS   -->
    <div class="container-fluid">
        <div class="row">
            <div id="container_banner">
                <div id="content_slider">
                    <div id="content-slider-1" class="contentSlider rsDefault">
                        <!-- ITEM -->
                        <?php
                       $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY ordem LIMIT 5");
                       if (mysql_num_rows($result) > 0) {
                           $i = 0;
                           while($row = mysql_fetch_array($result)){

                            if(empty($row[url])){
                            ?>
                            <div>
                                <img class="rsImg" src="./uploads/<?php echo $row[imagem] ?>" />
                            </div>
                            <?php
                            }else{
                            ?>
                                <div>

                                    <img class="rsImg" src="./uploads/<?php echo $row[imagem] ?>"  />


                                    <!--	botao saiba mais home-->
                                    <div class="text-center">
                                        <a href="<?php Util::imprime($row[url]) ?>">
                                            <button type="button" class="btn btn-primary  descer-saiba-mais">Saiba Mais</button>
                                        </a>
                                    </div>
                                    <!--	botao saiba mais home-->
                                </div>
                                <?php
                            }
                           }
                       }
                       ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--  BANNERS   -->
    <!--  ==============================================================  -->




    <!--  ==============================================================  -->
    <!-- conteudo -->
    <div class="pagina-full">

        <!-- topo -->
        <?php require_once('./includes/topo.php') ?>
            <!-- topo -->
    </div>
    <!-- conteudo -->
    <!--  ==============================================================  -->


    <!-- categorias home -->
    <div class="container ">
        <div class="row">


            <div class="categorias-home">
                <div id="carousel" class="flexslider">

                    <h2 class="bottom20">NOSSAS CATEGORIAS:</h2>

                    <!-- FlexSlider -->
                    <ul class="slides slider-prod-tumb ">
                        <?php
                        $result = $obj_site->select("tb_categorias_produtos", "ORDER BY ordem LIMIT 15");
                        if (mysql_num_rows($result) > 0) {
                            while($row = mysql_fetch_array($result)){
                            ?>
                            <li>
                                <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="" >
                                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" alt="" class="img-rounded">
                                    <h1><?php Util::imprime($row[titulo]) ?></h1>
                                </a>
                            </li>
                            <?php
                            }
                        }
                        ?>
                    </ul>
                    <!-- FlexSlider -->
                </div>
            </div>


        </div>
    </div>
    <!-- categorias home -->






    <!-- produtos home -->
    <div class="container">
        <div class="row">
            <div class="col-xs-3 produtos-home bottom20">
                <h2>DESTAQUES</h2>
            </div>
        </div>

        <div class="row">





            <!-- item 01 -->
            <?php
            $result = $obj_site->select("tb_produtos", "order by rand() limit 8");
            if(mysql_num_rows($result) > 0)
            {
                while($row = mysql_fetch_array($result)){
                ?>
                   <div class="col-xs-3">
                    <div class="thumbnail produtos-home ">

                       <div class="">

                         <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">


                       <div class="produto-hover">
                         <div class="col-xs-6 text-center">
                               <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>" class="info">
                                   <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-saiba-mais.jpg" alt="" />
                               </a>
                         </div>
                         <div class="col-xs-6 hover-btn-orcamento">
                             <a href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                                 <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-adicionar-orcamento.jpg" alt="" />
                             </a>
                         </div>

                      </div>

                       </div>


                       <div class="caption">
                         <h1><?php Util::imprime($row[titulo]) ?></h1>
                         <h3><i class="fa fa-star"></i>EM <?php Util::imprime( Util::troca_value_nome($row[id_categoriaproduto], "tb_categorias_produtos", "idcategoriaproduto", "titulo")) ?></h3>
                         <h3><i class="fa fa-star"></i><?php Util::imprime($row[marca]) ?></h3>
                       </div>
                   </div>
                 </div>
                <?php
                }
            }
            ?>





        </div>
    </div>
    <!-- produtos home -->

    <!-- servicos home -->
    <div class="container-fluid bg-servicos-home">
        <div class="row">
            <div class="container">
                <div class="row top30">
                    <div class="col-xs-7 servicos-home">
                        <h2>CONHEÇA UM POUCO MAIS A BSB PISCINAS</h2>
                    </div>
                </div>
                <div class="row top20">
                    <div class="col-xs-12 servicos-home">
                        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
                        <p><?php Util::imprime($dados[descricao]) ?></p>
                    </div>
                </div>
                <div class="row top30">
                    <div class="col-xs-4 servicos-home">
                        <h1>NOSSOS SERVIÇOS</h1>
                    </div>
                </div>
                <!-- saiba mais home-->
                <div class="row">

                    <?php
                    $result = $obj_site->select("tb_servicos", "order by rand() limit 2");
                    if(mysql_num_rows($result) > 0)
                    {
                        while($row = mysql_fetch_array($result)){
                        ?>
                        <div class="col-xs-12 top30 bottom10">
                            <div class="saiba-mais-servicos-home">
                                <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]) ?>">
                                    <h3>
                                        <i class="fa fa-star"></i>
                                        <?php Util::imprime($row[titulo]) ?>
                                        <i class="fa fa-plus-square"></i>
                                    </h3>
                                </a>
                            </div>
                        </div>
                        <?php
                        }
                    }
                    ?>




                </div>

                <!-- saiba mais home-->
            </div>
        </div>
    </div>
    <!-- servicos home -->

    <!-- dicas home -->
    <div class="container">
        <div class="row bottom40">
            <div class="col-xs-4 dicas-home">
                <h1>NOSSAS DICAS</h1>
            </div>
        </div>



        <!-- dicas 01 -->
        <div class="row bottom40">

            <?php
            $result = $obj_site->select("tb_dicas", "order by rand() limit 2");
            if(mysql_num_rows($result) > 0)
            {
                while($row = mysql_fetch_array($result)){
                ?>
            <div class="col-xs-6 borda-azul pbottom40">
                <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]) ?>">
                    <div class="col-xs-4 dicas-home1">
                         <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="img-circle">
                    </div>

                    <div class=" col-xs-8 dicas-home1">
                        <h1><?php Util::imprime($row[titulo]) ?></h1>
                        <h2><?php Util::imprime($row[descricao], 500) ?></h2>
                    </div>
                </a>
            </div>
            <?php
                }
            }
            ?>


        </div>
    </div>
    <!--dicas home -->

    <!-- rodape -->
    <?php require_once('./includes/rodape.php') ?>
        <!-- rodape -->





</body>

</html>
