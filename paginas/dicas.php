
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>
    <!-- ---- LAYER SLIDER ---- -->
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
    <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#carousel-gallery").touchCarousel({
                itemsPerPage: 1,
                scrollbar: true,
                scrollbarAutoHide: true,
                scrollbarTheme: "dark",
                pagingNav: false,
                snapToItems: true,
                scrollToLast: false,
                useWebkit3d: true,
                loopItems: true
            });
        });
    </script>
    <!-- XXXX LAYER SLIDER XXXX -->




</head>
<body>

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->



    <!-- bg-dicas -->
    <div class="container-fluir">
        <div class="row">
            <div class="bg-dicas"></div>
        </div>
    </div>
    <!-- bg-dicas -->

    <!-- descricao-dicas -->
    <div class="container">
        <div class="row pbottom40">
            <div class="col-xs-3 top50 bottom10">
                <div class="descricao-dicas">
                    <h1>BSB DICAS</h1>
                </div>
            </div>
            <!-- barra de pesquisa -->
            <div class="col-xs-9 top30 bottom10">
                <div class="topo-pesquisa1">
                    <div class="col-xs-4">
                      <h1 class="top20">PESQUISAR DICAS:</h1>
                  </div>
              </div>
              <!-- barra pesquisa -->

              <form class="" action="<?php echo Util::caminho_projeto() ?>/dicas/" method="post">
                  <div class="col-xs-8 top30">
                    <div class="input-group ">
                        <input type="text" name="busca_dica" class="form-control barra-pesquisa-topo " placeholder="ENCONTRE A DICA QUE VOCÊ PRECISA">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                  </div>
              </form>



    </div>
    <!-- barra de pesquisa -->
</div>


<div class="row bottom40">



<!-- dicas 01 -->
<?php
//  FILTRA PELO TITULO
if(isset($_POST[busca_dica])):
  $complemento = "AND titulo LIKE '%$_POST[busca_dica]%'";
endif;

$result = $obj_site->select("tb_dicas", $complemento);
if(mysql_num_rows($result) == 0)
{
  echo "<h2 class='bg-info' style='padding: 20px; margin: 20px;'>Nenhuma dica encontrada.</h2>";
}else{
    while($row = mysql_fetch_array($result)){
    ?>
     <div class="col-xs-6 borda-azul pbottom40 top50">
         <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]) ?>">
           <div class="col-xs-4 dicas-home1">
               <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="img-circle">
           </div>

            <div class=" col-xs-8 dicas-home1">
              <h1><?php Util::imprime($row[titulo]) ?></h1>
              <h2><?php Util::imprime($row[descricao]) ?></h2>
            </div>
        </a>
    </div>
<?php
}
}
?>






</div>


</div>
<!-- descricao-dicas -->





<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>
