
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>
    <!-- ---- LAYER SLIDER ---- -->
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
    <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#carousel-gallery").touchCarousel({
                itemsPerPage: 1,
                scrollbar: true,
                scrollbarAutoHide: true,
                scrollbarTheme: "dark",
                pagingNav: false,
                snapToItems: true,
                scrollToLast: false,
                useWebkit3d: true,
                loopItems: true
            });
        });
    </script>
    <!-- XXXX LAYER SLIDER XXXX -->






</head>
<body>

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->



    <!-- bg-empresa -->
    <div class="container-fluir">
        <div class="row">
            <div class="bg-produtos"></div>
        </div>
    </div>
    <!-- bg-empresa -->



    <!-- descricao-empresa -->
    <div class="container">
        <div class="row">
            <div class="col-xs-3 top40 bottom10">
                <div class="descricao-empresa">
                    <h3>PRODUTOS</h3>
                </div>
            </div>

            <div class="col-xs-9 top20">

              <div class="topo-pesquisa">
                <div class="col-xs-4">
                  <h1 class="top20">PESQUISAR PRODUTOS:</h1>
                </div>
              </div>



              <!-- barra pesquisa -->
              <form class="" action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
                  <div class="col-xs-4 top30">
                    <div class="input-group ">
                      <input type="text" class="form-control barra-pesquisa-topo " name="busca_topo" placeholder="Busca por nome">
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                      </span>
                    </div>
                </div>
            </form>



            <!-- menu topo selec -->
            <div class="col-xs-4 top28">
                <div class="dropdown ">
                  <button class="btn btn-default barra-pesquisa-topo1 dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    CATEGORIA
                    <span class="caret"></span>
                </button>
                    <ul class="dropdown-menu input100" aria-labelledby="dropdownMenu1">
                        <?php
                         $result = $obj_site->select("tb_categorias_produtos");
                         if (mysql_num_rows($result) > 0) {
                             while($row = mysql_fetch_array($result)){
                             ?>
                             <li><a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]) ?>"><?php Util::imprime($row[titulo]) ?></a></li>
                             <?php
                             }
                         }
                         ?>
                    </ul>
                </div>
            </div>



         </div>

    <!-- caroucel produtos -->
    <?php $result = $obj_site->select("tb_banners", "AND tipo_banner = 2 ORDER BY ordem LIMIT 5"); ?>
    <div class="text-center col-xs-12">
        <div id="carousel-example-generic" class="carousel slide " data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner controle-descricao" role="listbox">

                <?php
                 
                 if (mysql_num_rows($result) > 0) {
                     $i = 0;
                     while($row = mysql_fetch_array($result)){
                         if ($i == 0) {
                             $active = 'active';
                         }
                         ++$i;
                      ?>
                      <div class="item <?php echo $active ?>">
                        <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>"alt="">
                        <?php if (!empty($row[url])) { ?>
                            <div class="carousel-caption pbottom60">
                                <h5></h5>
                                <a target="_blank" href="<?php Util::imprime($row[url]) ?>" class="btn btn-primary btn-saiba-produtos top20" role="button">SAIBA MAIS</a>
                            </div>
                        <?php } ?>


                    </div>
                      <?php
                      }
                 
                  ?>
            </div>

            <!-- Controls -->
            <a class="left carousel-control controle" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control controle" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            </div>
    </div>
    <?php } ?>
    <!-- caroucel produtos -->
  </div>
</div>
<!-- descricao-empresa -->




<div class="container">
    <div class="row top30">
            <!-- menu produto -->
        <div class="col-xs-3">
            <ul class="nav nav-pills nav-stacked lista-categorias">

                <?php
                 $result = $obj_site->select("tb_categorias_produtos");
                 if (mysql_num_rows($result) > 0) {
                     while($row = mysql_fetch_array($result)){
                     ?>
                     <li role="presentation">
                         <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]) ?>">
                             <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" alt="" class="pull-left">
                             <span><?php Util::imprime($row[titulo]) ?></span>
                         </a>
                     </li>
                     <?php
                     }
                  }
                  ?>
            </ul>
        </div>



           <div class="col-xs-9">
                <!-- produtos geral -->



             <!-- item 01 -->
             <?php
             $url1 = Url::getURL(1);

             //  FILTRA AS CATEGORIAS
             if (isset( $url1 )) {
                $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
                $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
             }

             //  FILTRA PELO TITULO
             if(isset($_POST[busca_topo])):
               $complemento = "AND titulo LIKE '%$_POST[busca_topo]%'";
             endif;

             $result = $obj_site->select("tb_produtos", $complemento);
             if(mysql_num_rows($result) == 0)
             {
               echo "<h2 class='bg-info' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
             }else{
                 while($row = mysql_fetch_array($result)){
                 ?>
                    <div class="col-xs-4">
                     <div class="thumbnail produtos-home ">

                        <div class="">

                          <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">


                        <div class="produto-hover">
                          <div class="col-xs-6 text-center">
                                <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>" class="info">
                                    <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-saiba-mais.jpg" alt="" />
                                </a>
                          </div>
                          <div class="col-xs-6 hover-btn-orcamento">
                              <a href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                                  <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-adicionar-orcamento.jpg" alt="" />
                              </a>
                          </div>

                       </div>

                        </div>


                        <div class="caption">
                          <h1><?php Util::imprime($row[titulo]) ?></h1>
                          <h3><i class="fa fa-star"></i>EM <?php Util::imprime( Util::troca_value_nome($row[id_categoriaproduto], "tb_categorias_produtos", "idcategoriaproduto", "titulo")) ?></h3>
                          <h3><i class="fa fa-star"></i><?php Util::imprime($row[marca]) ?></h3>
                        </div>
                    </div>
                  </div>
                 <?php
                 }
             }
             ?>


        <!-- produtos geral -->
           </div>


    <!-- menu produto -->

    </div>
</div>




<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>
