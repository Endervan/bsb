
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>
<!-- ---- LAYER SLIDER ---- -->
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
    <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $("#carousel-gallery").touchCarousel({
            itemsPerPage: 1,
            scrollbar: true,
            scrollbarAutoHide: true,
            scrollbarTheme: "dark",
            pagingNav: false,
            snapToItems: true,
            scrollToLast: false,
            useWebkit3d: true,
            loopItems: true
        });
    });
    </script>
    <!-- XXXX LAYER SLIDER XXXX -->




</head>
<body>

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->



   
    <!-- formulario de contatos -->
    <?php
    //  VERIFICO SE E PARA ENVIAR O EMAIL
    if(isset($_POST[nome]))
    {
      $nome_remetente = Util::trata_dados_formulario($_POST[nome]);
      $assunto = Util::trata_dados_formulario($_POST[assunto]);
      $email = Util::trata_dados_formulario($_POST[email]);
      $telefone = Util::trata_dados_formulario($_POST[telefone]);
      $mensagem = Util::trata_dados_formulario(nl2br($_POST[mensagem]));

      if(!empty($_FILES[curriculo][name])):
        $nome_arquivo = Util::upload_arquivo("../uploads", $_FILES[curriculo]);
        $texto = "Anexo: ";
        $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
        $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
      endif;

            $texto_mensagem = "
                              Nome: $nome_remetente <br />
                              Assunto: $assunto <br />
                              Telefone: $telefone <br />
                              Email: $email <br />
                              Mensagem: <br />
                              $texto    <br><br>
                              $mensagem
                              ";


            Util::envia_email($config[email], "CURRÍCULO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
            Util::envia_email($config[email_copia], "CURRÍCULO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
            Util::alert_bootstrap("Obrigado por entrar em contato.");
            unset($_POST);
    }
    ?>
    
    <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data">
            <div class="container">
              <div class="row top50 bottom25">
                <div class="col-xs-8 fale-conosco">
                  <h3>TRABALHE CONOSCO</h3>
                  <!-- formulario orcamento -->
                    <div class="FormContato top20 bottom80">
                        <div class="row">
                          <div class="col-xs-6 form-group ">
                          <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                          <input type="text" name="nome" class="form-control fundo-form1 input100" placeholder="">
                        </div>

                        <div class="col-xs-6 form-group ">
                          <label class="glyphicon glyphicon-user"> <span>E-mail</span></label>
                          <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="">
                        </div>
                        </div>

                        <div class="row">
                          <div class="col-xs-6 form-group">
                          <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                          <input type="text" name="telefone" class="form-control fundo-form1 input100" placeholder="">
                        </div>

                        <div class="col-xs-6 form-group">
                           <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
                          <input type="text" name="assunto" class="form-control fundo-form1 input100" placeholder="">
                        </div>
                        </div>


                          <div class="row">
                            <div class="col-xs-6 form-group">
                            <label class="glyphicon glyphicon-file"> <span>Currículo</span></label>
                            <input type="file" name="curriculo" class="form-control fundo-form1 input100" placeholder="">
                          </div>


                          <div class="col-xs-6 form-group">
                            <label class="glyphicon glyphicon-book"> <span>Escolaridade</span></label>
                            <input type="text" name="escolaridade" class="form-control fundo-form1 input100" placeholder="">
                          </div>
                          </div>

                          <div class="row">
                            <div class="col-xs-6 form-group">
                            <label class="glyphicon glyphicon-lock"> <span>Cargo</span></label>
                            <input type="text" name="cargo" class="form-control fundo-form1 input100" placeholder="">
                          </div>

                          <div class="col-xs-6 form-group">
                            <label class="glyphicon glyphicon-briefcase"> <span>Area</span></label>
                            <input type="text" name="area" class="form-control fundo-form1 input100" placeholder="">
                          </div>
                          </div>

                        <div class="row">
                          <div class="col-xs-12 form-group">
                          <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                          <textarea name="mensagem" id="" cols="30" rows="8" class="form-control  fundo-form1 input100" placeholder=""></textarea>
                        </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="text-right  top30">
                          <button type="submit" class="btn btn-zul" name="btn_contato">
                            ENVIAR
                          </button>
                        </div>
                  </div>
                    <!-- formulario orcamento -->

                </div>

                <div class="col-xs-4 fale-conosco1">
                  <h3>ATENDIMENTO</h3>
                  <h2><?php Util::imprime($config[telefone1]) ?></h2>
                  <h2><?php if(!empty($config[telefone2])){ echo($config[telefone2]); } ?></h2>
                  <h2><?php if(!empty($config[telefone3])){ echo($config[telefone3]); } ?></h2>
                  <h2><?php if(!empty($config[telefone4])){ echo($config[telefone4]); } ?></h2>
              </div>
            </div>
        </div>
    </form>
    <!-- formulario de contatos -->



     <!-- bg-orcamento -->
    <div class="container-fluir">
        <div class="row">
            <div class="faleConosco">
              <iframe src="<?php Util::imprime($config[src_place]) ?>" width="100%" height="574" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    <!-- bg-orcamento -->




<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>


<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
});
</script>
