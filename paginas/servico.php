<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
   $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_servicos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/servicos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>
    <!-- ---- LAYER SLIDER ---- -->
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
    <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#carousel-gallery").touchCarousel({
                itemsPerPage: 1,
                scrollbar: true,
                scrollbarAutoHide: true,
                scrollbarTheme: "dark",
                pagingNav: false,
                snapToItems: true,
                scrollToLast: false,
                useWebkit3d: true,
                loopItems: true
            });
        });
    </script>
    <!-- XXXX LAYER SLIDER XXXX -->




</head>
<body>

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->



    <!-- bg-dicas -->
    <div class="container-fluir">
        <div class="row">
            <div class="bg-servicos"></div>
        </div>
    </div>
    <!-- bg-dicas -->

    <!-- descricao-dicas -->
    <div class="container">
        <div class="row pbottom40">
            <div class="col-xs-4 top50 bottom10">
                <div class="descricao-dicas">
                    <h1>BSB SERVIÇOS</h1>
                </div>
            </div>


    </div>
   </div>

  <!-- descricao dicas internas -->
  <div class="container">
    <div class="row top50">
      <div class="col-xs-8">
        <h1 class="top10"><?php Util::imprime($dados_dentro[titulo]) ?></h1>

        <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dados_dentro[imagem]) ?>" class="input100 top20" alt="">

        <p class="top30 pbottom20"><?php Util::imprime($dados_dentro[descricao]) ?></p>

         <!-- comentario dicas -->
            <div class="col-xs-5 comentario">
              <h4>COMENTÁRIOS</h4>
            </div>




            <!-- Modal HTML -->
            <?php
            if (isset($_POST[comentario])) {
              $obj_site->insert("tb_comentarios_servicos");
              Util::alert_bootstrap("Muito obrigado pelo seu comentário.");
            }
            ?>

            <div id="myModal1" class="modal fade" style="z-index=9999">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <form class="form-comentario" action="" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Deixa seu comentário.</h4>
                            </div>
                            <div class="modal-body">
                                  <div class="col-xs-6 form-group ">
                                    <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                                    <input type="text" name="nome" class="form-control fundo-form input100" placeholder="">
                                  </div>
                                  <div class="col-xs-6 form-group">
                                    <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                                    <input type="text" name="email" class="form-control fundo-form input100" placeholder="">
                                  </div>
                                  <div class="col-xs-12 form-group">
                                    <label class="glyphicon glyphicon-envelope"> <span>Comentário</span></label>
                                    <textarea name="comentario" id="" cols="30" rows="10" class="form-control  fundo-form input100" placeholder=""></textarea>
                                  </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Enviar</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                            </div>

                            <input type="hidden" name="id_servico" value="<?php echo $dados_dentro[0] ?>">
                            <input type="hidden" name="ativo" value="NAO">
                            <input type="hidden" name="data" value="<?php echo date("d/m/Y") ?>">

                        </form>

                    </div>
                </div>
            </div>

            <script type="text/javascript">
            $(function(){
                $("#btn-comentario").click(function(){
                    $('#myModal1').modal({
        			    backdrop: 'static',
        			    keyboard: false  // to prevent closing with Esc button (if you want this too)
		             });
                });
            });


            </script>













            <!-- barra de pesquisa -->
            <div class="col-xs-7">
              <div class="topo-pesquisa11 pull-right left10">
                <a href="javascript:void(0);" id="btn-comentario">
                  <h1 class="top20">DEIXE SEU COMENTÁRIO</h1>
                </a>
            </div>
            <!-- barra pesquisa -->
          </div>


        <!-- depoimentos -->
        <?php
        $result = $obj_site->select("tb_comentarios_servicos", "AND id_servico = $dados_dentro[0] ");
        if(mysql_num_rows($result) == 0)
        {
          echo "<div class='clearfix'></div><h1 style='padding: 50px;'>Nenhum comentário. Seja o primeiro a comentar.</h1>";
        }else{
            while($row = mysql_fetch_array($result)){
            ?>
            <div class="col-xs-12 descricao-comentario top20 bottom20">
              <h1 class="top25"><?php Util::imprime($row[nome]) ?></h1>
              <p class="top20 pbottom30"><?php Util::imprime($row[comentario]) ?></p>
            </div>
            <?php
            }
        }
        ?>

        <!-- depoimentos -->
      </div>
      <!-- comentario dicas -->








      <!-- ultimas noticias -->

      <div class="col-xs-4 ultimas-notocias">
        <h1>AS MAIS ACESSADAS</h1>
      </div>

            <!-- item 01 -->

            <?php
            $result = $obj_site->select("tb_servicos", "order by rand() limit 4");
            if(mysql_num_rows($result) > 0)
            {
                while($row = mysql_fetch_array($result)){
                ?>
                <div class="col-xs-4 ultimas-notocias">
                    <div class="top20 ">
                      <div class="col-xs-4">
                      <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]) ?>">
                        <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="img-circle input100">
                      </a>
                    </div>
                    <div class="col-xs-8">
                        <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]) ?>">
                          <h2><?php Util::imprime($row[titulo]) ?></h2>
                          <h3 class="top5"><?php Util::imprime($row[descricao], 200) ?></h3>
                        </a>
                    </div>
                    </div>
                </div>
                <?php
                }
            }
            ?>

      <!-- ultimas noticias -->
  </div>
</div>
  <!-- descricao dicas internas -->




<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>




<script>
    $(document).ready(function() {
        $('.form-comentario').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 nome: {
                    validators: {
                        notEmpty: {

                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {

                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                comentario: {
                    validators: {
                        notEmpty: {

                        }
                    }
                }
            }
        });
    });
</script>
