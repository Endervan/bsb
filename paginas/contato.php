
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('./includes/head.php'); ?>





</head>
<body class="bg-empresa">

  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->


  <!-- barra-internas-->
  <div class="container sombra-barra-internas ">
    <div class="row">
      <div class="container">
        <div class="row">
          <div class="col-xs-4 barra-interna text-center">
            <ol class="breadcrumb ">
              <li><span >você esta em:</span></li>
              <li><a href="<?php echo Util::caminho_projeto() ?>">Home<i class="fa fa-angle-right"></i></a></li>
              <li class="active">Contatos</li>
            </ol>
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra-descricao-internas.png" alt="">
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- barra-internas-->




  <!-- contatos -->
  <div class="container ">
    <div class="row bottom20 ">
      <div class=" col-xs-4 col-xs-offset-8 contatos-descricao text-right">
        <h2>CONTATOS</h2>
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra-descricao-internas.png" alt="">

        <p>Lorem Ipsum is simply dummy text of the printing and type
          setting industry. Lorem Ipsum has been the industry's stan
          dard dummy text ever since the 1500s

        </p>
        <h3>(61) 3302-4517</h3>

      </div>
      <div class="col-xs-4 col-xs-offset-8 descer menu-empresa">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-stacked text-center menu-contatos" role="tablist">
          <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">FALE CONOSCO</a></li>
          <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">TRABALHE CONOSCO</a></li>
          <li role="presentation"><a href="" aria-controls="messages" role="tab" data-toggle="tab">COMO CHEGAR</a></li>
        </ul>
      </div>

      <div class="col-xs-8">
        <!-- Tab panes -->
        <div class="tab-content posicao-tab">

          <!-- fale conosco -->
          <div role="tabpanel" class="tab-pane fade in active" id="home">
            <?php
                            //  VERIFICO SE E PARA ENVIAR O EMAIL
            if(isset($_POST[btn_contato]))
            {
              $nome_remetente = Util::trata_dados_formulario($_POST[nome]);
              $email = Util::trata_dados_formulario($_POST[email]);
              $assunto = Util::trata_dados_formulario($_POST[assunto]);
              $telefone = Util::trata_dados_formulario($_POST[telefone]);
              $mensagem = Util::trata_dados_formulario(nl2br($_POST[mensagem]));
              $texto_mensagem = "
              Nome: $nome_remetente <br />
              Assunto: $assunto <br />
              Telefone: $telefone <br />
              Email: $email <br />
              Mensagem: <br />
              $mensagem
              ";
              Util::envia_email($config[email], "CONTATO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
              Util::envia_email($config[email_copia], "CONTATO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
              Util::alert_bootstrap("Obrigado por entrar em contato.");
              unset($_POST);
            }
            ?>

            <form class="form-inline FormContato" role="form" method="post">

              <div class="row">
                <div class="col-xs-6 form-group ">
                  <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                  <input type="text" name="nome" class="form-control fundo-form input100" placeholder="">
                </div>
                <div class="col-xs-6 form-group">
                  <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                  <input type="text" name="email" class="form-control fundo-form input100" placeholder="">
                </div>
              </div>

              <div class="row">
                <div class="col-xs-6 top20 form-group">
                  <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                  <input type="text" name="telefone" class="form-control fundo-form input100" placeholder="">
                </div>
                <div class="col-xs-6 top20 form-group">
                  <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
                  <input type="text" name="assunto" class="form-control fundo-form input100" placeholder="">
                </div>

              </div>

              <div class="row">
                <div class="col-xs-12 top20 form-group">
                  <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                  <textarea name="mensagem" id="" cols="30" rows="10" class="form-control  fundo-form input100" placeholder=""></textarea>
                </div>

              </div>

              <div class="clearfix"></div>

              <div class="text-right right15 top30">
                <button type="submit" class="btn btn-cinza-contatos" name="btn_contato">
                  ENVIAR
                </button>
              </div>


            </form>

          </div>

          <!-- fale conosco -->





        </div>
        <!-- Tab panes -->
      </div>
      <!-- posicao-sombra-contatos -->

      <div class="col-xs-12">
        <div class="posicao-sombra-home1 text-right">
        </div>
      </div>

      <div class="posicao-sombra-home11 text-right">
        <a href="">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/como-chegar.png" alt="">
        </a>
      </div>

      <!-- posicao-sombra-contatos -->




</div>
</div>

<div class="container-fluir mapa-lg">
  <div class="row">
    <!-- mapa-geral -->
<div class=" text-center">

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3837.6217181778134!2d-48.01083799999997!3d-15.876467000000005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a326fb9b303ed%3A0x6fd1705b0f2fad84!2sAlkha!5e0!3m2!1spt-BR!2sbr!4v1442685372964" width="1400" height="800" frameborder="0" style="border:0" allowfullscreen></iframe></div>
<!-- mapa-geral -->

  </div>
</div>
<!-- contatos -->


<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>




<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>



<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
});
</script>
