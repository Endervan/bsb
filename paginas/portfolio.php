<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
   $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_portifolios", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/portfolios");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>
    <!-- ---- LAYER SLIDER ---- -->
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
    <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#carousel-gallery").touchCarousel({
                itemsPerPage: 1,
                scrollbar: true,
                scrollbarAutoHide: true,
                scrollbarTheme: "dark",
                pagingNav: false,
                snapToItems: true,
                scrollToLast: false,
                useWebkit3d: true,
                loopItems: true
            });
        });
    </script>
    <!-- XXXX LAYER SLIDER XXXX -->




</head>
<body>

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->



    <!-- bg-dicas -->
    <div class="container-fluir">
        <div class="row">
            <div class="bg-portfolios"></div>
        </div>
    </div>
    <!-- bg-dicas -->

    <!-- descricao-dicas -->
    <div class="container">
        <div class="row pbottom40">
            <div class="col-xs-4 top50 bottom10">
                <div class="descricao-dicas">
                    <h1>BSB PORTFÓLIO</h1>
                </div>
            </div>

    </div>

   </div>


   <div class="container top15">
       <div class="row">
           <div class="col-xs-12">
               <a href="<?php echo Util::caminho_projeto() ?>/portfolios" class="btn btn-primary">VOLTAR</a>
           </div>
       </div>
   </div>


  <!-- descricao dicas internas -->
  <div class="container">
    <div class="row top50">
      <div class="col-xs-6 top15">


          <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                  <?php
                   $result = $obj_site->select("tb_galerias_portifolios", "and id_portifolio = $dados_dentro[0]");
                   if (mysql_num_rows($result) > 0) {
                       $i = 0;
                       while($row = mysql_fetch_array($result)){
                           $imagens[] = $row;
                           if($i == 0){
                               $active = 'active';
                           }else{
                               $active = '';
                           }


                           ?>
                            <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php echo $active; ?>"></li>
                            <?php
                            ++$i;
                        }
                    }
                    ?>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">

                <?php
                if (count($imagens) > 0) {
                    $i = 0;
                    foreach ($imagens as $key => $imagem) {
                        if($i == 0){
                            $active = 'active';
                        }else{
                            $active = '';
                        }
                        ?>
                        <div class="item <?php echo $active ?>">
                          <?php $obj_site->redimensiona_imagem("../uploads/$imagem[imagem]", 557, 420) ?>
                          <div class="carousel-caption">
                            ...
                          </div>
                        </div>
                        <?php
                        ++$i;
                    }
                }
                ?>
              </div>

              <!-- Controls -->
              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>





      </div>

      <div class="col-xs-6">
          <h1 class="top10"><?php Util::imprime($dados_dentro[titulo]) ?></h1>
          <p class="top30 pbottom20"><?php Util::imprime($dados_dentro[descricao]) ?></p>
      </div>








      <!-- ultimas noticias -->




      <!-- ultimas noticias -->
  </div>
</div>
  <!-- descricao dicas internas -->


<div class="top50"></div>

<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>




<script>
    $(document).ready(function() {
        $('.form-comentario').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 nome: {
                    validators: {
                        notEmpty: {

                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {

                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                comentario: {
                    validators: {
                        notEmpty: {

                        }
                    }
                }
            }
        });
    });
</script>
