
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>
<!-- ---- LAYER SLIDER ---- -->
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
    <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $("#carousel-gallery").touchCarousel({
            itemsPerPage: 1,
            scrollbar: true,
            scrollbarAutoHide: true,
            scrollbarTheme: "dark",
            pagingNav: false,
            snapToItems: true,
            scrollToLast: false,
            useWebkit3d: true,
            loopItems: true
        });
    });
    </script>
    <!-- XXXX LAYER SLIDER XXXX -->




</head>
<body>

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->



    <!-- bg-empresa -->
    <div class="container-fluir">
        <div class="row">
            <div class="bg-servicos"></div>
        </div>
    </div>
    <!-- bg-empresa -->

    <!-- descricao-empresa -->
    <div class="container">
        <div class="row">

            <?php
             $result = $obj_site->select("tb_servicos");
             if (mysql_num_rows($result) > 0) {
                 $i = 0;
                 while($row = mysql_fetch_array($result)){
                     if ($i == 0) {
                         ?>
                         <div class="row">
                             <div class="col-xs-7 top70 bottom10">
                                 <div class="descricao-servicos">

                                     <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]) ?>">
                                         <h4><?php Util::imprime($row[titulo]) ?></h4>
                                         <div class="descricao-servico-desc">
                                             <p class="bottom20"><?php Util::imprime($row[descricao], 1000) ?></p>
                                         </div>
                                     </a>

                                     <a href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'servico'">
                                      <img src="<?php echo Util::caminho_projeto() ?>/imgs/botao-carrinho-servicos.jpg" alt="">
                                    </a>
                                 </div>
                             </div>

                             <div class="col-xs-5 top105">
                                 <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]) ?>">
                                     <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" class="input100"alt="">
                                 </a>
                             </div>
                         </div>
                         <?php
                         ++$i;
                     }else{
                         ?>
                         <div class="row">
                             <div class="col-xs-5 top135">
                                 <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]) ?>">
                                     <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" class="input100"alt="">
                                 </a>
                             </div>

                             <div class="col-xs-7 top100 bottom10">
                                 <div class="descricao-servicos">

                                     <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]) ?>">
                                         <h4><?php Util::imprime($row[titulo]) ?></h4>
                                         <div class="descricao-servico-desc">
                                             <p class="bottom20"><?php Util::imprime($row[descricao], 1000) ?></p>
                                         </div>
                                     </a>

                                     <a href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'servico'">
                                      <img src="<?php echo Util::caminho_projeto() ?>/imgs/botao-carrinho-servicos.jpg" alt="">
                                    </a>
                                 </div>
                             </div>
                         </div>



                         <?php
                         $i = 0;
                     }
                 ?>

                 <?php
                 }
             }
             ?>


        </div>
  </div>
    <!-- descricao-empresa -->






     <!-- portifolio -servicos -->
<div class="container-fluir fundo-azul top40 pbottom20 bottom80">
    <div class="row">
        <div class="container">
            <div class="row ">
                <div class="col-xs-5 top30 bottom20">
                    <div class="nosso-portifolio-empresa">
                        <h1>NOSSO PORTIFÓLIO</h1>
                    </div>
                </div>
            </div>
            <!-- banner-portifolio -->
            <div class="row pbottom40">
                <?php
                 $result = $obj_site->select("tb_portifolios", "ORDER BY rand() LIMIT 6");
                 if (mysql_num_rows($result) > 0) {
                     while($row = mysql_fetch_array($result)){
                      ?>
                        <div class="col-xs-4">
                            <a href="<?php echo Util::caminho_projeto() ?>/portfolio/<?php Util::imprime($row[url_amigavel]) ?>"  data-toggle="tooltip" data-placement="top" title="<?php Util::imprime($row[titulo]) ?>">
                                <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="">
                            </a>
                        </div>
                    <?php
                    }
                }
                ?>
            </div>
            <!-- banner-portifolio -->
        </div>
    </div>
    <div class="top60 row"></div>
</div>
<!-- portifolio -servicos -->




<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>



<?php echo Util::caminho_projeto() ?>
