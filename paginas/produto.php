<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
   $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>
    <!-- FlexSlider -->
<script defer src="http://masmidia.com.br/clientes/san-remo/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
<!-- Syntax Highlighter -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

<!-- Optional FlexSlider Additions -->
<script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
<script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
<script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


<!-- Syntax Highlighter -->
<link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


<!-- Demo CSS -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />


<script>
        $(window).load(function() {
          // The slider being synced must be initialized first
          $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 71,
            itemMargin: 5,
            asNavFor: '#slider'
          });

          $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel"
          });
        });
    </script>

    

<!-- adicionando ou diminuindo quantidade de produto -->
<script>
      $(document).ready(function() {
      $("#mais").click(function(){

      if($("#mudeValor").val() < 10){
      $('#mudeValor').val(parseInt($('#mudeValor').val())+1); return false;
      } else {
      $('#limite').html('limite maximo 10');
      return false;
      }

      });
      $("#menos").click(function(){
      $('#limite').html('');
      if($("#mudeValor").val()!=0){$('#mudeValor').val(parseInt($('#mudeValor').val())-1);} return false;
      });
      });
</script>



</head>
<body>

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->



    <!-- bg-empresa -->
    <div class="container-fluir">
        <div class="row">
            <div class="bg-produtos"></div>
        </div>
    </div>
    <!-- bg-empresa -->



    <!-- descricao-empresa -->
    <div class="container">
        <div class="row">
            <div class="col-xs-3 top40 bottom10">
                <div class="descricao-empresa">
                    <h3>PRODUTOS</h3>
                </div>
            </div>

            <div class="col-xs-9 top20">

              <div class="topo-pesquisa1">
                <div class="col-xs-3">
                  <h1 class="top20">PESQUISAR PRODUTOS:</h1>
                </div>
              </div>

              <!-- barra pesquisa -->
              <form class="" action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
                  <div class="col-xs-4 top30">
                    <div class="input-group ">
                      <input type="text" class="form-control barra-pesquisa-topo " name="busca_topo" placeholder="Busca por nome">
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                      </span>
                    </div>
                </div>
            </form>



            <!-- menu topo selec -->
            <div class="col-xs-4 top28">
                <div class="dropdown ">
                  <button class="btn btn-default barra-pesquisa-topo1 dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    CATEGORIA
                    <span class="caret"></span>
                </button>
                    <ul class="dropdown-menu input100" aria-labelledby="dropdownMenu1">
                        <?php
                         $result = $obj_site->select("tb_categorias_produtos");
                         if (mysql_num_rows($result) > 0) {
                             while($row = mysql_fetch_array($result)){
                             ?>
                             <li><a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]) ?>"><?php Util::imprime($row[titulo]) ?></a></li>
                             <?php
                             }
                         }
                         ?>
                    </ul>
                </div>
            </div>


         </div>
  </div>
</div>
<!-- descricao-empresa -->


<!-- descricao de produtos internos -->
<div class="container">
  <div class="row top40">
      <div class="col-xs-4 descricao-produtos">
        <!-- botao voltar -->
        <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn-primary btn-voltar" role="button"><i class="fa fa-angle-left"></i>VOLTAR</a>
        <!-- botao voltar -->
        <h6 class="top30 bottom10"><?php Util::imprime($dados_dentro[titulo]) ?></h6>
        <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
        <p><button type="button" class="btn btn-primary right10">MARCA:</button><?php Util::imprime($dados_dentro[marca]) ?></p>
        <p><button type="button" class="btn btn-primary right10">CATEGORIA:</button><?php Util::imprime( Util::troca_value_nome($dados_dentro[id_categoriaproduto], "tb_categorias_produtos", "idcategoriaproduto", "titulo")) ?></p>
        <p class="rating"><button type="button" class="btn  btn-primary pull-left">AVALIAÇÕES</button><span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span></p>
      </div>

      <!-- descricao do produto -->
    <div class="col-xs-4 slider-produtos">
        <!-- Place somewhere in the <body> of your page -->
        <div id="slider" class="flexslider">
          <ul class="slides slider-prod">

              <?php if(!empty($dados_dentro[imagem])): ?>
                  <li>
                      <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 370, 287) ?>
                  </li>
              <?php endif; ?>

              <?php
               $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = $dados_dentro[0] ");
               if (mysql_num_rows($result) > 0) {
                   while($row = mysql_fetch_array($result)){
                   ?>
                   <li>
                       <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 287) ?>
                   </li>
                   <?php
                   }
               }
               ?>
             <!-- items mirrored twice, total of 12 -->
          </ul>
        </div>


        <div class="slider-prod-dentro">
        <div id="carousel" class="flexslider">
          <ul class="slides slider-prod-tumb col-xs-3">

              <?php if(!empty($dados_dentro[imagem])): ?>
                  <li>
                      <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 68, 51) ?>
                  </li>
              <?php endif; ?>

              <?php
               $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = $dados_dentro[0] ");
               if (mysql_num_rows($result) > 0) {
                   while($row = mysql_fetch_array($result)){
                   ?>
                   <li>
                       <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 287) ?>
                   </li>
                   <?php
                   }
               }
               ?>
              <!-- items mirrored twice, total of 12 -->
          </ul>
        </div>
        </div>


 </div>

 <!-- atendimento -->
 <div class="col-xs-4 topo-telefone1">
   <h6>ATENDIMENTO EM</h6>
   <h2><span><?php Util::imprime($config[telefone1]) ?></span> <?php if(!empty($config[telefone2])){ echo("<span> | ".$config[telefone2]."</span>"); } ?></h2>

   <a href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
       <img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/btn-carrinho.png" alt="">
   </a>

   <!-- adicinar quantidade-->
   <?php /* ?>
   <div class="top25 text-center">
     <a id="mais" href="#">+</a>
    <input type="text" value="1" readonly="readonly" id="mudeValor">
    <a id="menos" href="#">-</a>
    <div id="limite"></div>
    <a href="">
     <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-carrinho.png" alt="">
   </a>
   </div>
   <?php */ ?>
   <!-- adicinar quantidade-->


 </div>
  <!-- atendimento -->
  </div>
</div>
<!-- descricao de produtos internos -->

  <!-- descricao de atentimento -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12 top60">
        <div class="descricao-empresa">
            <h3>DESCRIÇÃO</h3>
            <p><?php Util::imprime($dados_dentro[descricao]) ?></p>
        </div>
      </div>
    </div>
  </div>
  <!-- descricao de atentimento -->

  <!-- comentarios  -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12 top80">
        <div class="descricao-empresa11">
            <h3>COMENTÁRIOS</h3>


            <?php
            $result = $obj_site->select("tb_avaliacoes_produtos", "AND id_produto = '$dados_dentro[0]' ORDER BY data DESC");

            if(mysql_num_rows($result) > 0)
            {
                while($row = mysql_fetch_array($result))
                {
                ?>
                <div class="jumbotron top50">
                  <h6><?php Util::imprime($row[nome]) ?></h6>
                  <!-- avaliacao -->
                  <div class="descricao-produtos text-left">
                  <input id="avaliaca-1" class="avaliacao" type="number" value="<?php Util::imprime($row[nota]) ?>" />
                </div>
                <!-- avaliacao -->
                  <p><?php Util::imprime($row[comentario]) ?></p>
                </div>
                <?php
                }
            }else{
              echo "<h1 class='top50'>Nenhum comentário. Seja o primeiro a comentar.</h1>";
            }
            ?>



            <!-- form comentario -->
            <div class="top50">
                <?php
                if (isset($_POST[comentario])) {
                  $obj_site->insert("tb_avaliacoes_produtos");
                  Util::alert_bootstrap("Muito obrigado pelo seu comentário.");
                }
                ?>
                <form class="form-inline FormContato" role="form" method="post">

                      <div class="row">
                          <div class="col-xs-4 form-group">
                              <label class="glyphicon glyphicon-user"> Nome</label>
                              <input type="text" name="nome" class="form-control input100" placeholder="">
                          </div>
                          <div class="col-xs-4 form-group">
                              <label class="glyphicon glyphicon-envelope"> E-mail</label>
                              <input type="text" name="email" class="form-control input100" placeholder="">
                          </div>
                          <div class="col-xs-4 form-group">
                              <label class="fa fa-star"> Nota</label>
                              <select name="nota" id="nota" class="form-control input100">
                                  <option value="">Selecione</option>
                                  <option value="5">Excelente</option>
                                  <option value="4">Ótimo</option>
                                  <option value="3">Bom</option>
                                  <option value="2">Regular</option>
                                  <option value="1">Ruim</option>
                              </select>
                          </div>

                          <div class="col-xs-12 top20 form-group">
                              <label class="glyphicon glyphicon-pencil"> Comentário</label>
                              <textarea name="comentario" id="" cols="30" rows="10" class="form-control input100"></textarea>
                          </div>

                          <div class="clearfix"></div>

                          <input type="hidden" name="id_produto" value="<?php echo $dados_dentro[0] ?>">
                          <input type="hidden" name="ativo" value="NAO">
                          <input type="hidden" name="data" value="<?php echo date("d/m/Y") ?>">

                          <div class="text-right top30">
                              <button type="submit" class="btn btn-default">
                                  ENVIAR
                              </button>
                          </div>
                        </div>

                    </form>
            </div>
              <!-- form comentario -->

        </div>
      </div>
    </div>
  </div>
  <!-- comentarios  -->

  <!-- veja tambem -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
      <div class="descricao-empresa11 ">
            <h3>VEJA TAMBÉM</h3>
       </div>

       <div class="clearfix"></div>
    <div class="top40"></div>

        <?php
        $result = $obj_site->select("tb_produtos", "ORDER BY rand() LIMIT 4");
        if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
            ?>
               <div class="col-xs-3">
                <div class="thumbnail produtos-home ">

                   <div class="">

                     <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">


                   <div class="produto-hover">
                     <div class="col-xs-6 text-center">
                           <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>" class="info">
                               <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-saiba-mais.jpg" alt="" />
                           </a>
                     </div>
                     <div class="col-xs-6 hover-btn-orcamento">
                         <a href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                             <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-adicionar-orcamento.jpg" alt="" />
                         </a>
                     </div>

                  </div>

                   </div>


                   <div class="caption">
                     <h1><?php Util::imprime($row[titulo]) ?></h1>
                     <h3><i class="fa fa-star"></i>EM <?php Util::imprime( Util::troca_value_nome($row[id_categoriaproduto], "tb_categorias_produtos", "idcategoriaproduto", "titulo")) ?></h3>
                     <h3><i class="fa fa-star"></i><?php Util::imprime($row[marca]) ?></h3>
                   </div>
               </div>
             </div>
            <?php
            }
        }
        ?>

    </div>
  </div>
</div>
  <!-- veja tambem -->


<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>






<script>
    $(document).ready(function() {
        $('.FormContato').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 nome: {
                    validators: {
                        notEmpty: {

                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {

                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                nota: {
                    validators: {
                        notEmpty: {

                        }
                    }
                },
                comentario: {
                    validators: {
                        notEmpty: {

                        }
                    }
                },
                mensagem: {
                    validators: {
                        notEmpty: {

                        }
                    }
                }
            }
        });
    });
</script>
