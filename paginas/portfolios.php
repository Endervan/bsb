
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>
    <!-- ---- LAYER SLIDER ---- -->
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
    <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#carousel-gallery").touchCarousel({
                itemsPerPage: 1,
                scrollbar: true,
                scrollbarAutoHide: true,
                scrollbarTheme: "dark",
                pagingNav: false,
                snapToItems: true,
                scrollToLast: false,
                useWebkit3d: true,
                loopItems: true
            });
        });
    </script>
    <!-- XXXX LAYER SLIDER XXXX -->




</head>
<body>

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->



    <!-- bg-dicas -->
    <div class="container-fluir">
        <div class="row">
            <div class="bg-portfolios"></div>
        </div>
    </div>
    <!-- bg-dicas -->

    <!-- descricao-dicas -->
    <div class="container">
        <div class="row pbottom40">
            <div class="col-xs-4 top50 bottom10">
                <div class="descricao-dicas">
                    <h1>BSB PORTFÓLIO</h1>
                </div>
            </div>
            <!-- barra de pesquisa -->

    <!-- barra de pesquisa -->
</div>


<div class="row bottom40">


    <?php
     $result = $obj_site->select("tb_portifolios");
     if (mysql_num_rows($result) > 0) {
         while($row = mysql_fetch_array($result)){
          ?>
            <div class="col-xs-4">
                <a href="<?php echo Util::caminho_projeto() ?>/portfolio/<?php Util::imprime($row[url_amigavel]) ?>"  data-toggle="tooltip" data-placement="top" title="<?php Util::imprime($row[titulo]) ?>">
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="">
                </a>
            </div>
        <?php
        }
    }
    ?>





</div>


</div>
<!-- descricao-dicas -->





<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>
