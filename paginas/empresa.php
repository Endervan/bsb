
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>
<!-- ---- LAYER SLIDER ---- -->
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
    <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $("#carousel-gallery").touchCarousel({
            itemsPerPage: 1,
            scrollbar: true,
            scrollbarAutoHide: true,
            scrollbarTheme: "dark",
            pagingNav: false,
            snapToItems: true,
            scrollToLast: false,
            useWebkit3d: true,
            loopItems: true
        });
    });
    </script>
    <!-- XXXX LAYER SLIDER XXXX -->




</head>
<body>

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->



    <!-- bg-empresa -->
    <div class="container-fluir">
        <div class="row">
            <div class="bg-empresa"></div>
        </div>
    </div>
    <!-- bg-empresa -->

    <!-- descricao-empresa -->
    <div class="container">
        <div class="row">
            <div class="col-xs-3 top50 bottom10">
                <div class="descricao-empresa">
                    <h3>A EMPRESA</h3>
                </div>
            </div>
        </div>

        <div class="row descricao-empresa1 bottom20">
            <div class="col-xs-12">
                <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
                <p><?php Util::imprime($dados[descricao]) ?></p>
            </div>
        </div>
    </div>
    <!-- descricao-empresa -->

    <!-- depoimentos empresa -->
    <div class="container top40">
        <div class="row">
         <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner personalizar-carroucel" role="listbox">


                  <?php
                   $result = $obj_site->select("tb_depoimentos", "ORDER BY rand() LIMIT 5");
                   if (mysql_num_rows($result) > 0) {
                       $i = 0;
                       while($row = mysql_fetch_array($result)){
                        ?>
                        <div class="item <?php if($i == 0){ echo 'active'; } ?>">
                            <div class="col-xs-6">
                                <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="">
                            </div>
                            <div class="col-xs-6">
                                <h1>DEPOIMENTOS</h1>
                                <p><?php Util::imprime($row[depoimento], 10000) ?></p>
                                <h2><?php Util::imprime($row[titulo]) ?></h2>
                            </div>
                            <div class="carousel-caption">
                            </div>
                        </div>
                        <?php
                        ++$i;
                        }
                    }
                    ?>
                <!-- item 01 -->
    </div>

    <!-- Controls -->
    <a class="left carousel-control posicao-caroucel controle-esquerdo" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control posicao-caroucel  controle-direito" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
</div>
</div>
<!-- depoimentos empresa -->



<!-- portifolio -empresa -->
<div class="container-fluir fundo-azul top90 pb60">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 top30 bottom20 nosso-portifolio-empresa">
                    <h1>NOSSO PORTFÓLIO</h1>
                </div>


                <!-- banner-portifolio -->
                <?php
                 $result = $obj_site->select("tb_portifolios", "ORDER BY rand() LIMIT 6");
                 if (mysql_num_rows($result) > 0) {
                     while($row = mysql_fetch_array($result)){
                      ?>
                        <div class="col-xs-4">
                            <a href="<?php echo Util::caminho_projeto() ?>/portfolio/<?php Util::imprime($row[url_amigavel]) ?>"  data-toggle="tooltip" data-placement="top" title="<?php Util::imprime($row[titulo]) ?>">
                                <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="">
                            </a>
                        </div>
                    <?php
                    }
                }
                ?>

                <!-- banner-portifolio -->

            </div>
        </div>
    </div>
</div>
<!-- portifolio -empresa -->





<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>
